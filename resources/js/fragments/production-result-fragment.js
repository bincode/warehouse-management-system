import * as select from '../selects/select2-config';
import * as table from '../table-inputs/table-input-utils';

// Call the initialize function with the necessary arguments
table.init('table-input-goods', select.productSelectBySelectorConfig, select.rackSelectConfig);

// Call the function when the page is ready
$(function () {
    handleAjax();
});

$('input[name="for"]').on('input', handleAjax);
$('input[name="output"]').on('change', handleLossCalculation);

function handleAjax() {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected formula ID and plan quantity from the respective elements
    const lot = $('input[name="for"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/form/production-result/${lot}`;

    if (lot.length >= 8) {
        // Send an AJAX GET request to the API endpoint
        $.ajax({
            url: apiUrl,
            method: 'GET',
            success: function (response) {
                // Only update input values after a successful response
                if (response.product && response.product.description) {
                    $('input[name="description"]').val(response.product.description);
                }

                if (response.consumption !== undefined) {
                    // Set the consumption value with rounding
                    $('input[name="consumption"]').val(parseFloat(response.consumption.toFixed(4)));
                }

                // Call handleLossCalculation after consumption and output are set
                handleLossCalculation();

                // Reinitialize the DataTable safely
                if ($.fn.DataTable.isDataTable('#table-input-goods')) {
                    $('#table-input-goods').DataTable().destroy(); // Destroy the existing DataTable instance
                }

                // Re-initialize the table with Select2 configs after the response
                table.init('table-input-goods', select.productSelectBySelectorConfig, select.rackSelectConfig);
            },
            error: function () {
                // Error handling if the AJAX request fails
            }
        });
    }
}

// Calculate and update the loss based on consumption and output
function handleLossCalculation() {
    const consumption = parseFloat($('input[name="consumption"]').val()) || 0;
    const output = parseFloat($('input[name="output"]').val()) || 0;

    // Round both consumption and output to 4 decimal places
    const roundedConsumption = parseFloat(consumption.toFixed(4));
    const roundedOutput = parseFloat(output.toFixed(4));

    // Calculate the loss based on the rounded values
    const loss = parseFloat((roundedConsumption - roundedOutput).toFixed(4));

    // Update the loss input field
    $('input[name="loss"]').val(loss);
}

