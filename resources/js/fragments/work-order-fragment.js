// Import necessary utility modules
import * as table from '../table-inputs/table-input-utils';
import * as select from '../selects/select2-config';
import * as validation from '../validations/ajax-validation-utils';

// Call the function when the page is ready
$(function () {
    handleCreateFormula();
    updateAllShipDateInputs();

    // Event listener for change event on ship_priority select elements
    $(`select[name="ship_priority"]`).on('change', function () {
        toggleShipDateInput($(this));
    });

    $('.formula-active').on('select2:select', handleCreateFormula)
});

// Call the function when the element with ID "create-formula" is clicked
$('#create-formula').on('click', handleCreateFormula);

function handleCreateFormula() {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected formula ID and plan quantity from the respective elements
    const id = $('select[name="formula_id"]').val();
    const quantity = $('input[name="plan_quantity"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/form/work-order/${id}?quantity=${quantity}`;

    // Show loading message while fetching data
    if (id) {
        $('#fragmentContainer').html('<div class="text-center m-3"><i class="fas fa-spinner fa-spin"></i> Loading...</div>');
    }

    // Send an AJAX GET request to the API endpoint
    $.ajax({
        url: apiUrl,
        method: 'GET',
        success: function (response) {
            // Insert the response HTML into the element with ID "fragmentContainer"
            $('#fragmentContainer').html(atob(response.content));

            const isColorAvailable = !!response.product.color;

            $('.product-color')
                .val(isColorAvailable ? response.product.color : '')
                .prop('readonly', isColorAvailable);

            // Initialize table and select components
            table.init('table-input-work-order', select.materialsSelectConfig, select.availableRackSelectConfig);

            // Event handler for input changes
            $('input[name="plan_quantity"], input[name="percentage[]"]').on('keyup', function () {
                var totalPercentage = 0.00;

                // Calculate and update quantities and percentages
                $('input[name="amount[]"]').each(function () {
                    var totalQuantity = $('input[name="plan_quantity"]').val();
                    var $row = $(this).closest('tr');
                    var percentage = parseFloat($row.find('input[name="percentage[]"]').val());

                    if (!isNaN(percentage)) {
                        const quantity = (totalQuantity * percentage) / 100;
                        totalPercentage += percentage;
                        $(this).val(quantity.toFixed(2));
                    }

                    // Update total percentage display and enable/disable submit button
                    $('label[for="totalPercentage"]').text("Total Percentage : " + totalPercentage + "%");
                    const toggle = !isNaN(totalPercentage) && totalPercentage !== '' && totalPercentage == 100;
                    $('#submitwo').prop('disabled', !toggle).toggleClass('btn-primary', toggle).toggleClass('btn-secondary', !toggle);
                });
            });
        },
        error: function () {
            // Error handling if the AJAX request fails
        }
    });
}

// Submit form event handler
$('#workform').on('submit', function (e) {
    e.preventDefault();

    // Check if 'lot' input is empty
    const lot = $('input[name="lot"]').val();

    if (lot !== '') {
        // If 'lot' is not empty, proceed with stock validation
        const method = $(this).attr('method');
        validation.stock(method, true);
    } else {
        // If 'lot' is empty, skip stock validation and submit the form
        this.submit();
    }
});

$('#checkstock').on('click', function (e) {
    e.preventDefault();
    const method = 'POST';
    validation.stock(method)
});

// Function to enable or disable ship_date input based on the ship_priority value
function toggleShipDateInput(selectElement) {
    const selectedValue = selectElement.val();
    const shipDateInput = $('input[name="ship_date"]');

    if (selectedValue === 'SPECIFIED_DATE') {
        shipDateInput.removeAttr('disabled');
    } else {
        shipDateInput.prop('disabled', true);
    }
}

// Function to check and set the state of ship_date input fields for all rows
function updateAllShipDateInputs() {
    $(`select[name="ship_priority"]`).each(function () {
        toggleShipDateInput($(this));
    });
}
