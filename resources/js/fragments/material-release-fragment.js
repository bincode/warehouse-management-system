import * as table from '../table-inputs/table-input-utils';
import * as select from '../selects/select2-config';

// Call the function when the page is ready
$(function () {
    // Call the initialize function with the necessary arguments
    initializeTable();

    if ($('.invalid-feedback').length > 0) {
        $('input[name="for"]').val('');
        $('input[name="id"]').val(''); // Clear ID input
        $('input[name="description"]').val(''); // Clear description input
    }

    $('input[name="for"]').trigger('focus');
});

// Call the function when the element with ID "create-formula" is clicked
$('#load-product').on('click', handleCreateReleaseForm);
$('input[name="for"]').on('input', handleCreateReleaseForm);

function handleCreateReleaseForm() {

    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected formula ID and plan quantity from the respective elements
    const lot = $('input[name="for"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/form/material-release/${lot}`;

    if (lot.length >= 8) {
        if (lot) {
            $('#fragmentContainer').html('<div class="text-center m-3"><i class="fas fa-spinner fa-spin"></i> Loading...</div>');
        }

        // Send an AJAX GET request to the API endpoint
        $.ajax({
            url: apiUrl,
            method: 'GET',
            success: function (response) {
                // Decode the base64 content
                $('#fragmentContainer').html(atob(response.content));

                if (response.product) {
                    $('select[name="description"]').append('<option value="' + response.product.id + '">' + response.product.name + '</option>');

                    // Check if input fields exist before appending values
                    if ($('input[name="description"]').length > 0) {
                        $('input[name="description"]').val(response.product.id);
                    }

                    if ($('input[name="id"]').length > 0) {
                        $('input[name="id"]').val(response.product.name);
                    }

                    // Check if input id is filled and submit the form
                    if ($('input[name="id"]').val()) {
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'isRedirect',
                            value: 0
                        }).appendTo('#release');

                        $('#release').trigger('submit');
                    }
                } else {
                    // If no product, clear all inputs
                    $('select[name="description"]').empty(); // Clear options from the select

                    if ($('input[name="id"]').length > 0) {
                        $('input[name="id"]').val(''); // Clear ID input
                    }

                    if ($('input[name="description"]').length > 0) {
                        $('input[name="description"]').val(''); // Clear description input
                    }
                }

                // Call the initialize function with the necessary arguments
                initializeTable();
            },
            error: function () {
                // Error handling if the AJAX request fails
            }
        });
    }
}

function initializeTable() {
    if (!$.fn.DataTable.isDataTable('#table-input-materials')) {
        table.init('table-input-materials', select.materialsSelectConfig, select.availableRackSelectConfig);
    }
}
