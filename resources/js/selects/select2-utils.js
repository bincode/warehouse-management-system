import 'admin-lte/plugins/select2/js/select2.full.min.js';

// Global configuration for Select2
const SELECT2_GLOBAL_CONFIG = {
    placeholder: '... Select ...',
    ajax: {
        cache: true,
        delay: 250,
    },
};

// Function to transform AJAX response data into Select2-compatible format
function processResults(data) {
    return {
        results: $.map(data.data, function (obj) {
            return {
                id: obj.id,
                text: obj.name,
            };
        }),
    };
}


// Function to configure and initialize Select2
export function config(selector, config) {
    return $(selector).select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Function to configure and initialize Select2
export function reload($element, config) {
    return $element.select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

config('.select2', null);

// Configuration and initialization for "permissions[]" select element
export const permissionSelectConfig = {
    placeholder: '.. Select Permissions ..',
    ajax: {
        url: '/api/permissions',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: processResults,
    },
};

config("select[name='permissions[]']", permissionSelectConfig);

// Configuration and initialization for "roles[]" select element
export const roleSelectConfig = {
    placeholder: '.. Select Roles ..',
    ajax: {
        url: '/api/roles',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: processResults,
    },
};

config("select[name='roles[]']", roleSelectConfig);

// Configuration and initialization for "warehouse" select element
export const warehouseSelectConfig = {
    placeholder: '.. Select Warehouse ..',
    ajax: {
        url: '/api/warehouses',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: processResults,
    },
};

config("select[name='warehouse_id']", warehouseSelectConfig);

// Configuration and initialization for "category of product" select element
export const productCategorySelectConfig = {
    placeholder: '.. Select Category ..',
    ajax: {
        url: '/api/product/category',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: processResults,
    },
};

config("select.product-category", productCategorySelectConfig);

// Configuration and initialization for "rack_id" select element
export const rackSelectConfig = {
    placeholder: '.. Select Rack ..',
    ajax: {
        url: '/api/racks',
        data: function (params) {
            return {
                text: params.term,
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.code,
                    };
                }),
            };
        },
    },
};

config("select[name='rack_id']", rackSelectConfig);

// Configuration and initialization for "availableRack" select element
export const availableRack = {
    placeholder: '.. Select Location ..',
    ajax: {
        url: '/api/racks',
        data: function (params) {
            return {
                text: params.term,
                product: $(this).closest('tr').find('select[name="products[]"]').val(),
                select2: true,
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.code,
                    };
                }),
            };
        },
    },
};

config("select.availableRack", availableRack);

// Function to create Select2 configuration for filtering products by category
function filterProductbyCategory(category) {
    return {
        placeholder: '.. Select ' + category + ' ..',
        ajax: {
            url: '/api/products',
            data: function (params) {
                return {
                    text: params.term,
                    select2: true,
                    category: category,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.name,
                        };
                    }),
                };
            },
        },
    };
};

// Configuration for selecting products (all categories)
export const productSelectConfig = filterProductbyCategory('');
config("select.product", productSelectConfig);

// Configuration for selecting goods
export const goodsSelectConfig = filterProductbyCategory('goods');
config("select.goods", goodsSelectConfig);

// Configuration for selecting materials
export const materialsSelectConfig = filterProductbyCategory('materials');
config("select.materials", materialsSelectConfig);

export const relaseSelectConfig = {
    ajax: {
        delay: 250,
        url: '/api/releases',
        data: function (params) {
            return {
                text: params.term,
                select2: true,
            }
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.for,
                        product: obj.product,
                        used: obj.used,
                    };
                })
            };
        }
    }
}

config("select.for", relaseSelectConfig);

// Function to create Select2 configuration for filtering products by category
function filterCompanybyCategory(category) {
    return {
        placeholder: '.. Select ..',
        ajax: {
            url: '/api/relations',
            data: function (params) {
                return {
                    text: params.term,
                    type: category,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.name,
                        };
                    }),
                };
            },
        },
    };
};

// Configuration for selecting products (all categories)
export const companySelectConfig = filterCompanybyCategory('cs');
config("select.company", companySelectConfig);

// Configuration for selecting goods
export const supplierSelectConfig = filterCompanybyCategory('s');
config("select.supplier", supplierSelectConfig);

// Configuration for selecting materials
export const customerSelectConfig = filterCompanybyCategory('c');
config("select.customer", customerSelectConfig);

// Configuration and initialization for "category of product" select element
export const formulaSelectConfig = {
    placeholder: '.. Select Formula ..',
    ajax: {
        url: '/api/formulas',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: `${obj.product.name} (${obj.version})`,
                    };
                }),
            };
        },
    },
};

config("select[name='formula_id']", formulaSelectConfig);

// Configuration and initialization for "category of product" select element
export const machineCategorySelectConfig = {
    placeholder: '.. Select Category ..',
    ajax: {
        url: '/api/machines-category',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: processResults,
    },
};

config("select.machine-cat", machineCategorySelectConfig);

// Configuration and initialization for "category of product" select element
export const MachinProcSelectConfig = {
    placeholder: '.. Select Process Type ..',
    ajax: {
        url: '/api/machines-process-type',
        data: function (params) {
            return {
                text: params.term
            };
        },
        processResults: function (data) {
            return {
                results: $.map(data.data, function (obj) {
                    return {
                        id: obj.id,
                        text: obj.name,
                        description: obj.description
                    };
                }),
            };
        },
    },
    templateResult: function (item) {
        if (!item.id) {
            return item.text;
        }

        // Formatting the result item
        return $('<span><strong>' + item.text + '</strong><br><small>' + item.description + '</small></span>');
    },
};

config("select.machineproctype", MachinProcSelectConfig);

// Function to create Select2 configuration for filtering machine by type
function filterMachinebyType(type) {
    return {
        placeholder: '.. Select ' + type + ' ..',
        ajax: {
            url: '/api/machines',
            data: function (params) {
                return {
                    text: params.term,
                    type: type,
                };
            },
            processResults: processResults,
        },
    };
};

// Configuration for selecting machine (all)
export const machineSelectConfig = filterMachinebyType('');
config("select.machine", machineSelectConfig);

// Configuration for selecting machine (extruder)
export const extruderSelectConfig = filterMachinebyType('Extruder');
config("select.extruder", extruderSelectConfig);

// Configuration for selecting machine (mixer)
export const mixerSelectConfig = filterMachinebyType('Mixer');
config("select.mixer", mixerSelectConfig);

$(function () {
    $(document).on('change', 'select.product-category', function () {
        const selectedCategory = $(this).val();
        if (selectedCategory > 1) {
            $('input[name="color"]').closest('.form-group').show();
        } else {
            $('input[name="color"]').closest('.form-group').hide();
        }
    });
});

// Configuration and initialization for "category of product" select element
function filterSalesOrderByStatus(status) {
    return {
        placeholder: '.. Select SO Number ..',
        ajax: {
            url: '/api/salesorder',
            data: function (params) {
                return {
                    text: params.term,
                    product: $('#product_id').val(),
                    status: status
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.po_number,
                            customer: obj.company.name,
                            product: obj.products[0].name,
                            quantity: obj.products[0].pivot.amount,
                            note: obj.products[0].pivot.note,
                            shipDate: obj.products[0].pivot.ship_date
                        };
                    }),
                };
            },
        },
        templateResult: function (item) {
            if (!item.id) {
                return item.text;
            }

            // Formatting the result item
            return $('<span><strong>' + item.text + '</strong> <small>(Ship Date : ' + item.shipDate + ')</small><br><small>' + item.product + ' / ' + item.customer + ' / ' + item.quantity + ' Kg </small></span>');
        },
    };
}

export const SalesOrderSelectConfig = filterSalesOrderByStatus("");
config("select.sales_po", SalesOrderSelectConfig);

export const SalesOrderPrepareSelectConfig = filterSalesOrderByStatus("created");
config("select.sales_po_prepare", SalesOrderPrepareSelectConfig);

$('select.sales_po, select.sales_po_prepare').on('select2:select select2:unselect', function (e) {
    var data = e.params.data;

    // Ambil semua data yang telah dipilih
    var selectedData = $(this).select2('data');

    // Inisialisasi variabel untuk menyimpan pelanggan
    var customers = [];
    var orderQuantity = 0;

    // Loop melalui data yang dipilih dan ambil atribut customer
    selectedData.forEach(function (item) {
        customers.push(item.customer);
        orderQuantity += item.quantity;
    });

    // Gabungkan pelanggan menjadi string yang dipisahkan oleh koma
    var customersString = customers.join(' / ');

    // Set nilai input customer dengan pelanggan yang dipilih
    $('input[name="customer"]').val(customersString);

    // Set nilai input product dengan produk dari item yang baru saja dipilih
    $('input[name="product"]').val(data.product);

    $('input[name="order_quantity"]').val(orderQuantity);
});
