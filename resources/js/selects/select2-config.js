// Importing the Select2 library
import 'admin-lte/plugins/select2/js/select2.full.min.js';

// Global configuration for Select2
const SELECT2_GLOBAL_CONFIG = {
    placeholder: '... Select ...',
    ajax: {
        delay: 250,
    },
};

// Function to format data by default
function defaultFormat(data) {
    return {
        results: $.map(data.data, function (obj) {
            return {
                id: obj.id,
                text: obj.name,
            };
        }),
    };
}

// Function to configure Select2
export function config(selector, config) {
    return $(selector).select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Function to reload Select2
export function reload($element, config) {
    return $element.select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

function filterProductbyCategory(category, selector) {
    return {
        placeholder: `.. Select ${category.charAt(0).toUpperCase() + category.slice(1)} ..`,
        ajax: {
            url: '/api/products',
            data: function (params) {
                const selectorValue = $(selector).length ? $(selector).val().match(/\d+/)[0] : null;
                return {
                    text: selectorValue || params.term, // Mengambil nilai dari selector jika ada, atau dari input Select2
                    category: category,
                    select2: true,
                };
            },
            processResults: defaultFormat,
        },
    };
};

function filterRackbyProduct(isAvaliable = true) {
    return {
        placeholder: '.. Select Location ..',
        ajax: {
            url: '/api/racks',
            data: function (params) {
                var product = $(this).closest('tr').find('select[name="products[]"]').val();

                var dataParams = {
                    text: params.term,
                    select2: true
                };

                if (product && isAvaliable) {
                    dataParams.product = product;
                }

                return dataParams;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.code,
                        };
                    }),
                };
            },
        },
    }
}

function filterFormulaByActive() {
    return {
        placeholder: '.. Select Formula ..',
        ajax: {
            url: '/api/formulas',
            data: function (params) {
                return {
                    text: params.term,
                    select2: true,
                    isActive: true
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: `${obj.product.name} (${obj.version})`,
                        };
                    }),
                };
            },
        },
    };
}

function filterCompanybyCategory(category = 'cs') {
    return {
        placeholder: '.. Select ..',
        ajax: {
            url: '/api/relations',
            data: function (params) {
                return {
                    text: params.term,
                    type: category,
                };
            },
            processResults: defaultFormat,
        },
    };
};

function filterMachinebyType(type) {
    return {
        placeholder: '.. Select ' + type + ' ..',
        ajax: {
            url: '/api/machines',
            data: function (params) {
                return {
                    text: params.term,
                    type: type,
                };
            },
            processResults: defaultFormat,
        },
    };
};

function filterMachineProcessType() {
    return {
        placeholder: '.. Select Process Type ..',
        ajax: {
            url: '/api/machines-process-type',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.name,
                            description: obj.description
                        };
                    }),
                };
            },
        },
        templateResult: function (item) {
            if (!item.id) {
                return item.text;
            }

            // Formatting the result item
            return $('<span><strong>' + item.text + '</strong><br><small>' + item.description + '</small></span>');
        },
    }
};

// Configuration for selecting products (all categories)
export const productSelectConfig = filterProductbyCategory('');
config("select.product", productSelectConfig);

// Configuration for selecting goods
export const goodsSelectConfig = filterProductbyCategory('goods');
config("select.product-goods", goodsSelectConfig);

// Configuration for selecting materials
export const materialsSelectConfig = filterProductbyCategory('materials');
config("select.product-materials", materialsSelectConfig);

// Configuration for selecting products (all categories)
export const productSelectBySelectorConfig = filterProductbyCategory('', '.product-selector-search');
config("select.product-search", productSelectBySelectorConfig);

export const rackSelectConfig = filterRackbyProduct(false);
config("select.rack", rackSelectConfig);

export const availableRackSelectConfig = filterRackbyProduct();
config("select.rack-available", availableRackSelectConfig);

export const formulaSelectConfig = filterFormulaByActive();
config("select.formula-active", formulaSelectConfig);

// Configuration for selecting products (all categories)
export const companySelectConfig = filterCompanybyCategory();
config("select.company", companySelectConfig);

// Configuration for selecting goods
export const supplierSelectConfig = filterCompanybyCategory('s');
config("select.supplier", supplierSelectConfig);

// Configuration for selecting materials
export const customerSelectConfig = filterCompanybyCategory('c');
config("select.customer", customerSelectConfig);

// Configuration for selecting machine (all)
export const machineSelectConfig = filterMachinebyType('');
config("select.machine", machineSelectConfig);

// Configuration for selecting machine (extruder)
export const extruderSelectConfig = filterMachinebyType('Extruder');
config("select.extruder", extruderSelectConfig);

// Configuration for selecting machine (mixer)
export const mixerSelectConfig = filterMachinebyType('Mixer');
config("select.mixer", mixerSelectConfig);

export const machineProcessTypeSelectConfig = filterMachineProcessType();
config("select.machine-process-type", machineProcessTypeSelectConfig);
