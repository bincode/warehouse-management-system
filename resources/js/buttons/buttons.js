import * as button from './button-utils.js';

/**
 * Method to create edit action button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function renderEdit(disabled = false) {
    return button.render('btn_edit', 'btn-warning', 'fas fa-edit', disabled, 'Edit');
}

/**
 * Method to create view action button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function renderView(disabled = false) {
    return button.render('btn_view', 'btn-info', 'fas fa-eye', disabled, 'View');
}

/**
 * Method to create delete action button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function renderDelete(disabled = false) {
    return button.render('btn_trash', 'btn-danger', 'fas fa-trash', disabled, 'Delete');
}

/**
 * Method to create rack action button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function renderRack(disabled = false) {
    return button.render('btn_rack', 'btn-primary', 'fas fa-layer-group', disabled, 'Racks');
}

/**
 * Method to create history action button
 *
 * @param {boolean} isAllowed
 * @returns {string}
 */
export function renderHistory(disabled = false) {
    return button.render('btn_history', 'btn-primary', 'fas fa-history', disabled, 'See History');
}
