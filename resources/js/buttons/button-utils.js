/**
 * Render Button Action
 *
 * @param {string} id
 * @param {string} color
 * @param {string} icon
 * @param {boolean} disabled
 * @param {string} label
 * @returns string
 */
export function render(id, color, icon, disabled, label = '') {
    // Determine the button color based on whether it is allowed or not
    const buttonColor = disabled ? color : "btn-secondary";

    // Determine the button class based on whether it is allowed or not
    const buttonClass = disabled ? "" : "disabled";

    // Create the tooltip attribute if a label is provided
    const tooltip = label ? `data-toggle="tooltip" data-placement="bottom" title="${label}"` : '';

    // Construct the HTML string for the button element
    return `<button id="${id}" class="btn btn-sm mr-1 ${buttonColor}" ${buttonClass} ${tooltip}><i class="${icon}"></i></button>`;
}
