import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Rack Code",
        data: "code",
    },
    {
        title: "Note",
        data: "note",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicAction(true, true);
        }
    }
];

// Get the path from the current URL
const path = window.location.pathname;

// Match the number after "warehouses/" using regular expression
const matches = path.match(/\/warehouses\/(\d+)\/racks/);

// If matched, the second value in the 'matches' array is the value "2"
const warehouse_id = matches ? matches[1] : null;

const paramAjax = {
    warehouse: warehouse_id
};

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableRack", "/api/racks", columns, paramAjax);
