import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Machine Name",
        data: "name",
    },
    {
        title: "Description",
        data: "description",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicAction(true, true);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableMachine", "/api/machines", columns);
