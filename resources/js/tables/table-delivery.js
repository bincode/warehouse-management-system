import * as table from "./base-table";
import "../utils/daterangepicker-utils";

// Define the column configurations for the DataTable
const columns = [{
    title: "Date",
    data: "date",
}, {
    title: "Document Reference",
    data: "for",
}, {
    title: "Customer Name",
    data: 'company.name',
    width: "20%",
    defaultContent: "?"
}, {
    title: "Actions",
    data: null,
    width: "5%",
    class: "text-center",
    searchable: false,
    render: function (data, type, row) {
        var disableButton = data.id != 1;
        return table.callBasicAction(disableButton, disableButton);
    }
}
];

let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();

    d.start_date = $('#tableSrcDateStart').val();
    d.end_date = $('#tableSrcDateEnd').val();
};

$(function () {
    // Call init to create the DataTable and get access to renderEditAndDeleteButtons
    const myTable = table.init("tableDelivery", "/api/deliveries", columns, paramAjax);

    // Bind daterangepicker apply event to reload table when date is changed
    const searchDate = $('#tableSrcDateStart, #tableSrcDateEnd').on('apply.daterangepicker', function (ev, picker) {
        myTable.ajax.reload();
    });
});
