// Import DataTables plugin and some other modules
import 'admin-lte/plugins/datatables/jquery.dataTables.min.js';
import 'admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js';
import 'admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js';
import 'admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js';

import * as button from '../buttons/buttons';
import { ConfirmationDeleteItem, LoadingAlert, SuccessDeleteAlert } from '../utils/alert-utils';

// Function to create a base DataTable with specified columns and AJAX URL
export function init(tableId, url, columns, paramAjax, paging = false, serverSide = false) {
    console.log('Init Datatable');

    // Initialize the DataTable with specified options
    const table = $('#' + tableId).DataTable({
        processing: serverSide,
        serverSide: serverSide,
        paging: paging,
        isFilterDate: false,
        ordering: false,
        pageLength: 100,
        dom: 'tp',
        ajax: {
            url: url,
            data: paramAjax,
            type: 'GET',
            dataSrc: 'data',
            beforeSend: function () {
                LoadingAlert();
            },
            complete: function () {
                LoadingAlert().close();
            }
        },
        columns: columns,
        drawCallback: function (settings) {
            // Check the number of recordsFiltered from the response
            if (settings.json && settings.json.recordsFiltered < 100) {
                // Hide pagination if recordsFiltered is less than 100
                $('#' + tableId + '_paginate').hide();
            } else {
                // Show pagination if more than 100 records are filtered
                $('#' + tableId + '_paginate').show();
            }
        }
    });

    /**
     * Method's Tables Search
     */
    $.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
        // Check if both date input elements exist
        let result = true;

        if ($('#tableSrcDateStart').length > 0 && $('#tableSrcDateEnd').length > 0) {
            // Date format
            const dFormat = 'DD/MM/YYYY';

            // Parse the date from the row data
            const dDate = moment(data[0], dFormat);

            // Get the start and end dates from the input elements
            const dStart = moment($('#tableSrcDateStart').val(), dFormat).subtract(1, 'days');
            const dEnd = moment($('#tableSrcDateEnd').val(), dFormat).add(1, 'days');

            // Check if the date falls within the specified range
            result = (dDate.isAfter(dStart) && dDate.isBefore(dEnd)) ? true : false;
        }

        return result;
    });


    // Handle click event on edit button in table
    $('#' + tableId + ' tbody').on('click', '#btn_edit', function (e) {

        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Open Edit Page with ID of selected row in URL
        location.href = window.location.pathname + '/' + data['id'] + '/edit';
    });

    // Handle click event on view button in table
    $('#' + tableId + ' tbody').on('click', '#btn_view', function (e) {

        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Open Edit Page with ID of selected row in URL
        location.href = window.location.pathname + '/' + data['id'];
    });

    // Handle click event on delete button in table
    $('#' + tableId + ' tbody').on('click', '#btn_trash', function (e) {
        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Show confirmation dialog before deleting
        ConfirmationDeleteItem().then(function (result) {
            if (result.value) {
                // If user confirms deletion, send DELETE request to server
                handleDeleteRequest(data['id'], url, table);
            }
        });
    });

    $('#' + tableId + ' tbody').on('click', '#btn_rack', function (e) {
        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Open Racks Page with ID of selected row in URL
        location.href = window.location.pathname + '/' + data['id'] + '/racks';
    });

    $('#' + tableId + ' tbody').on('click', '#btn_history', function (e) {
        // Get the row index and data from the table
        const index = $(this).parents('tr');
        const data = table.row(index).data();

        // Open Edit Page
        location.href = window.location.href + '/' + data['id'] + '/history';
    });

    $('.search-ajax').on('keyup', function (event) {
        // Checking if the pressed key is the "Enter" key
        if (event.key === 'Enter') {
            // If the "Enter" key is pressed, reload the table
            table.ajax.reload();
        }
    });

    // Function to send DELETE request to server and handle response
    function handleDeleteRequest(id, uri, dataTable) {
        const url = uri + '/' + id;

        // Set CSRF token header for AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Send DELETE request to server
        $.ajax({
            url: url,
            type: 'DELETE',
            success: function (response, textStatus, xhr) {
                table.ajax.reload();
            },
            error: function (xhr, status, error) {
                const response = xhr.responseJSON || {};
                const errorMessage = response.message || 'An unexpected error occurred.';
                ServerErrorAlert(errorMessage);
            }
        });
    }

    return table;
};

export function callBasicAction(canEdit, canDelete) {
    return button.renderEdit(canEdit) + button.renderDelete(canDelete);
}

export function callBasicWithHistoryAction(canEdit, canDelete) {
    return button.renderHistory(true) + callBasicAction(canEdit, canDelete);
}

export function callBasicWithRackAction(canEdit, canDelete) {
    return button.renderRack(true) + callBasicAction(canEdit, canDelete);
}

export function callBasicWithViewAction(canEdit, canDelete) {
    return button.renderView(true) + callBasicAction(canEdit, canDelete);
}
