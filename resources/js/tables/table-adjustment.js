import * as table from "./base-table";
import { InputDateDialog, InputDateFileDialog } from '../utils/alert-utils';

import "../utils/daterangepicker-utils";

// Define the column configurations for the DataTable
const columns = [{
    title: "Date",
    data: "date",
},
{
    title: "Document Reference",
    data: "for",
},
{
    title: "Customer Name",
    data: 'description',
    defaultContent: "?"
},
{
    title: "Status",
    data: 'status',
    width: "5%",
    defaultContent: "?"
},
{
    title: "Actions",
    data: null,
    width: "5%",
    class: "text-center",
    searchable: false,
    render: function (data, type, row) {
        var disableButton = data.id != 1;
        return table.callBasicAction(disableButton, disableButton);
    }
}
];

let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();

    d.start_date = $('#tableSrcDateStart').val();
    d.end_date = $('#tableSrcDateEnd').val();
};


$(function () {
    // Call init to create the DataTable and get access to renderEditAndDeleteButtons
    const myTable = table.init("tableAdjustment", "/api/adjustments", columns, paramAjax);

    // Bind daterangepicker apply event to reload table when date is changed
    const searchDate = $('#tableSrcDateStart, #tableSrcDateEnd').on('apply.daterangepicker', function (ev, picker) {
        myTable.ajax.reload();
    });
});

$('#buttonExport').on('click', function (e) {
    e.preventDefault();

    // Define a validation callback
    const validateDate = (dialog, dateValue) => {
        const today = new Date().toISOString().split('T')[0]; // Get today's date in YYYY-MM-DD format

        if (dateValue > today) {
            dialog.showValidationMessage(`Date cannot be in the future.`);
        }
        return { date: dateValue }; // Return the date if valid
    };

    InputDateDialog("Download Cut Off Data", 'Download', validateDate).then((result) => {
        if (result.isConfirmed) {
            const selectedDate = result.value.date;

            // Convert the date to d/m/Y format
            const dateParts = selectedDate.split('-'); // Assuming selectedDate is in YYYY-MM-DD format
            const formattedDate = `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;

            // Redirect to the export URL
            window.location.href = `/api/stocktaking/export?date=${encodeURIComponent(formattedDate)}`;
        }
    });
});

$('#buttonImport').on('click', function (e) {
    e.preventDefault();

    InputDateFileDialog("Upload File", "Upload").then(result => {
        if (result.isConfirmed) {
            const { date, file } = result.value;

            // Check if date and file are correctly defined
            if (!date || !file) {
                console.log("Date or file missing.");
                return;
            }

            // Convert the date to d/m/Y format
            const dateParts = date.split('-'); // Assuming selectedDate is in YYYY-MM-DD format
            const formattedDate = `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;

            // Create FormData to send the date and file
            const formData = new FormData();
            formData.append('date', formattedDate);
            formData.append('file', file);

            // AJAX POST request to send the form data
            $.ajax({
                url: '/api/stocktaking/import',
                method: 'POST',
                data: formData,
                processData: false, // Prevent jQuery from processing the data
                contentType: false, // Prevent jQuery from setting the content type
                success: function (response) {
                    console.log('success');
                },
                error: function (xhr) {
                    console.log('failed');
                }
            });
        }
    });
});
