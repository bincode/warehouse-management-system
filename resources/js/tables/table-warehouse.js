import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Warehouse Name",
        data: "name",
    },
    {
        title: "Address",
        data: "address",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicWithRackAction(true, true);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableWarehouse", "/api/warehouses", columns);
