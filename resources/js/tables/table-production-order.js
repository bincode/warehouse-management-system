import * as table from "./base-table";
import * as button from '../buttons/button-utils';
import '../moment/moment-initialization';
import "../alerts/alert-utils";

// Define the columns for the DataTable
const columns = [{
    title: 'Product Name',
    data: 'name',
}, {
    title: 'Color',
    data: 'color',
    class: "text-center",
    width: "10%",
    default: "-",
}, {
    title: 'Quantity',
    data: 'amount',
    class: "text-right",
    width: "10%",
}, {
    title: 'Ship Date',
    data: 'nearest_ship_date',
    class: "text-right",
    width: "10%",
}, {
    title: '-',
    data: 'nearest_ship_date',
    class: "text-center",
    width: "10%",
    render: function (data, type, row) {
        // Calculate relative time using moment.js
        let shipDate = moment(data);
        let today = moment().startOf('day');
        let badgeClass = '';
        let displayText = shipDate.fromNow();

        if (shipDate.isSame(today, 'day')) {
            badgeClass = 'badge badge-warning'; // yellow badge for today
            displayText = 'TODAY';
        } else if (shipDate.isSame(today.add(1, 'day'), 'day')) {
            badgeClass = 'badge badge-warning'; // yellow badge for tomorrow
            displayText = 'TOMORROW';
        } else if (shipDate.isBefore(today, 'day')) {
            badgeClass = 'badge badge-danger'; // red badge for past dates
        } else {
            badgeClass = 'badge badge-success'; // green badge for future dates beyond tomorrow
        }

        return `<span class="${badgeClass}">${displayText}</span>`;
    }
}, {
    title: "",
    data: null,
    width: "5%",
    class: "text-center",
    searchable: false,
    render: function (data, type, row) {
        return button.render('btn_create', 'btn-success', 'fas fa-plus-square', true, 'Create');
    }
}];

// Define the parameters for the AJAX request
let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();
};

// Initialize the DataTable
const myTable = table.init("tableProductionOrder", "/api/productionorder", columns, paramAjax);

$('#tableProductionOrder tbody').on('click', '#btn_create', function (e) {
    // Get the row index and data from the table
    const index = $(this).parents('tr');
    const data = myTable.row(index).data();

    // Open Racks Page with ID of selected row in URL
    location.href = window.location.pathname + '/create/' + data['id'];
});
