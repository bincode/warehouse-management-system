import * as table from "./base-table";
import * as button from '../buttons/button-utils';
import * as alert from "../alerts/alert-utils";
import '../moment/moment-initialization';

// Define the columns for the DataTable
const columns = [{
    title: 'Product Name',
    data: 'name',
}, {
    title: 'Customer Name',
    data: 'customer',
    width: "25%",
}, {
    title: 'Quantity',
    data: 'amount',
    class: "text-right",
    width: "10%",
}, {
    title: 'Ship Date',
    data: 'ship_date',
    class: "text-right",
    width: "10%",
}, {
    title: '-',
    data: 'ship_date',
    class: "text-center",
    width: "10%",
    render: function (data, type, row) {
        // Calculate relative time using moment.js
        let shipDate = moment(data);
        let today = moment();
        let badgeClass = '';
        let relativeTime;

        // Periksa apakah tanggal pengiriman bertepatan dengan hari ini
        if (shipDate.isSame(today, 'day')) {
            relativeTime = 'Today';
        } else {
            // Hitung selisih hari antara tanggal pengiriman dan hari ini
            let diffDays = shipDate.diff(today, 'days');
            relativeTime = `${diffDays+1} days`;
        }

        if (shipDate.isBefore(today, 'day')) {
            badgeClass = 'badge badge-danger'; // red badge for past dates
        } else if (shipDate.isBefore(today.add(7, 'days'), 'day')) {
            badgeClass = 'badge badge-warning'; // yellow badge for dates within a week
        } else {
            badgeClass = 'badge badge-success'; // green badge for dates more than a week away
        }

        return `<span class="${badgeClass}">${relativeTime}</span>`;
    }
}, {
    title: "",
    data: null,
    width: "5%",
    class: "text-center",
    searchable: false,
    render: function (data, type, row) {
        return button.render('btn_create', 'btn-success', 'fas fa-plus-square', true, 'Create');
    }
}];

// Define the parameters for the AJAX request
let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();
};

// Initialize the DataTable
const myTable = table.init("tableDeliverySchedule", "/api/deliveryschedule", columns, paramAjax);

$('#tableDeliverySchedule tbody').on('click', '#btn_create', function (e) {
    // Get the row index and data from the table
    const index = $(this).parents('tr');
    const data = myTable.row(index).data();

    // Mendapatkan tanggal hari ini dengan Moment.js
    let formattedDate = moment().format('YYYY-MM-DD');

    // Menambahkan data now ke dalam objek data
    data.now = formattedDate;
    const url = '/deliveryrequest';

    alert.showInput(data).then((result) => {
        if (result.isConfirmed) {
            // Get the CSRF token from the meta tag
            const token = $('meta[name="csrf-token"]').attr('content');

            console.log(result.value);

            // Lakukan panggilan AJAX di sini dengan data yang diperoleh
            $.ajax({
                url: url,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': token // Menggunakan token CSRF dalam header
                },
                data: {
                    product_sales_order_id: data['id'],
                    date: result.value.date,
                    lot: result.value.lot,
                    quantity: result.value.quantity,
                    bag: result.value.bag,
                    document: result.value.document,
                    note: result.value.note
                },
                success: function (response) {
                    // Handle success response
                    alert.showSuccessAlert();
                    myTable.ajax.reload();
                },
                error: function (xhr, status, error) {
                    // Handle error response
                    alert.showServerErrorAlert();
                }
            });
        }
    });
});
