import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Username",
        data: "username",
    },
    {
        title: "Role",
        data: "roles",
        render: function (data, type, row) {
            var render = '';

            if (data) {
                render = data.map(role => `<small class="badge badge-primary"> ${role.name} </small>`).join('');
            }

            return render;
        }
    },
    {
        title: "Last Login Time",
        data: "attempt_clock",
    },
    {
        title: "Last Login IP",
        data: "attempt_ip",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            var disableButton = data.id != 1;
            return table.callBasicAction(disableButton, disableButton);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableUser", "/api/users", columns);
