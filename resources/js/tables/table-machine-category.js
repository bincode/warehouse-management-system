import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Category",
        data: "name",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicAction(true, true);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableMachineCategory", "/api/machines-category", columns);
