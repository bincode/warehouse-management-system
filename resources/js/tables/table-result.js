import "../utils/daterangepicker-utils";
import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
    title: "Date",
    width: "5%",
    data: "date",
},{
    title: "No. Lot",
    width: "15%",
    data: "for",
},{
    title: "Goods Product",
    data: 'description',
    defaultContent: "N/A",
},{
    title: "Actions",
    data: null,
    width: "5%",
    class: "text-center",
    searchable: false,
    render: function (data, type, row) {
        var disableButton = data.id != 1;
        return table.callBasicAction(disableButton, disableButton);
    }
}
];

let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();

    d.start_date = $('#tableSrcDateStart').val();
    d.end_date = $('#tableSrcDateEnd').val();
};

$(function () {
    // Call init to create the DataTable and get access to renderEditAndDeleteButtons
    const myTable = table.init("tableResult", "/api/results", columns, paramAjax);

    // Bind daterangepicker apply event to reload table when date is changed
    const searchDate = $('#tableSrcDateStart, #tableSrcDateEnd').on('apply.daterangepicker', function (ev, picker) {
        myTable.ajax.reload();
    });
});
