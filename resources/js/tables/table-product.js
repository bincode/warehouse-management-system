import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Product Name",
        data: "name",
    },
    {
        title: "Description",
        data: "description",
    },
    {
        title: "Quantity",
        data: 'quantity',
        width: "10%",
        className: "text-right dt-body-justify",
        render: function (data, type, row) {
            return Number.parseFloat(data.toFixed(3));
        }
    },
    {
        title: "Reserve Quantity",
        data: 'reserve',
        width: "10%",
        className: "text-right dt-body-justify",
        render: function (data, type, row) {
            return Number.parseFloat(data.toFixed(3));
        }
    },
    {
        data: 'unit',
        className: "text-left dt-body-justify",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicWithHistoryAction(true, true);
        }
    }
];

let paramAjax = function (d) {
    d.text = $('input[name="search-ajax"]').val();
};

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableProduct", "/api/products", columns, paramAjax);
