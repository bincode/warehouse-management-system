import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Role",
        data: "name",
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            var disableButton = data.id != 1;
            return table.callBasicAction(disableButton, disableButton);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableRole", "/api/roles", columns);
