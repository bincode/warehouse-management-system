import * as table from "./base-table";
import * as button from '../utils/buttons-utils';

import { InputStatusDialog, LoadingAlert, ServerErrorAlert } from '../utils/alert-utils';
import { getStatusColor } from "../status-colors";

import "../utils/daterangepicker-utils";

$(function () {
    // Call init to create the DataTable and get access to renderEditAndDeleteButtons
    const myTable = table.init("tableWorkOrder", "/api/workorders", columns, paramAjax);

    myTable.on('draw', function () {
        myTable.rows().every(function () {
            const data = this.data();
            if (!data.lot) {
                $(this.node()).addClass('bg-info text-white');
            }
        });
    });

    myTable.on('click', '.change-status', function (e) {
        e.stopPropagation();

        const id = $(this).data('id');
        const status = $(this).data('status');

        handleChangeStatus(myTable, id, status);
    });

    // Bind daterangepicker apply event to reload table when date is changed
    const searchDate = $('#tableSrcDateStart, #tableSrcDateEnd').on('apply.daterangepicker', function (ev, picker) {
        myTable.ajax.reload();
    });
});

// Column configurations for the DataTable
const columns = [
    {
        title: "Date",
        width: "5%",
        data: "date",
    },
    {
        title: "No. Lot",
        width: "15%",
        data: "lot",
        render: function (data, type, row) {
            return data ? data : renderBadge('danger', 'Not Set');
        },
    },
    {
        title: "Product Name",
        data: "formula.product.name",
    },
    {
        title: "Product Description",
        data: "formula.product.description",
    },
    {
        title: "Planning Quantity",
        width: "5%",
        class: "text-right",
        data: "plan_quantity",
        render: function (data, type, row) {
            return data + " Kg";
        },
    },
    {
        title: "Status",
        width: "10%",
        class: "text-right",
        data: null,
        render: function (data, type, row) {
            return renderBedgeStatus(data.id, data.status);
        },
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            const isDisable = data.lot > 0;
            return `${button.view(isDisable)} ${button.edit()} ${button.del()}`;
        },
    }
];

// Parameters for the AJAX request
const paramAjax = (d) => {
    const searchText = $('input[name="search-ajax"]').val();

    // Only add d.text if searchText is not empty
    if (searchText) {
        d.text = searchText;
    }

    d.start_date = $('#tableSrcDateStart').val();
    d.end_date = $('#tableSrcDateEnd').val();
};

// Render Badge
const renderBadge = (status, text) => {
    return `<span class="badge badge-${status}">${text}</span>`;
};

// Render Status Badge Status
const renderBedgeStatus = (id, status) => {
    return "<span class='badge " + getStatusColor(status) + " change-status p-2' data-id='" + id + "' data-status='" + status + "'> " + status.toUpperCase() + " </span>";
};

// Handle Change Status
const handleChangeStatus = (table, id, status) => {

    // Get the CSRF token from the meta tag
    const token = $('meta[name="csrf-token"]').attr('content');

    InputStatusDialog(status).then((result) => {
        if (result.isConfirmed) {
            const newStatus = result.value;

            LoadingAlert('Validation Stock');

            // Make an AJAX request to update the status
            $.ajax({
                url: `/api/workorders/updatestatus/${id}`, // API endpoint for updating status
                method: 'PUT',
                data: {
                    status: newStatus,
                    _token: token
                },
                success: function (response) {
                    // If any product has insufficient stock, show alert
                    if (response.some(result => result.status === false)) {
                        StockValidationAlert(response);
                    } else {
                        table.ajax.reload();
                    }
                },
                error: function (xhr, status, error) {
                    const response = xhr.responseJSON || {};
                    const errorMessage = response.message || 'An unexpected error occurred.';
                    ServerErrorAlert(errorMessage);
                }
            });
        }
    });
};
