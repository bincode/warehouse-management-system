import * as table from "./base-table";
import * as datepicker from "../daterangepicker/daterangepicker-utils";

var quantity = 0;
const columns = [{
        title: 'Date',
        data: 'date',
        width: '10%',
    },
    {
        title: 'Document Reference',
        data: 'document_reference',
    },
    {
        title: 'Description',
        data: 'description',
    },
    {
        title: 'Location',
        data: 'location',
    },
    {
        title: 'In',
        data: 'in',
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            // Calculate the total quantity
            quantity += data;

            // Format and return the formatted output
            return Number(data.toFixed(3));
        },
    },
    {
        title: 'Out',
        data: 'out',
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            // Calculate the total quantity
            quantity -= data;

            // Format and return the formatted output
            return Number(data.toFixed(3));
        },
    },
    {
        title: 'Quantity',
        data: null,
        width: '10%',
        className: "text-center",
        render: function (data, type, row) {
            return Number.parseFloat(quantity.toFixed(3));
        },
    },
];

// Extract the ID from the current URL
const id = (new URL(window.location.href).pathname.match(/\d+/) || [])[0];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init('tbProductHistory', '/api/products/' + id + '/history', columns, null, false, false);

const searchDate = datepicker.init('#tableSrcDateStart, #tableSrcDateEnd').on('change', function(e){
    myTable.draw();
});

