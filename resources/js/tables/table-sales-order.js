import * as table from "./base-table";
import * as button from '../utils/buttons-utils';

import { LoadingAlert, ServerErrorAlert } from '../utils/alert-utils';
import { getStatusColor } from "../status-colors";

import "../utils/daterangepicker-utils";

$(function () {
    // Call init to create the DataTable and get access to renderEditAndDeleteButtons
    const myTable = table.init("tableSalesOrder", "/api/salesorder", columns, paramAjax);

    myTable.on('click', 'tr', function (event) {
        var row = myTable.row(event.currentTarget);
        toggleChildRow(row, myTable);
    });

    $('#tableSrcDateStart, #tableSrcDateEnd').on('apply.daterangepicker', function (ev, picker) {
        myTable.ajax.reload();
    });

    $('select[name="status"]').on('change', function (e) {
        myTable.ajax.reload();
    });

    $('#buttonImport').on('click', function (e) {
        e.preventDefault();

        InputDateFileDialog("Upload File", "Upload").then(result => {
            if (result.isConfirmed) {
                const { date, file } = result.value;

                // Check if date and file are correctly defined
                if (!date || !file) {
                    console.log("Date or file missing.");
                    return;
                }

                // Convert the date to d/m/Y format
                const dateParts = date.split('-'); // Assuming selectedDate is in YYYY-MM-DD format
                const formattedDate = `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;

                // Create FormData to send the date and file
                const formData = new FormData();
                formData.append('date', formattedDate);
                formData.append('file', file);

                // AJAX POST request to send the form data
                $.ajax({
                    url: '/api/salesorder/import',
                    method: 'POST',
                    data: formData,
                    processData: false, // Prevent jQuery from processing the data
                    contentType: false, // Prevent jQuery from setting the content type
                    success: function (response) {
                        console.log('success');
                    },
                    error: function (xhr) {
                        console.log('failed');
                    }
                });
            }
        });
    });
});

// Column configurations for the DataTable
const columns = [
    {
        title: 'PO Date',
        data: 'date',
    },
    {
        title: 'Company Name',
        data: 'company.name',
    },
    {
        title: 'PO Number',
        data: 'po_number',
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center",
        searchable: false,
        render: function (data, type, row) {
            return `${button.edit()} ${button.del()}`;
        }
    }
];

// Parameters for the AJAX request
const paramAjax = (d) => {
    d.text = $('input[name="search-ajax"]').val();
    d.status = $('select[name="status"]').val();

    d.start_date = $('#tableSrcDateStart').val();
    d.end_date = $('#tableSrcDateEnd').val();
};

// Function to toggle child rows
const toggleChildRow = (row, table) => {
    var data = row.data();

    if (row.child.isShown()) {
        // Hide the child row and remove the class
        row.child.hide();
        row.node().classList.remove('shown');
    } else {
        // Hide any currently shown child rows
        table.rows('.shown').every(function () {
            this.child.hide();
            this.node().classList.remove('shown');
        });

        // Show the selected child row
        row.child(formatRowDetails(data)).show();
        row.node().classList.add('shown');
    }
};

// Format the data for the child row
function formatRowDetails(data) {
    let html = '<div class="row"><table class="table table-sm m-0" style="width: 100%"><tbody>';

    html += `
        <tr class="text-center table-dark">
            <td>No.</td>
            <td>Product Name</td>
            <td>Quantity</td>
            <td>Due Date</td>
            <td>Remarks</td>
            <td>Status</td>
        </tr>
    `;

    data.products.forEach((item, index) => {
        const nama = item.name;
        const quantity = item.pivot.amount;
        const ship_priority = item.pivot.ship_priority;
        const ship_date = ship_priority == 'SPECIFIED_DATE' ? item.pivot.ship_date : ship_priority;
        const remarks = item.pivot.remarks ?? "-";
        const status = item.pivot.status;

        // Get the color for the current status, default to black if status is unknown
        const statusColor = getStatusColor(status);

        // Convert status to have the first letter in uppercase
        const statusText = status.charAt(0).toUpperCase() + status.slice(1);

        html += `
            <tr>
                <td class="text-center" style="width:5%">${index + 1}.</td>
                <td><b>${nama}</b></td>
                <td class="text-right" style="width:20%">${quantity} Kg</td>
                <td class="text-center" style="width:25%">${ship_date}</td>
                <td>${remarks}</td>
                <td class="text-center" style="width:10%">
                    <span class='badge " + ${statusColor} + " change-status p-2' data-id='" + ${item.pivot.sales_order_id} + "' data-status="${status}"> ${status.toUpperCase()} </span>
                </td>
            </tr>
        `;
    });

    html += '</tbody></table></div>';
    return html;
}

// Set up click event listener for the change status buttons
$('#tableSalesOrder tbody').on('click', '.change-status', function (e) {
    e.stopPropagation();
    
    LoadingAlert();

    const salesOrderId = $(this).data('id');
    const productId = $(this).data('product-id');
    const currentStatus = $(this).data('status');

    // Show a prompt to change the status using SweetAlert
    alert.showOptionStatus(currentStatus).then((result) => {
        if (result.isConfirmed) {
            const newStatus = result.value; // Get the new status from the prompt result

            // Get the CSRF token from the meta tag
            const token = $('meta[name="csrf-token"]').attr('content');

            // Update the badge color and data attribute to reflect the new status
            const badge = $(this).closest('td').find('.change-status');
            badge.css('color', getStatusColor(newStatus)).data('status', newStatus);

            // Make an AJAX request to update the status
            $.ajax({
                url: `/api/salesorder/updatestatus/${salesOrderId}`, // API endpoint for updating status
                method: 'PUT',
                data: {
                    product: productId,
                    status: newStatus,
                    _token: token
                },
                success: function (response) {

                    // Update the status text in the UI
                    const statusCell = badge.closest('td');
                    const statusText = statusCell.find('.status-text');
                    statusText.text(response.newStatus.charAt(0).toUpperCase() + response.newStatus.slice(1));

                    // Update the badge color and data attribute with the new status
                    badge.css('color', getStatusColor(response.newStatus)).data('status', response.newStatus);

                    // Show success toast notification
                    SuccessToast('Status Change Success');
                },
                error: function (xhr, status, error) {
                    const response = xhr.responseJSON || {};
                    const errorMessage = response.message || 'An unexpected error occurred.';
                    ServerErrorAlert(errorMessage);

                    // Revert the badge color and data attribute to the original status on error
                    badge.css('color', getStatusColor(currentStatus)).data('status', currentStatus);
                }
            });
        }
    });
});
