import * as table from "./base-table";

// Define the column configurations for the DataTable
const columns = [{
        title: "Company Name",
        data: "name",
        className: 'align-middle',
    },
    {
        title: 'Address',
        data: 'address',
        className: 'text-wrap align-middle',
    },
    {
        title: 'Type',
        data: 'type',
        width: '10%',
        className: 'text-nowrap text-center align-middle',
        render: function (data, type, row) {
            var badges = '';

            if (data.includes('c')) {
                badges += '<small class="badge badge-primary m-1"> Customer </small>';
            }
            if (data.includes('s')) {
                badges += '<small class="badge badge-warning m-1"> Supplier </small>';
            }
            return badges;
        },
    },
    {
        title: "Actions",
        data: null,
        width: "5%",
        class: "text-center align-middle",
        searchable: false,
        render: function (data, type, row) {
            return table.callBasicAction(true, true);
        }
    }
];

// Call init to create the DataTable and get access to renderEditAndDeleteButtons
const myTable = table.init("tableRelation", "/api/relations", columns);
