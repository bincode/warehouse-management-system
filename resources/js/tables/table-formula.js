import * as table from "./base-table";
import * as button from '../utils/buttons-utils';
import { SuccessToast, ServerErrorAlert } from '../utils/alert-utils';

$(function () {
    // Initialize the DataTable
    const myTable = table.init("tableFormula", "/api/formulas", columns, paramAjax);

    // Event listener for switch toggle
    $('#tableFormula').on('change', '.switch-active', handleSwitchToggle);
});

// Column configurations for the DataTable
const columns = [
    {
        title: "Active",
        data: "is_active",
        class: "text-center",
        width: "5%",
        render: (data, type, row) => renderSwitch(row.id, data),
    },
    {
        title: "Product",
        data: "product.name",
    },
    {
        title: "Description",
        data: "product.description",
    },
    {
        title: "Version",
        data: "version",
        class: "text-center",
    },
    {
        title: "Actions",
        data: null,
        class: "text-center",
        width: "5%",
        render: function (data, type, row) {
            const isDisable = data.workorders_count < 1;
            return `${button.edit()} ${button.del(isDisable)}`;
        },
    },
];

// Parameters for the AJAX request
const paramAjax = (d) => {
    d.text = $('input[name="search-ajax"]').val();
};

// Render switch for active status
const renderSwitch = (id, status) => {
    const safeId = escapeHTML(id.toString());
    const checked = status ? 'checked' : '';
    return `
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input switch-active" data-id="${safeId}" id="switch-${safeId}" ${checked}>
            <label class="custom-control-label" for="switch-${safeId}"></label>
        </div>
    `;
};

// Utility function to escape HTML for security
const escapeHTML = (str) => str.replace(/</g, "&lt;").replace(/>/g, "&gt;");

// Function to handle the switch toggle event
const handleSwitchToggle = (e) => {
    const checkbox = e.target;
    const id = checkbox.getAttribute('data-id');
    const apiUrl = `/api/formulas/${id}/activated`;

    // Store the previous state of the checkbox
    const previousState = checkbox.checked;

    // Get the CSRF token
    const csrfToken = $('meta[name="csrf-token"]').attr('content');

    // Make the AJAX request
    $.ajax({
        url: apiUrl,
        method: 'POST',
        headers: { 'X-CSRF-Token': csrfToken },
        success: (response) => SuccessToast(response.message),
        error: (xhr) => {
            const response = xhr.responseJSON || {};
            const errorMessage = response.message || 'An unexpected error occurred.';
            ServerErrorAlert(errorMessage);

            // Revert checkbox state if there was an error
            checkbox.checked = !previousState;
        }
    });
};
