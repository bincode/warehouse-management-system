import * as select from '../utils/select2-utils';
import * as table from '../forms/table-input/table-input-utils';

// Re-initialize the table with Select2 configs after the response
table.init('table-input-materials', select.materialsSelectConfig, select.rackSelectConfig);

// Call the function when the page is ready
$(function () {
    function handleAjax() {

        // Extract the domain from the current URL
        const domain = window.location.origin;

        // Get the selected formula ID and plan quantity from the respective elements
        const lot = $('input[name="for"]').val();

        // Return if lot character less then 8
        if (lot.length < 7) return;

        $('#fragmentContainer').html('<div class="text-center m-3"><i class="fas fa-spinner fa-spin"></i> Loading...</div>');

        // Construct the API endpoint URL
        const apiUrl = `${domain}/form/material-release/${lot}`;

        $.ajax({
            url: apiUrl,
            method: 'GET',
            success: function (response) {
                // Decode the base64 content
                $('#fragmentContainer').html(atob(response.content));

                // Reinitialize the DataTable safely
                if ($.fn.DataTable.isDataTable('#table-input-materials')) {
                    $('#table-input-materials').DataTable().destroy(); // Destroy the existing DataTable instance
                }

                // Re-initialize the table with Select2 configs after the response
                table.init('table-input-materials', select.materialsSelectConfig, select.rackSelectConfig);
                

                if (response.product) {
                    $('select[name="description"]').append('<option selected="selected" value="' + response.product.id + '">' + response.product.name + '</option>');

                    handleTotalCalculation();
                }
                
                $('input[name="amount[]"]').on('input', handleTotalCalculation);
            }
        });
    }

    function handleTotalCalculation() {
        const amounts = $("input[name='amount[]']");
        let totalAmount = 0;

        amounts.each(function () {
            var value = $(this).val() ? parseFloat($(this).val()) : 0;
            totalAmount += value;
        });

        // Update the total weight and percentage labels
        var $totalLabel = $("label[for='total']");
        if ($totalLabel.length) {
            $totalLabel.text("Material Weight : " + totalAmount + " Kg");
        }
    }

    handleTotalCalculation();

    $('input[name="amount[]"]').on('input', handleTotalCalculation);
    $('input[name="for"]').on('input', handleAjax);
});
