import * as dialog from '../utils/alert-utils';
import * as select from '../utils/select2-utils';
import * as table from '../forms/table-input/table-input-utils';

table.init('table-input-formula', select.materialsSelectConfig, select.availableRackSelectConfig);

$(function () {
    handlePercentageCalculation();
});

$(document).on('input', 'input[name="percentage[]"]', handlePercentageCalculation);

function handlePercentageCalculation() {

    const percentage = $("input[name='percentage[]']");
    let totalPercentage = 0;

    percentage.each(function () {
        var value = $(this).val() ? parseFloat($(this).val()) : 0;
        totalPercentage += value;
    });

    let $label = $("label[for='totalPercentage']");
    if ($label.length) {
        // Format angka menjadi 4 angka di belakang koma
        let displayPercentage = totalPercentage.toFixed(4).replace(/\.?0+$/, ""); // Hilangkan trailing 0 jika tidak perlu
        $label.text("Total Percentage : " + displayPercentage + " %");

        // Ubah status tombol Save berdasarkan totalPercentage
        $('#saveButton')
            .prop('disabled', displayPercentage !== '100')
            .toggleClass('btn-primary', displayPercentage === '100')
            .toggleClass('btn-secondary', displayPercentage !== '100');
    }
}


// Handle form submission
$('form').on('submit', function (e) {
    // Check if the form method is POST
    if ($(this).attr('method').toLowerCase() === 'post') {
        // Prevent the default form submission
        e.preventDefault();

        // Show confirmation dialog to the user
        dialog.ConfirmationActivatedFormula().then((result) => {
            // If the user confirms, add a hidden input to mark the formula as active and submit the form
            if (result.isConfirmed) {
                $('<input>').attr({ type: 'hidden', name: 'is_active', value: 1 }).appendTo('form');
                e.target.submit();
            }
            // If the user denies, submit the form without marking it as active
            else if (result.isDenied) {
                e.target.submit();
            }
        });
    }
});