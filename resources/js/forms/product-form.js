import '../utils/select2-utils';
import '../utils/daterangepicker-utils';

$(function () {
    // Function to handle showing/hiding color input
    function handleCategoryChange() {
        const selectedCategory = $('select.inventory-type').val();
        const colorInput = $('input[name="color"]');
        const colorFormGroup = colorInput.closest('.form-group');

        if (selectedCategory > 1) {
            colorFormGroup.show();
            colorInput.prop('disabled', false); // Enable the input
        } else {
            colorFormGroup.hide();
            colorInput.prop('disabled', true); // Disable the input
        }
    }

    // Trigger the function when the category changes
    $(document).on('change', 'select.inventory-type', handleCategoryChange);

    // Trigger the function when the page loads (for edit forms)
    handleCategoryChange();
});
