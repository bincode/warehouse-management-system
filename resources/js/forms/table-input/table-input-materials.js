import * as select from '../../utils/select2-utils';
import * as table from './table-input-utils';

// Call the initialize function with the necessary arguments
table.init('table-input-materials', select.materialsSelectConfig, select.availableRackSelectConfig);
table.init('table-input-goods', select.goodsSelectConfig, select.rackSelectConfig);
table.init('table-input-products', select.goodsSelectConfig, select.rackSelectConfig);
