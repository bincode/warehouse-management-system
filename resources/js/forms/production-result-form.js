import * as select from '../utils/select2-utils';
import * as table from '../forms/table-input/table-input-utils';

// Re-initialize the table with Select2 configs after the response
table.init('table-input-goods', select.productBySelectorConfig, select.rackSelectConfig);

// Call the function when the page is ready
$(function () {

    function handleAjax() {
        // Extract the domain from the current URL
        const domain = window.location.origin;

        // Get the selected formula ID and plan quantity from the respective elements
        const lot = $('input[name="for"]').val();

        // Construct the API endpoint URL
        const apiUrl = `${domain}/api/releases?text=${lot}`;

        if (lot.length >= 8) {
            // Send an AJAX GET request to the API endpoint
            $.ajax({
                url: apiUrl,
                method: 'GET',
                success: function (response) {
                    const data = response.data[0];

                    // Only update input values after a successful response
                    if (data.product && data.product.description) {
                        $('input[name="description"]').val(data.product.description);
                    }

                    if (data.consumption !== undefined) {
                        // Set the consumption value with rounding
                        $('input[name="consumption"]').val(parseFloat(data.consumption.toFixed(4)));
                    }

                    // Call handleLossCalculation after consumption and output are set
                    handleLossCalculation();

                    // Reinitialize the DataTable safely
                    if ($.fn.DataTable.isDataTable('#table-input-goods')) {
                        $('#table-input-goods').DataTable().destroy(); // Destroy the existing DataTable instance
                    }

                    // Re-initialize the table with Select2 configs after the response
                    table.init('table-input-goods', select.productBySelectorConfig, select.rackSelectConfig);


                    $('input[name="amount[]"]').on('change', handleTotalCalculation);
                },
                error: function () {
                    // Error handling if the AJAX request fails
                }
            });
        }
    }

    function handleLossCalculation() {
        const consumption = parseFloat($('input[name="consumption"]').val()) || 0;
        const output = parseFloat($('input[name="output"]').val()) || 0;

        // Round both consumption and output to 4 decimal places
        const roundedConsumption = parseFloat(consumption.toFixed(4));
        const roundedOutput = parseFloat(output.toFixed(4));

        // Calculate the loss based on the rounded values
        const loss = parseFloat((roundedConsumption - roundedOutput).toFixed(4));

        // Update the loss input field
        $('input[name="loss"]').val(loss);
    }

    function handleTotalCalculation() {
        const amounts = $("input[name='amount[]']");
        let totalAmount = 0;

        amounts.each(function () {
            var value = $(this).val() ? parseFloat($(this).val()) : 0;
            totalAmount += value;
        });

        let $output = $("input[name='output']");
        if ($output.length) {
            $output.val(totalAmount).trigger('change');
        }

        handleLossCalculation();
    }

    handleAjax();
    handleTotalCalculation();

    $('input[name="for"]').on('input', handleAjax);
});

