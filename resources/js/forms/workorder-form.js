import * as select from '../utils/select2-utils';
import * as table from '../forms/table-input/table-input-utils';
import * as alert from '../utils/alert-utils';

import '../utils/daterangepicker-utils';

// Call the function when the page is ready
$(function () {
    handleCreateFormula();
    updateAllShipDateInputs();

    $('.formula-active').on('select2:select', handleCreateFormula);

    $('#checkstock').on('click', function (e) {
        e.preventDefault();
        const method = 'POST';

        // Show loading alert while validating stock
        alert.LoadingAlert("Validating Stock");

        // Extract the domain from the current URL
        const domain = window.location.origin;

        // Construct the API endpoint URL
        const apiUrl = `${domain}/api/products/validationstock`;

        // Array to store products
        var products = [];
        var workorders = [];

        var workOrderId = $('input[name="id"]').val();
        if (workOrderId) {
            workorders.push(workOrderId);
        }

        $('#table-input-work-order').find('tr').each(function () {
            const productId = $(this).find('select[name="products[]"]').val();
            const quantity = $(this).find('input[name="amount[]"]').val();
            products.push({
                id: productId,
                quantity: quantity
            });
        });

        $.ajax({
            url: apiUrl,
            method: method,
            data: { products, workorder: workorders },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                let htmlContent = '<div class="text-left">';

                // Loop through each item in the response array
                response.forEach(item => {
                    let stockColor = item.status ? 'green' : 'red';
                    let formattedStock = parseFloat(item.avStock).toFixed(4).replace(/\.?0+$/, '');
                    let formattedRvStock = parseFloat(item.rvStock).toFixed(4).replace(/\.?0+$/, '');

                    // Construct HTML content for each item
                    htmlContent += `<p><span style="background-color: ${stockColor}; padding: 2px 5px; color: white;">${item.message}</span> : <strong>${item.product}</strong>, Avl: <span style="color: ${stockColor};">${formattedStock}</span>, Rsv: <span style="color: orange;">${formattedRvStock}</span></p>`;
                    htmlContent += '<hr>';
                });

                htmlContent += '</div>';

                alert.StockValidationAlert(htmlContent);
            },
            error: function (xhr, status, error) {
                alert.ServerErrorAlert(error);
            }
        });
    });
});

// Event listener for change event on ship_priority select elements
$(`select[name="ship_priority"]`).on('change', function () {
    toggleShipDateInput($(this));
});

// Function to enable or disable ship_date input based on the ship_priority value
function toggleShipDateInput(selectElement) {
    const selectedValue = selectElement.val();
    const shipDateInput = $('input[name="ship_date"]');

    if (selectedValue === 'SPECIFIED_DATE') {
        shipDateInput.removeAttr('disabled');
    } else {
        shipDateInput.prop('disabled', true);
    }
}

// Function to check and set the state of ship_date input fields for all rows
function updateAllShipDateInputs() {
    $(`select[name="ship_priority"]`).each(function () {
        toggleShipDateInput($(this));
    });
}

function handleCreateFormula() {
    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Get the selected formula ID and plan quantity from the respective elements
    const id = $('select[name="formula_id"]').val();
    const quantity = $('input[name="plan_quantity"]').val();

    // Construct the API endpoint URL
    const apiUrl = `${domain}/form/work-order/${id}?quantity=${quantity}`;

    // Show loading message while fetching data
    if (id) {
        $('#fragmentContainer').html('<div class="text-center m-3"><i class="fas fa-spinner fa-spin"></i> Loading...</div>');

        // Send an AJAX GET request to the API endpoint
        $.ajax({
            url: apiUrl,
            method: 'GET',
            success: function (response) {
                // Insert the response HTML into the element with ID "fragmentContainer"
                $('#fragmentContainer').html(atob(response.content));

                // Remove the and align-items-center classes  
                $('.col').removeClass('align-items-center');

                const isColorAvailable = !!response.product.color;
                $('.product-color')
                    .val(isColorAvailable ? response.product.color : '')
                    .prop('readonly', isColorAvailable);

                // Initialize table and select components
                table.init('table-input-work-order', select.materialsSelectConfig, select.availableRackSelectConfig);

                // Check for adjustments in work orders  
                const totalWorkOrders = response.totalWorkOrders; // Assuming this is returned in the response  
                const totalAdjustments = response.totalAdjustments; // Assuming this is returned in the response

                // Show warning if there are adjustments  
                if (totalAdjustments > 0) {
                    const percentage = ((totalAdjustments / totalWorkOrders) * 100).toFixed(2);
                    const message = `This formula has previously undergone adjustments.<br>  
                    Total Adjustments: <span style="color: red;">${totalAdjustments}</span> out of   
                    <span style="color: red;">${totalWorkOrders}</span> work orders   
                    (<span style="color: red;">${percentage}%</span>).<br>  
                    Please review the adjustments before proceeding.`;

                    alert.ServerWarningAlert('Attention: Previous Adjustments Detected', message);
                }

                // Helper function to calculate quantity based on percentage, formatted to 3 decimal places
                function calculateQuantityByPercentage(planQuantity, percentage) {
                    return (planQuantity * (percentage / 100)).toFixed(4).replace(/\.?0+$/, '');
                }

                // Helper function to calculate percentage based on quantity, formatted to 3 decimal places
                function calculatePercentageByQuantity(planQuantity, quantity) {
                    return ((quantity / planQuantity) * 100).toFixed(4).replace(/\.?0+$/, '');
                }

                // Helper function to update total percentage display
                function updateTotalPercentage() {
                    let totalPercentage = 0;
                    $('input[name="percentage[]"]').each(function () {
                        totalPercentage += parseFloat($(this).val()) || 0;
                    });
                    $('label[for="totalPercentage"]').text("Total Percentage : " + totalPercentage.toFixed(4).replace(/\.?0+$/, '') + "%");

                    // Enable submit button only if totalPercentage is exactly 100%
                    const isPercentageComplete = totalPercentage.toFixed(4).replace(/\.?0+$/, '') === '100';
                    $('#submitwo').prop('disabled', !isPercentageComplete)
                        .toggleClass('btn-primary', isPercentageComplete)
                        .toggleClass('btn-secondary', !isPercentageComplete);
                }

                // Event handler for changes to plan quantity or percentages
                function updateQuantities() {
                    const planQuantity = parseFloat($('input[name="plan_quantity"]').val()) || 0;

                    $('input[name="percentage[]"]').each(function (index) {
                        const isLastElement = index === $('input[name="percentage[]"]').length - 1;

                        // Clear amount for the last element, otherwise calculate
                        const $amount = $(this).closest('tr').find('input[name="amount[]"]');
                        if (isLastElement) {
                            $amount.val('');
                        } else {
                            const percentage = parseFloat($(this).val()) || 0;
                            $amount.val(calculateQuantityByPercentage(planQuantity, percentage));
                        }
                    });

                    updateTotalPercentage(); // Update total percentage display
                }

                // Event handler for changes to individual percentage
                $('input[name="percentage[]"]').on('keyup', function () {
                    const planQuantity = parseFloat($('input[name="plan_quantity"]').val()) || 0;
                    const percentage = parseFloat($(this).val()) || 0;
                    const $amount = $(this).closest('tr').find('input[name="amount[]"]');

                    $amount.val(calculateQuantityByPercentage(planQuantity, percentage));
                    updateQuantities();
                });

                // Event handler for changes to individual amount
                $('input[name="amount[]"]').on('keyup', function () {
                    const planQuantity = parseFloat($('input[name="plan_quantity"]').val()) || 0;
                    const quantity = parseFloat($(this).val()) || 0;
                    const $percentage = $(this).closest('tr').find('input[name="percentage[]"]');

                    $percentage.val(calculatePercentageByQuantity(planQuantity, quantity));
                    updateQuantities();
                });

                // Event handler for changes to plan quantity
                $('input[name="plan_quantity"]').on('keyup', updateQuantities);

                updateTotalPercentage();
            },
            error: function () {
                // Error handling if the AJAX request fails
            }
        });
    }
}