import Swal from 'sweetalert2';

// Displays a confirmation this for deleting an item
export const showDeleteConfirmation = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, delete it!'
    });

// Displays an alert for a failed delete operation, with an error message from the server response
export const showDeleteFailedAlert = (response) =>
    Swal.fire({
        title: "Delete Failed",
        text: response.message,
        icon: 'warning',
    });

// Displays a success alert after a successful delete operation
export const showSuccessAlert = () =>
    Swal.fire({
        title: 'Success!',
        text: 'Your file has been saved.',
        icon: 'success'
    });

// Displays a success alert after a successful delete operation
export const showDeleteSuccessAlert = () =>
    Swal.fire({
        title: 'Deleted!',
        text: 'Your file has been deleted.',
        icon: 'success'
    });

// Displays an alert for a server error
export const showServerErrorAlert = (message = "Please contact your administrator.") =>
    Swal.fire({
        title: "Server Error",
        text: message,
        icon: 'question',
    });

export const showHTMLServerErrorAlert = (message = "Please contact your administrator.") =>
    Swal.fire({
        title: "Server Error",
        html: message,
        icon: 'question',
    });

export const showCreateNewFormulaConfirmation = () =>
    Swal.fire({
        title: 'Confirmation',
        text: 'The entered formula has changes or additions. Do you want to create a new formula?',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    });

// Displays an alert for loading and validating
export const showLoadingAlert = (message = "loading") =>
    Swal.fire({
        title: "Loading",
        html: "Please wait while " + message + " ....",
        allowOutsideClick: false,
        showCancelButton: false,
        showConfirmButton: false,
        didOpen: () => {
            Swal.showLoading(); // Show loading spinner
        }
    });

// Function to display an alert with product information and stock status
export function showStockValidationAlert(response) {
    let htmlContent = '<div class="text-left">';

    // Loop through each item in the response array
    response.forEach(item => {
        let stockColor = item.status ? 'green' : 'red';
        let formattedStock = parseFloat(item.avStock).toFixed(2);
        let formattedRvStock = parseFloat(item.rvStock).toFixed(2);

        // Construct HTML content for each item
        htmlContent += `<p><span style="background-color: ${stockColor}; padding: 2px 5px; color: white;">${item.message}</span> : <strong>${item.product}</strong>, Avl: <span style="color: ${stockColor};">${formattedStock}</span>, Rsv: <span style="color: orange;">${formattedRvStock}</span></p>`;
        htmlContent += '<hr>';
    });

    htmlContent += '</div>';

    // Display SweetAlert modal with stock validation information
    Swal.fire({
        title: "Stock Validation",
        html: htmlContent,
        icon: 'info'
    });
}

export const showFileUploadAlert = () =>
    Swal.fire({
        title: "Upload File",
        text: "Pilih file yang ingin Anda unggah:",
        input: "file",
        showCancelButton: true,
        confirmButtonText: "Upload",
        inputAttributes: {
            accept: ".xls,.xlsx"
        }
    })

export const showOptionStatus = (currentStatus) =>
    Swal.fire({
        title: 'Change Status',
        input: 'select',
        inputOptions: {
            'continue': 'Continue',
            'hold': 'Hold',
            'canceled': 'Canceled'
        },
        inputValue: currentStatus,
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value) {
                return 'You need to choose a status!';
            }
        }
    });

export const showSuccessToast = (message) =>
    Swal.fire({
        icon: 'success',
        title: message,
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
    });

export const showInput = (data) =>
    Swal.fire({
        title: 'Add Packing List',
        html: `
            <div class="col">
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="dateInput" class="form-label">Date:</label>
                    </div>
                    <div class="col-8">
                        <input id="dateInput" type="date" class="form-control" placeholder="Select date" value="${data.now}" required>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="lotInput" class="form-label">No. Lot:</label>
                    </div>
                    <div class="col-8">
                        <input id="lotInput" class="form-control" placeholder="Enter No. Lot" value="${data.lot}" required>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="quantityInput" class="form-label">Quantity:</label>
                    </div>
                    <div class="col-6">
                        <input id="quantityInput" type="number" class="form-control" placeholder="Enter quantity" value="${data.amount}" required>
                    </div>
                    <div class="col-2">
                        <strong> Kg </strong>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="bagInput" class="form-label">Bag:</label>
                    </div>
                    <div class="col-6">
                        <input id="bagInput" type="text" class="form-control" placeholder="Enter bag" value="${data.amount / 25 }" required>
                    </div>
                    <div class="col-2">
                        <strong> Zak </strong>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="document" class="form-label">Document:</label>
                    </div>
                    <div class="col-8">
                        <select id="document" class="form-control" required>
                            <option value="Nothing" ${data.document === 'Nothing' ? 'selected' : ''}>Nothing</option>
                            <option value="COA" ${data.document === 'COA' ? 'selected' : ''}>COA</option>
                            <option value="Color Book" ${data.document === 'Color Book' ? 'selected' : ''}>Color Book</option>
                            <option value="COA+CB" ${data.document === 'COA+CB' ? 'selected' : ''}>COA+CB</option>
                        </select>
                    </div>
                </div>
                <div class="row m-3">
                    <div class="col-4 text-left align-center">
                        <label for="noteInput" class="form-label">Note:</label>
                    </div>
                    <div class="col-8">
                        <input id="noteInput" type="text" class="form-control" placeholder="Note" value="${data.note ? data.note : ''}">
                    </div>
                </div>
            </div>
        `,
        reverseButtons: true,
        preConfirm: () => {
            return {
                date: $('#dateInput').val(),
                lot: $('#lotInput').val(),
                quantity: $('#quantityInput').val(),
                bag: $('#bagInput').val(),
                document: $('#document').val(),
                note: $('#noteInput').val()
            };
        }
    });

export const showDeliveredConfirmation = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "The delivery order will be created and deducted directly from the available stock.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, create the transaction!'
    });

export const showInputReferenceNumber = () =>
    Swal.fire({
        title: 'Enter Document Number',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Submit',
        reverseButtons: true,
    });

export const showFormulaActiveConfirmation = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to display this formula for the production section?",
        icon: 'warning',
        showCancelButton: true,
        showDenyButton: true,
        confirmButtonText: 'Yes, display it!',
        denyButtonText: 'No, only for development',
        cancelButtonText: 'Cancel'
    });

export const showPreview = (orderId) =>
    Swal.fire({
        title: 'Choose an action',
        showCancelButton: true,
        confirmButtonText: '<i class="fas fa-search"></i> Preview',
        cancelButtonText: 'Status Option',
        target: $('.fullscreen-container')[0],
    });

export const showOptionWorkOrderStatus = () =>
    Swal.fire({
        title: 'Select Status Workorder',
        input: 'select',
        inputOptions: {
            'continue': 'Continue',
            'running': 'Running',
            'finished': 'Finished',
            'hold': 'Hold',
            'cancel': 'Cancel'
        },
        inputPlaceholder: 'Select a status',
        showCancelButton: true,
        target: $('.fullscreen-container')[0],
    });

export const showInputWorkOrderQuantity = () =>
    Swal.fire({
        title: 'Enter Quantity',
        input: 'number',
        inputPlaceholder: 'Enter Quantity',
        showCancelButton: true,
    });

    export const showHTML = (title, htmlContent) => {
        Swal.fire({
            title: title,
            html: htmlContent,
            showConfirmButton: false,
            target: $('.fullscreen-container')[0],
        });
    }
