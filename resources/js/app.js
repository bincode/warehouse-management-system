import './bootstrap';

// Import jQuery
window.$ = window.jQuery = require('jquery');

// Import AdminLTE
import 'admin-lte';
import printJS from 'print-js';

// Tangkap event keydown (tombol ditekan) di seluruh halaman
document.addEventListener("keydown", function (event) {
    // Jika pengguna menekan Ctrl+P
    if (event.ctrlKey && event.key === "p") {
        // Mencegah tindakan bawaan Ctrl+P (yaitu, pencetakan default browser)
        event.preventDefault();

        // Periksa apakah elemen dengan ID "kanban-container" ada di halaman
        const kanbanContainer = document.getElementById("kanban-container");

        if (kanbanContainer) {
            // Jika elemen ditemukan, mulai pencetakan menggunakan Print.js
            printJS({
                printable: "kanban-container", // ID elemen yang ingin dicetak
                type: "html",
                css: '/css/print.css',
                scanStyles: false
                // Tambahkan opsi tambahan sesuai kebutuhan
            });
        } else {
            // Jika elemen tidak ditemukan, gunakan metode pencetakan default browser
            window.print();
        }
    }
});
