// Importing required modules
import '../tables/base-table';
import * as select from '../selects/select2-config';
import * as datepicker from '../daterangepicker/daterangepicker-utils';

/**
 * Initialize the table with Select2 on the specified select elements.
 */
export function init(tableId, productConfig, rackConfig = null) {

    // Get the product select element
    const $product = $(`select[name="products[]"]`);

    // Initialize Select2 on the initial select element
    initSelect($product, productConfig, rackConfig);
    calculateTotal();

    // Create a DataTable instance for the table
    const table = $(`#${tableId}`).DataTable({
        paging: false,
        order: false,
        info: false,
        lengthChange: false,
        searching: false
    });

    // Event listener for change event on table cells
    $(`#${tableId} tbody`).on('change', 'td', function (e) {
        const $target = $(e.target);
        console.log($target);

        // Check if the changed element is within a shipdate input field
        if (!$target.is('select[name="products[]"], input[name="percentage[]"], input[name="amount[]"]')) {
            return;
        }

        calculateTotal($target.is('input[name="amount[]"]'));

        // Get the row associated with the changed cell
        const row = table.row(this);

        // Get the total number of records in the table
        const totalRecords = table.page.info().recordsTotal;

        // Get the product select element within the row
        const $product = row.nodes().to$().find(`select[name="products[]"]`);

        // Get the index of the row
        const rowIndex = row.index();

        // Check if it's the last row in the table
        if (rowIndex === totalRecords - 1) {

            // Get the delete button within the row
            const $currentButtonDelete = row.nodes().to$().find('button[id=btn_delete_list]');

            // Enable the delete button and update its classes
            $currentButtonDelete.removeAttr('disabled').addClass("btn-danger").removeClass("btn-secondary");

            // Clone the current row
            const $clonedRow = row.node().cloneNode(true);

            // Add the cloned row to the table and redraw it
            const $nextRow = table.row.add($clonedRow).draw();

            // Get the delete button within the row
            const $nextButtonDelete = $nextRow.nodes().to$().find('button[id=btn_delete_list]');

            // Disable the delete button and update its classes
            $nextButtonDelete.attr('disabled', true).addClass('btn-secondary').removeClass('btn-danger');

            // Remove any previous Select2 elements from the cloned row
            const $nextProduct = $nextRow.nodes().to$().find(`select[name="products[]"]`);

            $nextProduct.next("span.select2").last().remove();
            $nextProduct.val(0);

            // Reinitialize Select2 for the cloned row
            initSelect($nextProduct, productConfig, rackConfig);

            // Initialize datepicker for the cloned row
            datepicker.init($nextRow.nodes().to$().find('.datepicker'));
        }

        // Reinitialize Select2 for the current row
        initSelect($product, productConfig, rackConfig);
    });

    // Event listener for click event on delete button
    $(`#${tableId} tbody`).on('click', 'button[id="btn_delete_list"]', function () {
        table.row($(this).parents('tr'))
            .remove()
            .draw();

        calculateTotal();
    });
}

/**
 * Initializes Select2 for the provided product select element.
 */
function initSelect($product, productConfig, rackConfig) {

    // Reload Select2 instance with updated product configuration
    select.reload($product, productConfig);

    // Check if rackConfig is provided
    if (rackConfig !== null) {
        // Get the location select element
        let $location = $product.closest('tr').find('select[name="racks[]"]');

        // Reload Select2 instance with updated rack configuration
        $location.next("span.select2").last().remove();
        select.reload($location, rackConfig);

        // Set focus on the product select element after selection
        $product.on('select2:select', function (e) {
            $(this).trigger('focus');
        });

        // Ensure focus on the first product select element in the row
        $product.first().focus();

        // Set tabindex for product select elements to allow tab navigation
        $product.attr('tabindex', 0);
        $location.attr('tabindex', 0);

        // Handle tab navigation between product select elements
        $product.on('keydown', function (e) {
            if (e.key === 'Tab' && !e.shiftKey) {
                e.preventDefault();

                // Find the next element with class .input-focus
                const $currentElement = $(this);
                const $nextFocusElement = $currentElement.closest('tr').nextAll().find('.input-focus').first();

                if ($nextFocusElement.length) {
                    // Focus on the next element with class .input-focus
                    $nextFocusElement.focus();
                    // Open the Select2 dropdown for better user experience
                    $nextFocusElement.select2('open');
                } else {
                    // Optionally handle cases where no next focusable element is found
                    console.log("No next focusable element found");
                }
            }
        });
    }
}

function calculateTotal(isAmountChange = true) {
    var totalAmount = 0;
    var totalPercentage = 0;
    var amountInputs = $("input[name='amount[]']");
    var percentageInputs = $("input[name='percentage[]']");

    // Check if amount inputs exist before performing calculations
    if (isAmountChange && amountInputs.length > 0) {
        // Calculate total amount
        amountInputs.each(function () {
            var value = $(this).val() ? parseFloat($(this).val()) : 0;
            totalAmount += value;
        });

        // Calculate total percentage
        if (totalAmount > 0) {
            percentageInputs.each(function () {
                var amount = parseFloat($(this).closest('tr').find('input[name="amount[]"]').val()) || 0;
                var percentage = (amount / totalAmount) * 100;
                $(this).val(percentage.toFixed(3));
                totalPercentage += percentage;
            });
        }

        // Adjust the total percentage to 100%
        if (percentageInputs.length > 0) {
            var adjust = 100 - totalPercentage;
            if (adjust !== 0) {
                var lastInput = percentageInputs.last();
                var lastValue = parseFloat(lastInput.val()) || 0;
                lastInput.val((lastValue + adjust).toFixed(3));
            }
        }
    }

    var totalPercentage = 0;

    percentageInputs.each(function () {
        var value = $(this).val() ? parseFloat($(this).val()) : 0;
        $(this).val(value);
        totalPercentage += value;
    });

    // Update the total weight and percentage labels
    var $totalLabel = $("label[for='total']");
    if ($totalLabel.length) {
        $totalLabel.text("Material Weight : " + totalAmount + " Kg");
    }

    var $percentageLabel = $("label[for='totalPercentage']");
    if ($percentageLabel.length) {
        $percentageLabel.text("Total Percentage : " + totalPercentage.toFixed(3) + " %");
        $('#saveButton').prop('disabled', totalPercentage.toFixed(3) < 100).toggleClass('btn-primary', totalPercentage.toFixed(3) === 100).toggleClass('btn-secondary', totalPercentage.toFixed(3) < 100);
    }

    var $output = $("input[name='output']");
    if ($output.length) {
        $output.val(totalAmount).trigger('change');
    }
}
