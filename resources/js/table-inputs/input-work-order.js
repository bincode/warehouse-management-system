import * as select from '../selects/select2-config';
import * as table from './table-input-utils';

// Call the initialize function with the necessary arguments
table.init('table-input-work-order', select.materialsSelectConfig, select.availableRackSelectConfig);
