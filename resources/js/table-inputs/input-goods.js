import * as select from '../selects/select2-config';
import * as table from '../table-inputs/table-input-utils';

// Call the initialize function with the necessary arguments
table.init('table-input-goods', select.goodsSelectConfig, select.rackSelectConfig);
