import * as select from '../selects/select2-config';
import * as table from './table-input-utils';

// Call the initialize function with the necessary arguments
table.init('input-sales-order', select.goodsSelectConfig, select.availableRackSelectConfig);

// Function to enable or disable ship_date input based on the ship_priority value
function toggleShipDateInput(selectElement) {
    const selectedValue = selectElement.val();
    const shipDateInput = selectElement.closest('tr').find('input[name="ship_date[]"]');

    if (selectedValue === 'SPECIFIED_DATE') {
        shipDateInput.removeAttr('disabled');
    } else {
        shipDateInput.prop('disabled', true);
    }
}

// Function to check and set the state of ship_date input fields for all rows
function updateAllShipDateInputs() {
    $(`#input-sales-order tbody select[name="ship_priority[]"]`).each(function () {
        toggleShipDateInput($(this));
    });
}

// Event listener for change event on ship_priority select elements
$(`#input-sales-order tbody`).on('change', 'select[name="ship_priority[]"]', function () {
    toggleShipDateInput($(this));
});

// Call updateAllShipDateInputs on page load
$(document).ready(function () {
    updateAllShipDateInputs();
});
