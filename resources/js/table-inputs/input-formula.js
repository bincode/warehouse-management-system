import * as select from '../selects/select2-config';
import * as alert from '../alerts/alert-utils';
import * as table from './table-input-utils';

// Initialize the table input with the specified configurations
table.init('table-input-formula', select.materialsSelectConfig, select.availableRackSelectConfig);

// Handle form submission
$('form').on('submit', function (e) {
    // Check if the form method is POST
    if ($(this).attr('method').toLowerCase() === 'post') {
        // Prevent the default form submission
        e.preventDefault();

        // Show confirmation alert to the user
        alert.showFormulaActiveConfirmation().then((result) => {
            // If the user confirms, add a hidden input to mark the formula as active and submit the form
            if (result.isConfirmed) {
                $('<input>').attr({ type: 'hidden', name: 'is_active', value: 1 }).appendTo('form');
                e.target.submit();
            }
            // If the user denies, submit the form without marking it as active
            else if (result.isDenied) {
                e.target.submit();
            }
        });
    }
});
