/**
 * Render Button Action
 *
 * @param {string} id
 * @param {string} color
 * @param {string} icon
 * @param {boolean} disabled
 * @param {string} label
 * @returns string
 */
export function render(id, color, icon, disabled, label = '') {
    // Determine the button color based on whether it is allowed or not
    const buttonColor = disabled ? color : "btn-secondary";

    // Determine the button class based on whether it is allowed or not
    const buttonClass = disabled ? "" : "disabled";

    // Create the tooltip attribute if a label is provided
    const tooltip = label ? `data-toggle="tooltip" data-placement="bottom" title="${label}"` : '';

    // Construct the HTML string for the button element
    return `<button id="${id}" class="btn btn-sm mr-1 ${buttonColor}" ${buttonClass} ${tooltip}><i class="${icon}"></i></button>`;
}

/**
 * Create edit button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function edit(disabled = true) {
    return render('btn_edit', 'btn-warning', 'fas fa-edit', disabled, 'Edit');
}

/**
 * Create view button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function view(disabled = true) {
    return render('btn_view', 'btn-info', 'fas fa-eye', disabled, 'View');
}

/**
 * Create delete button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function del(disabled = true) {
    return render('btn_trash', 'btn-danger', 'fas fa-trash', disabled, 'Delete');
}

/**
 * Create rack button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function rack(disabled = true) {
    return render('btn_rack', 'btn-primary', 'fas fa-boxes-stacked', disabled, 'Racks');
}

/**
 * Create history button
 *
 * @param {boolean} disabled
 * @returns {string}
 */
export function history(disabled = true) {
    return render('btn_history', 'btn-primary', 'fas fa-history', disabled, 'See History');
}