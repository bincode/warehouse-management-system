import dragula from 'dragula';

export function initDragula(containerSelector, scrollContainerSelector) {
    var drake = dragula({
        isContainer: function (el) {
            return el.classList.contains(containerSelector);
        },
        invalid: function (el) {
            return el.classList.contains('btn-finished') || el.classList.contains('btn-running');
        }
    });

    var $scrollContainer = $(scrollContainerSelector);
    var autoScrollInterval;

    drake.on('drag', function (el) {
        autoScrollInterval = setInterval(function () {
            var rect = $scrollContainer[0].getBoundingClientRect();
            var elRect = el.getBoundingClientRect();

            var scrollThreshold = 50;
            var scrollSpeed = 5;

            if (elRect.right > rect.right - scrollThreshold) {
                $scrollContainer.scrollLeft($scrollContainer.scrollLeft() + scrollSpeed);
            } else if (elRect.left < rect.left + scrollThreshold) {
                $scrollContainer.scrollLeft($scrollContainer.scrollLeft() - scrollSpeed);
            }
        }, 20);
    });

    drake.on('dragend cancel', function () {
        clearInterval(autoScrollInterval);
    });

    return drake;
}
