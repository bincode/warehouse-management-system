import { info } from 'laravel-mix/src/Log';
import * as dialog from '../alerts/alert-utils';

// Helper to get CSRF token
function getCsrfToken() {
    return $('meta[name="csrf-token"]').attr('content');
}

// Helper for error handling
function handleError(xhr) {
    const errorMsg = xhr.status === 400 ? (xhr.responseJSON.message || 'Error occurred') : 'Server error';
    dialog.showServerErrorAlert(errorMsg);
}

// Load the production schedule
export async function load() {
    const loading = dialog.showLoadingAlert("Load Schedule");

    try {
        const response = await $.ajax({
            url: "/api/schedule/production",
            method: "GET"
        });
        setupScheduleDOM(response);
    } catch (error) {
        dialog.showServerErrorAlert();
    } finally {
        loading.close();
    }
}

// Save the production plan
export async function save() {
    const taskData = collectTaskData();
    const transformedData = transformTaskData(taskData);

    try {
        await $.ajax({
            url: '/api/workorders/save',
            method: 'POST',
            data: {
                workorder: transformedData,
                _token: getCsrfToken()
            }
        });
        dialog.showSuccessAlert();
        await load(); // Reload the schedule after saving
    } catch (xhr) {
        console.error('Error saving data:', xhr);
    }
}

// Collect task data from the DOM
function collectTaskData() {
    const taskData = [];

    $('.task[data-id]').each(function () {
        const machineId = $(this).closest('td').data('id');
        const taskId = $(this).data('id');
        const order = $(this).data('order');

        taskData.push({
            machineId: machineId,
            taskId: taskId,
            order: order
        });
    });

    return taskData;
}

// Transform task data by grouping by machineId
function transformTaskData(taskData) {
    const transformedData = {};

    taskData.forEach(({
        machineId,
        taskId,
        order
    }) => {
        if (!transformedData[machineId]) {
            transformedData[machineId] = [];
        }
        transformedData[machineId].push({
            taskId,
            order
        });
    });

    return transformedData;
}

// Setup the schedule in the DOM and initialize task events
function setupScheduleDOM(response) {
    $('#scrollContainer').html(response);

    $('.task').each(function (index) {
        const status = $(this).data('order');
        if (status) {
            $(this).css('animation-delay', index * 0.1 + 's'); // Menambahkan delay 0.2 detik untuk setiap task}
        };
    });

    // Attach click events to tasks
    $('.task').on('click', function () {
        const taskId = $(this).data('id');
        handleTaskEvent(taskId);
    });
}

async function handleTaskEvent(taskId) {
    try {
        // Fetch task details using AJAX
        const response = await $.ajax({
            url: `/api/workorders?id=${taskId}`,
            method: 'GET',
        });

        const task = response.data[0];

        // Helper function to create buttons
        function createButton(text, iconClass, btnClass) {
            return `<button id="btn-${text.toLowerCase()}" class="btn ${btnClass} btn-option m-1 text-bold" style="width: 120px; height: 120px;">
                        ${text} <i class="${iconClass}"></i>
                    </button>`;
        }

        // Build the HTML content for the dialog
        let htmlContent = createButton('FORMULA', 'fas fa-search', 'btn-primary');

        // Add "RUNNING" button if the task is not currently running
        if (task.status !== 'running') {
            htmlContent += createButton('RUNNING', 'fas fa-play', 'btn-info');
        }

        // Always add "FINISHED" button
        htmlContent += createButton('FINISHED', 'fas fa-check', 'btn-finished');

        // If the task is running, add "HOLD" and "CANCEL" buttons
        if (task.status === 'running') {
            htmlContent += createButton('HOLD', 'fas fa-pause', 'btn-hold text-dark');
            htmlContent += createButton('CANCELED', 'fas fa-times', 'btn-canceled');
            htmlContent += createButton('DEFAULT', 'fas fa-rotate-left', 'btn-planning');
        }

        // Display the dialog with the generated HTML content
        dialog.showHTML(task.formula.product.name, htmlContent);

        // Event listener for button clicks
        $('.btn-option').on('click', function () {
            const action = $(this).attr('id').replace('btn-', ''); // Get action from button ID

            // Handle action for "FORMULA" button
            if (action === 'formula') {
                window.location.href = `/workorders/${task.id}`; // Redirect to work order page
                return;
            }

            // Handle "DEFAULT" button action
            if (action === 'default') {
                ajaxUpdateTaskStatus(task.id, 'continue'); // Update task status to continue
                return;
            }

            // Update task status for other buttons
            ajaxUpdateTaskStatus(task.id, action); // Update task status based on the clicked button
        });

    } catch (xhr) {
        handleError(xhr);
    }
}

// Handle task click events (preview and status update)
async function handleTaskClick(taskId) {
    const token = getCsrfToken();

    const result = await dialog.showPreview(taskId);
    if (result.isConfirmed) {
        window.location.href = `/workorders/${taskId}`;
    } else if (result.dismiss === Swal.DismissReason.cancel) {
        await ajaxUpdateTaskStatus(token, taskId);
    }
}

// Update task status via AJAX
async function ajaxUpdateTaskStatus(taskId, newStatus) {
    const token = getCsrfToken();

    try {
        await $.ajax({
            url: `/api/workorders/updatestatus/${taskId}`,
            method: 'PUT',
            data: {
                status: newStatus, // Make sure `newStatus` is defined where appropriate
                _token: token
            }
        });
        await load(); // Reload the schedule after the update
    } catch (xhr) {
        handleError(xhr);
    }
}
