import Swal from 'sweetalert2';

export const ConfirmationActivatedFormula = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to display this formula for the production section?",
        icon: 'warning',
        showCancelButton: true,
        showDenyButton: true,
        confirmButtonText: 'Yes, display it!',
        denyButtonText: 'No, only for development',
        cancelButtonText: 'Cancel'
    });

export const ConfirmationDeleteItem = () =>
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, delete it!'
    });

export const LoadingAlert = (message = "loading") =>
    Swal.fire({
        title: "Loading",
        html: "Please wait while " + message + " ....",
        allowOutsideClick: false,
        showCancelButton: false,
        showConfirmButton: false,
        didOpen: () => {
            Swal.showLoading(); // Show loading spinner
        }
    });

export const InputDateDialog = (title = 'Input Date', btnText = 'OK', preConfirmCallback) =>
    Swal.fire({
        title: title,
        input: 'date',
        confirmButtonText: btnText,
        preConfirm: () => {
            const dateValue = Swal.getInput().value; // Get the input value

            // Default validation (if no callback is provided)
            if (!dateValue) {
                Swal.showValidationMessage(`Please select a date.`);
            }

            // Call the provided preConfirmCallback if it exists
            if (typeof preConfirmCallback === 'function') {
                return preConfirmCallback(Swal, dateValue);
            }

            return { date: dateValue }; // Return the selected date
        }
    });

export const InputDateFileDialog = (title = 'Input File', btnText = 'OK', preConfirmCallback) =>
    Swal.fire({
        title: title,
        html: `
            <style>
                .swal2-input {
                    width: calc(100% - 20px); /* Adjust width to make them the same */
                    margin: 10px 0; /* Add margin for better spacing */
                }
            </style>
            <span>Enter the date and upload the file <br> the server will automatically calculate the difference between the actual stock and the system.</span>
            <input type="date" id="inputDate" class="swal2-input form-control p-1 m-2" required>
            <input type="file" id="inputFile" class="swal2-input form-control p-1 m-2" accept=".xlsx, .xls" required>
        `,
        confirmButtonText: btnText,
        preConfirm: () => {
            const dateValue = document.getElementById('inputDate').value; // Get the date input value
            const fileInput = document.getElementById('inputFile'); // Get the file input
            const file = fileInput.files[0]; // Get the first file

            // Validate the inputs
            if (!dateValue) {
                Swal.showValidationMessage(`Please select a date.`);
                return false; // Stop further execution
            }
            if (!file) {
                Swal.showValidationMessage(`Please select a file.`);
                return false; // Stop further execution
            }

            // Return the date and file as an object
            return { date: dateValue, file };
        }
    });

export const InputStatusDialog = (currentStatus) =>
    Swal.fire({
        title: 'Change Status',
        input: 'select',
        inputOptions: {
            'continue': 'Continue',
            'hold': 'Hold',
            'canceled': 'Canceled'
        },
        inputValue: currentStatus,
        showCancelButton: true,
        inputValidator: (value) => {
            if (!value) {
                return 'You need to choose a status!';
            }
        }
    });

export const ServerErrorAlert = (message = "Please contact your administrator.") =>
    Swal.fire({
        title: "Server Error",
        text: message,
        icon: 'question',
    });

export const StockValidationAlert = (htmlContent) =>
    Swal.fire({
        title: "Stock Validation",
        html: htmlContent,
        icon: 'info'
    });

export const SuccessToast = (message) =>
    Swal.fire({
        icon: 'success',
        title: message,
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timerProgressBar: true,
        timer: 3000,
    });

export const SuccessDeleteAlert = () =>
    Swal.fire({
        title: 'Deleted!',
        text: 'Your file has been deleted.',
        icon: 'success',
    });

export const ServerWarningAlert = (title = 'warning', message = "Please contact your administrator.") =>
    Swal.fire({
        title: title,
        html: message,
        icon: 'warning',
    });