// Importing the Select2 library
import 'admin-lte/plugins/select2/js/select2.full.min.js';

// Global configuration for Select2
const SELECT2_GLOBAL_CONFIG = {
    placeholder: '... Select ...',
    ajax: {
        delay: 250,
    },
};

// Function to format data by default
function defaultFormat(data) {
    return {
        results: $.map(data.data, function (obj) {
            return {
                id: obj.id,
                text: obj.name,
            };
        }),
    };
}

// Function to configure Select2
export function config(selector, config) {
    return $(selector).select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Function to reload Select2
export function reload($element, config) {
    return $element.select2($.extend({}, SELECT2_GLOBAL_CONFIG, config));
}

// Configuration and initialization
function ajaxPermissions() {
    return {
        placeholder: 'Select Permissions',
        ajax: {
            url: '/api/permissions',
            data: function (params) {
                return {
                    text: params.term,
                };
            },
            processResults: defaultFormat,
        },
    }
};

// Configuration for selecting permissions
export const permissionSelectConfig = ajaxPermissions();
config("select.permissions", permissionSelectConfig);

// Configuration and initialization
export function ajaxRoles() {
    return {
        placeholder: '.. Select Roles ..',
        ajax: {
            url: '/api/roles',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: defaultFormat,
        }
    }
};

// Configuration for selecting roles
export const roleSelectConfig = ajaxRoles();
config("select.roles", roleSelectConfig);

// Configuration and initialization
export function ajaxProductCategory() {
    return {
        placeholder: '.. Select Category ..',
        ajax: {
            url: '/api/products/category',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: defaultFormat,
        },
    }
};

// Configuration for selecting product category
export const productCategorySelectConfig = ajaxProductCategory();
config("select.product-category", productCategorySelectConfig);

// Configuration and initialization
export function ajaxRacks(isAvailableOnly = false) {
    return {
        placeholder: '.. Select Rack ..',
        ajax: {
            url: '/api/racks',
            data: function (params) {

                // Initialize dataParams with the search term from the input field
                var dataParams = {
                    text: params.term,
                };

                // Get the value of the selected product in the current row
                let product = $(this).closest('tr').find('select[name="products[]"]').val();

                // If a product is selected and 'isAvailableOnly' is true, add the product to dataParams
                if (product && isAvailableOnly) {
                    dataParams.product = product;
                }

                // Return the final parameters for the request
                return dataParams;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.code,
                        };
                    }),
                };
            },
        },
    }
};

// Configuration for selecting racks
export const rackSelectConfig = ajaxRacks();
config("select.rack", rackSelectConfig);

// Configuration for selecting available racks by product
export const availableRackSelectConfig = ajaxRacks(true);
config("select.rack-available", availableRackSelectConfig);

export function ajaxProductby(category, selector) {
    return {
        placeholder: `.. Select ${category.charAt(0).toUpperCase() + category.slice(1)} ..`,
        ajax: {
            url: '/api/products',
            data: function (params) {
                const selectorValue = $(selector).length ? $(selector).val().match(/\d+/)[0] : null;
                return {
                    text: selectorValue || params.term,
                    category: category,
                    history: false,
                    select2: true,
                };
            },
            processResults: defaultFormat,
        },
    };
};

// Configuration for selecting materials
export const materialsSelectConfig = ajaxProductby('materials');
config("select.product-materials", materialsSelectConfig);

export const goodsSelectConfig = ajaxProductby('goods');
config("select.product-goods", goodsSelectConfig);

export const productBySelectorConfig = ajaxProductby('', '.product-selector-search');
config("select.product-search", productBySelectorConfig);

// Configuration for selecting  relation
export function ajaxRelationType() {
    return {
        placeholder: '.. Select Relation ..',
        ajax: {
            url: '/api/relations/getRelationType',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: defaultFormat,
        },
    }
};

export const relationType = ajaxRelationType();
config("select.relation-type", relationType);

export function ajaxRelation(type) {
    return {
        placeholder: '.. Select  ..',
        ajax: {
            url: '/api/relations',
            data: function (params) {
                return {
                    text: params.term,
                    type: type,
                };
            },
            processResults: defaultFormat
        },
    };
};

// Configuration for selecting products (all categories)
export const relationSelectConfig = ajaxRelation('cs');
config("select.relation", relationSelectConfig);

// Configuration for selecting goods
export const supplierSelectConfig = ajaxRelation('s');
config("select.supplier", supplierSelectConfig);

// Configuration for selecting materials
export const customerSelectConfig = ajaxRelation('c');
config("select.customer", customerSelectConfig);

// Configuration and initialization for "category of product" select element
export function ajaxFormula(isActive = null) {
    return {
        placeholder: '.. Select Formula ..',
        ajax: {
            url: '/api/formulas',
            data: function (params) {
                return {
                    text: params.term,
                    isActive: isActive
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: `${obj.product.name} (${obj.version})`,
                        };
                    }),
                };
            },
        },
    };
};

export const formulaActiveSelectConfig = ajaxFormula(1);
config("select.formula-active", formulaActiveSelectConfig);

// Function to create Select2 configuration for filtering machine by type
export function ajaxMachine(type = null) {
    return {
        placeholder: '.. Select ' + type + ' ..',
        ajax: {
            url: '/api/machines',
            data: function (params) {
                return {
                    text: params.term,
                    type: type ?? null,
                };
            },
            processResults: defaultFormat,
        },
    };
};

export const extruderMachineSelectConfig = ajaxMachine('extruder');
config("select.machine-extruder", extruderMachineSelectConfig);

export const mixerMachineSelectConfig = ajaxMachine('mixer');
config("select.machine-mixer", mixerMachineSelectConfig);

// Configuration and initialization for "category of product" select element
export function ajaxMachineProcessType() {
    return {
        placeholder: '.. Select Process Type ..',
        ajax: {
            url: '/api/machines-process-type',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.name,
                            description: obj.description
                        };
                    }),
                };
            },
        },
        templateResult: function (item) {
            if (!item.id) {
                return item.text;
            }

            // Formatting the result item
            return $('<span><strong>' + item.text + '</strong><br><small>' + item.description + '</small></span>');
        },
    };
}

export const ProcessTypeMachineSelectConfig = ajaxMachineProcessType();
config("select.machine-process-type", ProcessTypeMachineSelectConfig);

// Configuration for selecting  relation
export function ajaxDatePriorityType() {
    return {
        placeholder: '.. Select Priority Date ..',
        ajax: {
            url: '/api/sales-order/getDatePriorityType',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: defaultFormat,
        },
    }
};

export const datePriorityType = ajaxDatePriorityType();
config("select.date-priority-type", datePriorityType);

// Configuration for selecting  relation
export function ajaxInventoryType() {
    return {
        placeholder: '.. Select Inventory Type ..',
        ajax: {
            url: '/api/product/getInventoryType',
            data: function (params) {
                return {
                    text: params.term
                };
            },
            processResults: defaultFormat,
        },
    }
};

export const inventoryType = ajaxInventoryType();
config("select.inventory-type", inventoryType);