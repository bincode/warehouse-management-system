// Import utility functions for handling moment.js and date range picker functionalities
import '../utils/moment-utils';
import 'daterangepicker';

// Document ready function to initialize datepicker elements
$(function () {
    // Initialize datepickers for all elements with class 'datepicker'
    init('.datepicker');

    // Iterate through all elements with the 'datepicker' class
    $('.datepicker').each(function () {
        const id = $(this).attr('id');  // Get the ID of the element
        const storedValue = localStorage.getItem(id);  // Retrieve stored value from localStorage using the ID as the key

        if (storedValue) {
            // If a value is found in localStorage, set it as the value of the input
            $(this).val(storedValue);
        } else {
            // If no stored value, set the default date based on the input's ID
            if (id === 'tableSrcDateStart') {
                startOfMonth(this);  // Set to the start of the current month
            } else if (id === 'tableSrcDateEnd') {
                endOfMonth(this);    // Set to the end of the current month
            }
        }
    });

    // Event listener for when a date is selected in the date picker
    $('.datepicker').on('apply.daterangepicker', function (ev, picker) {
        const id = $(this).attr('id');  // Get the ID of the element
        localStorage.setItem(id, $(this).val());  // Store the selected date in localStorage
    });
});

/**
 * Initializes the datepicker with specific settings.
 * 
 * @param {string} selector - CSS selector for elements to initialize as datepicker.
 * @returns {Object} - The initialized datepicker instance.
 */
export function init(selector) {
    console.log('Init Date Picker');
    const $datepicker = $(selector).daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'  // Set date format
        },
        autoApply: true,         // Automatically apply the selected date
        singleDatePicker: true,  // Only allow a single date selection
        showDropdowns: true,     // Show year and month dropdowns
        minYear: 2011,           // Minimum selectable year
        maxYear: moment().format('YYYY')  // Maximum selectable year (current year)
    });

    return $datepicker;  // Return the initialized datepicker
}

/**
 * Sets the value of the input to the start of the current month if no value is set.
 * 
 * @param {HTMLElement} selector - The input element to set the date for.
 */
export function startOfMonth(selector) {
    if (!$(selector).val()) {
        $(selector).val(moment().startOf('month').format('DD/MM/YYYY'));
    }
}

/**
 * Sets the value of the input to the end of the current month if no value is set.
 * 
 * @param {HTMLElement} selector - The input element to set the date for.
 */
export function endOfMonth(selector) {
    if (!$(selector).val()) {
        $(selector).val(moment().endOf('month').format('DD/MM/YYYY'));
    }
}
