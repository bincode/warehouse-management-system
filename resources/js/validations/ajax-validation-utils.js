// validation-utils.js
import * as dialog from '../alerts/alert-utils';

// Helper function to handle AJAX requests
function sendAjaxRequest(apiUrl, method, data, successCallback) {
    $.ajax({
        url: apiUrl,
        method: method,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: successCallback,
        error: function (xhr, status, error) {
            dialog.showServerErrorAlert(error);
        }
    });
}

export function stock(method, isAutoCreateFormula = false) {
    // Show loading alert while validating stock
    dialog.showLoadingAlert("Validating Stock");

    // Extract the domain from the current URL
    const domain = window.location.origin;

    // Construct the API endpoint URL
    const apiUrl = `${domain}/api/products/validationstock`;

    // Array to store products
    var products = [];
    var workorders = [];

    var workOrderId = $('input[name="id"]').val();
    if (workOrderId) {
        workorders.push(workOrderId);
    }

    $('#table-input-work-order').find('tr').each(function () {
        const productId = $(this).find('select[name="products[]"]').val();
        const quantity = $(this).find('input[name="amount[]"]').val();
        products.push({
            id: productId,
            quantity: quantity
        });
    });

    sendAjaxRequest(apiUrl, method, {
        products,
        workorder: workorders
    }, function (response) {
        if (response.some(result => result.status === false) || !isAutoCreateFormula) {
            dialog.showStockValidationAlert(response);
        } else {
            formula(method);
        }
    });

}

export function formula(method) {
    // Show loading alert while validating formula
    dialog.showLoadingAlert("validating formula");

    // Extract the formula ID from the current URL
    const domain = window.location.origin;
    const id = $('select[name="formula_id"]').val();
    const apiUrl = `${domain}/api/formulas/${id}/validate`;

    // Send AJAX request to validate formula
    $.ajax({
        url: apiUrl,
        method: method,
        data: $('#workform').serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (response) {
            // If formula validation succeeds, submit the form
            if (response.success) {
                dialog.showLoadingAlert('saving work order');
                $('#workform').off('submit').trigger('submit');
            } else {
                // Show confirmation alert to create a new formula
                dialog.showCreateNewFormulaConfirmation().then((result) => {
                    if (result.isConfirmed) {
                        dialog.showLoadingAlert('saving work order');
                        $('#workform').off('submit').trigger('submit');
                    }
                });
            }
        },
        error: function (xhr, status, error) {
            // Show server error alert if request fails
            dialog.showServerErrorAlert();
        }
    });
}
