import dragula from 'dragula';
import * as dialog from '../alerts/alert-utils';
import '../moment/moment-initialization';

$(function () {
    // Inisialisasi Dragula
    var drake = dragula({
        isContainer: function (el) {
            return el.classList.contains('date-column');
        }
    });

    // Auto-scroll functionality
    var $scrollContainer = $('#scrollContainer');
    var isDragging = false;
    var autoScrollInterval;

    drake.on('drag', function (el) {
        isDragging = true;
        console.log('Dragging started');

        autoScrollInterval = setInterval(function () {
            var rect = $scrollContainer[0].getBoundingClientRect();
            var elRect = el.getBoundingClientRect();
            var scrollThreshold = 50;
            var scrollSpeed = 5;

            if (elRect.right > rect.right - scrollThreshold) {
                $scrollContainer.scrollLeft($scrollContainer.scrollLeft() + scrollSpeed);
                console.log('Scrolling right');
            } else if (elRect.left < rect.left + scrollThreshold) {
                $scrollContainer.scrollLeft($scrollContainer.scrollLeft() - scrollSpeed);
                console.log('Scrolling left');
            }
        }, 20);
    });

    drake.on('dragend', function () {
        isDragging = false;
        clearInterval(autoScrollInterval);
    });

    drake.on('cancel', function () {
        isDragging = false;
        clearInterval(autoScrollInterval);
    });

    $('.task').on('click', function () {

        const data = $(this).data('task');

        // Mendapatkan tanggal hari ini dengan Moment.js
        let formattedDate = moment().format('YYYY-MM-DD');

        // Menambahkan data now ke dalam objek data
        data.now = formattedDate;
        data.amount = data.quantity;

        dialog.showInput(data).then(function (result) {
            if (result.isConfirmed) {
                // Get the CSRF token from the meta tag
                const token = $('meta[name="csrf-token"]').attr('content');
                const url = '/deliveryrequest/' + data.id;

                // Lakukan panggilan AJAX di sini dengan data yang diperoleh
                $.ajax({
                    url: url,
                    type: 'PUT',
                    headers: {
                        'X-CSRF-TOKEN': token // Menggunakan token CSRF dalam header
                    },
                    data: {
                        product_sales_order_id: data.reference.sales_order_id,
                        date: result.value.date,
                        lot: result.value.lot,
                        quantity: result.value.quantity,
                        bag: result.value.bag,
                        document: result.value.document,
                        note: result.value.note
                    },
                    success: function (response) {
                        // Handle success response
                        dialog.showSuccessAlert();
                        location.reload();
                    },
                    error: function (xhr, status, error) {
                        // Handle error response
                        dialog.showServerErrorAlert();
                    }
                });
            }
        });
    });

    $('.btn-delivered').on('click', function (e) {
        e.preventDefault();

        const data = $(this).data('task');
        data.date = moment(data.date).format('DD/MM/YYYY');

        dialog.showInputReferenceNumber().then(function (result) {
            if (result.isConfirmed) {

                data.for = result.value;

                dialog.showDeliveredConfirmation().then(function (input) {
                    const url = '/api/deliveryschedule';
                    // Set CSRF token header for AJAX requests
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    // Send DELETE request to server
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        success: function (response, textStatus, xhr) {
                            dialog.showSuccessAlert();
                            location.reload();
                        },
                        error: function (xhr, status, error) {
                            dialog.showServerErrorAlert(error);
                        }
                    });
                });
            }
        });

    });

    $('btn-delete').on('click', function (e) {
        e.preventDefault();

        const id = $(this).data('id');
        const task = $(this).closest('a');

        dialog.showDeleteConfirmation().then(function (result) {
            const url = '/api/deliveryschedule/' + id;

            // Set CSRF token header for AJAX requests
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Send DELETE request to server
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (response, textStatus, xhr) {
                    dialog.showSuccessAlert();
                    task.remove();
                },
                error: function (xhr, status, error) {
                    dialog.showServerErrorAlert(error);
                }
            });
        });
    });
});
