import { initDragula } from '../utils/drag-drop';
import { initRange } from '../daterangepicker/daterangepicker-utils';
import * as schedule from '../utils/schedule-production';

$(function () {
    // Initialize Dragula for drag-and-drop functionality
    initDragula('machine-column', '#scrollContainer');

    // Load the production schedule
    schedule.load();

    // Initialize date range picker
    initRange('#dateRangePicker');

    // Save the production plan on button click
    $('#saveProductionPlan').on('click', function () {
        schedule.save();
    });

    // Reload schedule on manual refresh button click
    $('#refreshSchedule').on('click', function () {
        schedule.load();
    });

    // Function to check if the document is in fullscreen mode
    function isFullscreen() {
        return document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;
    }

    // Automatically reload the schedule every 60 seconds if in fullscreen
    setInterval(function () {
        if (isFullscreen()) {
            schedule.load();
        }
    }, 60000); // 1 minute interval
});
