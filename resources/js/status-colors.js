// status-colors.js

/**
 * Function to get status color based on status
 * @param {string} status - The status of the work order
 * @returns {string} - The appropriate button color class
 */
export function getStatusColor(status) {
    const statusClasses = {
        'all': 'btn-all',
        'created': 'btn-created',
        'planning': 'btn-planning',
        'prepare': 'btn-prepare',
        'process': 'btn-process',
        'finished': 'btn-finished',
        'packing': 'btn-packing',
        'delivered': 'btn-delivered',
        'completed': 'btn-completed',
        'hold': 'btn-hold',
        'canceled': 'btn-canceled'
    };

    return statusClasses[status.toLowerCase()] || 'btn-all'; // Default to gray if status not found
}
