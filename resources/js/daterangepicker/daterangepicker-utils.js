import '../moment/moment-initialization';
import 'daterangepicker';

$(function () {
    init('.datepicker');
});

export function init(selector) {
    const $datepicker = $(selector).daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'
        },
        autoApply: true,
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 2011,
        maxYear: moment().format('YYYY')
    });

    return $datepicker;
}

export function initRange(selector) {
    const $datepicker = $(selector).daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'
        },
        autoApply: true,
        showDropdowns: true,
    });

    return $datepicker;
}

export function startOfMonth(selector){
    if (!$(selector).val()) {
        $(selector).val(moment().startOf('month').format('DD/MM/YYYY'));
    }
}

export function endOfMonth(selector) {
    if (!$(selector).val()) {
        $(selector).val(moment().endOf('month').format('DD/MM/YYYY'));
    }
}
