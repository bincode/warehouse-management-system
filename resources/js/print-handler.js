import printJS from 'print-js';

/**
 * Menangani pencetakan dengan Print.js
 * @param {string} printableId - ID elemen yang ingin dicetak
 * @param {string} cssPath - Jalur ke file CSS untuk pencetakan
 */
export function printContent(printableId, cssPath) {
    printJS({
        printable: printableId,
        type: 'html',
        css: cssPath,
        scanStyles: false
    });
}

// Event listener untuk menangani pencetakan menggunakan shortcut keyboard
document.addEventListener("keydown", function (event) {
    // Jika pengguna menekan Ctrl+P
    if (event.ctrlKey && event.key === "p") {
        // Mencegah tindakan bawaan Ctrl+P (yaitu, pencetakan default browser)
        event.preventDefault();

        // Memulai pencetakan menggunakan Print.js
        printContent("kanban-container", '/css/print.css');
    }
});
