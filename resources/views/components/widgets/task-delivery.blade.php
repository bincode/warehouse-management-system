<a id="{{ $task->id }}" href="#" class="btn btn-block {{ $makeOutlineColor() }} text-left" style="font-size: 100%;">
    <!-- Display the product name in a bold, ellipsis-truncated span -->
    <div class="row no-wrap align-items-center">
        <div class="col task" data-task="{{ $task }}">
            <div class="row">
                <div class="col-auto">
                    <strong>{{ $task->reference->product->name }}</strong>
                </div>
                <div class="col text-right">
                    <strong>({{ $task->quantity }} KG / {{ $task->bag }} ZAK)</strong>
                </div>
                <div class="col-auto text-right">
                    <strong> LOT NO. {{ $task->lot }} </strong>
                </div>
            </div>

            <!-- Horizontal rule with light border and margin adjustments -->
            <hr class="border-dark m-1">

            <div class="row">
                <div class="col">
                    <strong>{{ $task->reference->order->company->name }}</strong>
                </div>
                <div class="col text-right">
                    <span class="{{ $makeBadgeClass() }}"><strong>{{ strtoupper($getNote()) }}</strong></span>
                </div>
            </div>
        </div>
        <div class="col-auto hide-on-hover">
            @if ($task->reference->status === "finished")
                <button class="btn btn-block btn-danger btn-delete" data-id="{{ $task->id }}"><i class="fa fas fa-trash"></i></button>
                <button class="btn btn-block btn-primary btn-delivered" data-task="{{ $task }}"><i class="fa fas fa-truck"></i></button>
            @else
                <button class="btn btn-block btn-dark"><i class="fa fas fa-lock"></i></button>
            @endif
        </div>
    </div>
</a>
