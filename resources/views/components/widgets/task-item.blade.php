<a href="javascript:void(0);" class="{{ $makeButtonClass }}" style="font-size: 100%;display: block;min-height: 120px;" data-id="{{ $order->id }}" data-status="{{ $order->status }}" data-order="{{$order->order}}" data-toggle="tooltip"
   title="{{ $order->formula->product->name }}">

    <!-- Display the product name in a bold, ellipsis-truncated span -->
    <span>
        <strong class="text-ellipsis">{{ $order->formula->product->name }}</strong>
    </span>

    <!-- Horizontal rule with light border and margin adjustments -->
    <hr class="border-light mt-1 mb-1">

    <!-- Display the product color or a dash if the color is not set -->
    <span class="d-block text-right machine-info">
        <small><strong class="text-ellipsis">{{ $order->formula->product->color ?? '-' }}</strong></small>
    </span>

    <!-- Display the planned quantity in kilograms -->
    <span class="d-block text-right machine-info">
        <small>Qty. <strong>{{ $order->plan_quantity }}</strong> Kg</small>
    </span>

    <!-- Display the planned quantity in kilograms -->
    <span class="d-block text-right machine-info">
        <small><strong>{{ $order->mixer->name ?? '-' }}</strong></small>
    </span>

    <span class="d-block text-right machine-info">
        <small><strong>{{ $order->lot ?? '-' }}</strong></small>
    </span>
</a>
