<tr>
    <td class="machine-name" rowspan="7"><strong>{{ $machine->name }}</strong></td>
    <td><strong>TGL MASUK PO</strong></td>
    @foreach($workorders as $workorder)
        <th>{{ isset($workorder) && $workorder->salesorders->isNotEmpty() ? $workorder->salesorders->first()->date : '' }}
        </th>
    @endforeach
</tr>
<tr>
    <td>Code</td>
    @foreach($workorders as $workorder)
        <th>{{ isset($workorder) && $workorder->formula ? $workorder->formula->product->name : '' }}
        </th>
    @endforeach
</tr>
<tr>
    <td>Colour</td>
    @foreach($workorders as $workorder)
        <th>{{ isset($workorder) && $workorder->formula ? preg_replace('/\s*\(.*?\)/', '', $workorder->formula->product->color) : '' }}
        </th>
    @endforeach
</tr>
<tr>
    <td>MFG. NO</td>
    @foreach($workorders as $workorder)
        <th>{{ isset($workorder) ? $workorder->lot : '' }}</th>
    @endforeach
</tr>
<tr>
    <td>QTY / MIXER</td>
    @foreach($workorders as $workorder)
        <th>
            @isset($workorder)
                {{ isset($workorder) ? $workorder->plan_quantity : '' }} /
                {{ isset($workorder) && $workorder ? $workorder->mixer->name : '' }}
            @endisset
        </th>
    @endforeach
</tr>
<tr>
    <td>Stock</td>
    @foreach($workorders as $workorder)
        <th></th>
    @endforeach
</tr>
<tr>
    <td>Delivery</td>
    @foreach($workorders as $workorder)
        <th>
            @isset($workorder)
                {{ isset($workorder) && $workorder->salesorders->isNotEmpty()
                    ? ($workorder->salesorders->first()->pivot->ship_priority !== 'SPECIFIED_DATE'
                        ? $workorder->salesorders->first()->pivot->ship_priority
                        : \Carbon\Carbon::parse($workorder->salesorders->first()->pivot->ship_date)->format('d/m/Y'))
                    : '-' }}
            @endisset
        </th>
    @endforeach

</tr>
<tr>
    <td></td>
    @foreach($workorders as $workorder)
        <th></th>
    @endforeach
</tr>
