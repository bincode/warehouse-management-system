<table id={{ $name }} class="table table-hover datatable mt-3" style="width:100%">
    <thead class="thead">
        <tr>
            <th class='text-center p-2'>Materials</th>
            <th class='text-center p-2' width='18%'>Percentage (%)</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($items)
            @foreach ($items as $materials)
                <!-- Render input materials list component with data row -->
                <x-row-input-formula :dataRow='$materials' />
            @endforeach
        @endisset

        <!-- Render input materials list component without data row -->
        <x-row-input-formula />
    </tbody>
</table>
