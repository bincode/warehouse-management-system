@extends('components.forms.base-input-group-component')

@section('input-item')
    <textarea name="{{ $name }}" {{ $attributes->class(['form-control', 'is-invalid' => $errors->has($name)]) }}>{{ $getValue() }}</textarea>
@overwrite
