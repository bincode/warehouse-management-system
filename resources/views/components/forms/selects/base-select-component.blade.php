@extends('components.forms.base-input-group-component')

@section('input-item')

    {{-- Select Field --}}
    <select {{ $attributes->class(['form-control', 'select2' => true ,'is-invalid' => $errors->has($name)])->merge(['name' => $name, 'style' => 'width: 100%']) }} >

        {{-- Placeholder --}}
        {{-- If $placeholder variable is set, then display a placeholder option with empty value --}}
        @if (isset($placeholder))
            <option value=''>{{ $placeholder }}</option>
        @endif

        {{-- Options --}}
        {{-- Loop through each option in the $makeOptionList variable --}}
        @foreach ($makeOptionList as $key => $value)
            {{-- Display an option with value $key and text $value --}}
            <option {{ $isSelected($key) ? 'selected="selected"' : '' }} value={{ $key }}>{{ $value }}</option>
        @endforeach
        {{-- ./Options --}}

    </select>
    {{-- ./Select Field --}}
@overwrite
