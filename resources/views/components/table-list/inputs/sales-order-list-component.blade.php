<table id={{ $name }} class="table table-hover datatable" style="width:100%">
    <thead class="thead bg-primary ">
        <tr>
            <th class='text-center p-2'>Products</th>
            <th class='text-center p-2' width='12%'>Quantity</th>
            <th class='text-center p-2' width='15%'>Ship Priority</th>
            <th class='text-center p-2' width='15%'>Ship Date</th>
            <th class='text-center p-2' width='22%'>Note</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($items)
            @foreach ($items as $item)
                <!-- Render input item list component with data row -->
                <x-input-sales-order-row :data='$item' />
            @endforeach
        @endisset

        <!-- Render input item list component without data row -->
        <x-input-sales-order-row />
    </tbody>
</table>
