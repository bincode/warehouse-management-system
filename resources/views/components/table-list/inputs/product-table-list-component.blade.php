<table id={{ $name }} class="table table-hover datatable mt-3" style="width:100%">
    <thead class="thead">
        <tr>
            <th class='text-center p-2'>Items</th>
            <th class='text-center p-2' width='15%'>Rack</th>
            <th class='text-center p-2' width='18%'>Quantity</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($products)
            @foreach ($products as $product)
                <!-- Render input product list component with data row -->
                <x-input-product-row :data='$product' />
            @endforeach
        @endisset

        <!-- Render input product list component without data row -->
        <x-input-product-row />
    </tbody>
</table>
