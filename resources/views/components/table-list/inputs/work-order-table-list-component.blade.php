<table id={{ $name }} class="table table-hover datatable mt-3" style="width:100%">
    <thead class="thead">
        <tr>
            <th class='text-center p-2'>Items</th>
            <th class='text-center p-2' width='18%'>Percentage</th>
            <th class='text-center p-2' width='18%'>Quantity</th>
            <th width='2%'></th>
        </tr>
    </thead>
    <tbody>
        @isset($items)
            @foreach ($items as $item)
                <!-- Render input item list component with data row -->
                <x-input-work-order-row :data='$item' />
            @endforeach
        @endisset

        <!-- Render input item list component without data row -->
        <x-input-work-order-row />
    </tbody>
</table>
