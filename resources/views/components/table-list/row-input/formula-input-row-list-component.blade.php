<tr class="pb-0">
    <!-- Product Select -->
    <td>
        <x-select-product name='products[]' class='input-focus product-materials' fgroup-class='m-0' :bind='$dataRow' />
    </td>
    <!-- Percentage Input -->
    <td>
        <x-input name='percentage[]' class='input-focus text-center sum' fgroup-class='m-0' type="number" step="0.001" :bind='$dataRow' />
    </td>
    <!-- Delete Button -->
    <td class="d-flex align-items-center">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }}' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
