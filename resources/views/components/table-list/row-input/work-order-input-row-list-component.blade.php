<tr class="pb-0">
    <!-- Product Select -->
    <td class="pb-0">
        <x-select-product name='products[]' class='input-focus product-materials' fgroup-class='m-1' :bind='$dataRow' />
    </td>
    <!-- Quantity Input -->
    <td class="pb-0">
        <x-input name='percentage[]' class='input-focus text-center sum' fgroup-class='m-1' :bind='$dataRow' />
    </td>
    <!-- Quantity Input -->
    <td class="pb-0">
        <x-input name='amount[]' class='input-focus text-center' fgroup-class='m-1' :bind='$dataRow' />
    </td>
    <!-- Delete Button -->
    <td class="pb-2">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }} m-1' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
