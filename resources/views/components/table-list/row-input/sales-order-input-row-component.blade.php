<tr class="pb-0">
    <!-- Product Select -->
    <td class="pb-0">
        <x-select-product name='products[]' class='input-focus product-goods' fgroup-class='m-0' :bind='$dataRow' />
    </td>
    <!-- Quantity Input -->
    <td class="pb-0">
        <x-input name='amount[]' type='number' step="0.001" class='input-focus text-right sum' fgroup-class='m-0' :bind='$dataRow' />
    </td>
    <!-- Ship Priority Input -->
    <td class="pb-0">
        <x-select-date-priority name='ship_priority[]' fgroup-class='m-0' :bind='$dataRow' />
    </td>
    <!-- Ship Date Input -->
    <td class="pb-0">
        <x-input name='ship_date[]' daterangepicker fgroup-class='m-0' :bind='$dataRow' />
    </td>
    <!-- Remark Input -->
    <td class="pb-0">
        <x-input name='remark[]' fgroup-class='m-0' :bind='$dataRow' />
    </td>

    <!-- Delete Button -->
    <td class="pb-2">
        <button id="btn_delete_list" type='button' class='{{ $makeActionDeleteButtonClass }} m-0' {{ $isDeleteButtonDisabled }}>
            <i class="fas fa-trash"></i>
        </button>
    </td>
</tr>
