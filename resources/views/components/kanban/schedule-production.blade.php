<table class="table table-borderless w-100" style="height: 100%;table-layout: fixed;">
    <thead>
        <tr class='text-center bg-primary'>
            <!-- Render machine headers -->
            @foreach ($machines as $machine)
                <th class="border border-white machine-card">
                    {{ $machine->name }}
                </th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <tr class="machine-column">
            <!-- Render work orders for each machine -->
            @foreach ($machines as $machine)
                <td class="machine-column machine-card" data-id="{{ $machine->id }}">
                    @foreach ($machine->workOrders as $order)
                        <x-task :workorder=$order />
                    @endforeach
                </td>
            @endforeach
        </tr>
    </tbody>
</table>
