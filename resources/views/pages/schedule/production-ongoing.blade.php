@extends('_layouts.base')

@section('title', 'Kanban Schedule')

@section('content')
    <div class="row text-right">
        <div class="col-2 offset-10">
            <button id="toggleFullscreen" class="btn btn-block btn-dark zoom-responsive">TOGGLE FULLSCREEN</button>
        </div>
    </div>

    <!-- Fullscreen container yang mencakup bagian heading dan konten -->
    <div class="fullscreen-container p-2" id="fullscreenContainer" style="display: flex;flex-direction: column;">

        <div class="row text-center w-100">
            <div class="col-12 text-primary">
                <hr class="planning-divider">
                <h1 class="planning-title"><strong>PRODUCTION ONGOING</strong></h1>
                <span id="currentTime" class="current-time"></span>
                <hr class="planning-divider">
            </div>
        </div>

        <!-- Tabel Production Ongoing -->
        <div class="row pl-2 pr-3" style="flex-grow: 1; display: flex; flex-direction: column;">
            <table class="table table-borderless w-100" style="height: 100%;table-layout: fixed;">
                <thead>
                    <tr>
                        <th class="text-center" style="font-size: calc(0.6rem + 0.5vw);">Nama Mesin</th>
                        <th class="text-center" style="font-size: calc(0.6rem + 0.5vw);">Product Running </th>
                        <th class="text-center" style="font-size: calc(0.6rem + 0.5vw);">Status</th>
                        <th class="text-center" style="font-size: calc(0.6rem + 0.5vw);">Quantity</th>
                        <th class="text-center" style="font-size: calc(0.6rem + 0.5vw);">Next Planning</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($machines as $machine)
                        <tr class="border-bottom btn-{{ optional($machine->workOrders->first())->status ?? 'secondary'}}">
                            <td style="font-size: calc(0.6rem + 0.5vw);">{{ $machine->name }}</td>
                            <td style="font-size: calc(0.6rem + 0.5vw);">{{ optional(optional($machine->workOrders->first())->formula)->product->name ?? '-' }}</td>
                            <td class="text-center" style="font-size: calc(0.6rem + 0.5vw);">{{ strtoupper(optional($machine->workOrders->first())->status ?? 'OFF') }}</td>
                            <td class="text-center" style="font-size: calc(0.6rem + 0.5vw);">{{ optional($machine->workOrders->first())->plan_quantity ?? '-' }} Kg</td>
                            <td style="font-size: calc(0.6rem + 0.5vw);">{{ optional(optional($machine->workOrders->skip(1)->first())->formula)->product->name ?? '-' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/kanban-utils.js') }}"></script>
    <script>
        document.getElementById('toggleFullscreen').addEventListener('click', function() {
            const container = document.getElementById('fullscreenContainer');

            if (!document.fullscreenElement) {
                container.requestFullscreen().catch(err => {
                    console.error("Failed to enter fullscreen mode:", err);
                });
            } else {
                document.exitFullscreen().catch(err => {
                    console.error("Failed to exit fullscreen mode:", err);
                });
            }
        });

        function updateTime() {
            const now = new Date();

            // Mendapatkan jam, menit, dan detik
            const hours = String(now.getHours()).padStart(2, '0');
            const minutes = String(now.getMinutes()).padStart(2, '0');
            const seconds = String(now.getSeconds()).padStart(2, '0');
            const currentTime = hours + ':' + minutes + ':' + seconds;

            // Mendapatkan tanggal dalam format yang diinginkan
            const options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            };
            const currentDate = now.toLocaleDateString('id-ID', options);

            // Update elemen dengan waktu dan tanggal
            document.getElementById('currentTime').textContent = currentDate + ' ' + currentTime;
        }

        // Panggil updateTime setiap 1 detik
        setInterval(updateTime, 1000);
        // Panggil langsung untuk menampilkan waktu segera
        updateTime();
    </script>
@endsection
