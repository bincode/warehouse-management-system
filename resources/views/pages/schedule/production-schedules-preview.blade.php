@extends('_layouts.base')

@section('title', 'Kanban Schedule')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/print.css') }}">
@endsection

@section('content')

    <div id="kanban-container">
        <div class="header">
            <h2><b>PRODUCTION PLANNING (MASTERBATCH)</b></h2>
            <p>{{ \Carbon\Carbon::now()->format('d F Y') }}</p>
            <div class="form-number">F-PIC-01-01</div>
        </div>

        @php
            $half = $machines->count() / 2;
            $machinesLeft = $machines->take(ceil($half));
            $machinesRight = $machines->slice(ceil($half));
        @endphp

        <div class="kanban-container">
            <!-- First Table -->
            <table id="kanban-table-left" class="kanban-table text-center" style="font-size: 80%">
                <thead>
                    <tr>
                        <th style="width: 10%"></th>
                        <th style="width: 12.5%"><strong>REMARK</strong></th>
                        @for ($i = 1; $i < 7; $i++)
                            <th style="width: 11.25%">{{ $i }}</th>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    @foreach ($machinesLeft as $machine)
                        <x-machine-row :machine='$machine'/>
                    @endforeach
                </tbody>
            </table>

            <!-- Second Table -->
            @if($machinesRight->isNotEmpty())
            <table id="kanban-table-right" class="kanban-table text-center" style="font-size: 80%">
                <thead>
                    <tr>
                        <th style="width: 10%"></th>
                        <th style="width: 12.5%"><strong>REMARK</strong></th>
                        @for ($i = 1; $i < 7; $i++)
                            <th style="width: 11.25%">{{ $i }}</th>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    @foreach ($machinesRight as $machine)
                        <x-machine-row :machine='$machine'/>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>

        <!-- Footer Section -->
        <div class="footer">
            <p>Last Update: {{ \Carbon\Carbon::now()->format('d F Y H:i') }}</p>
        </div>
    </div>
@endsection

@section('js')
<script src="{{ URL::asset('js/print-handler.js') }}"></script>
@endsection
