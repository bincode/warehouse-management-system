@extends('_layouts.base')

@section('title', 'Schedule')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="row text-center">
        <div class="col-12">
            <h1>DELIVERY SCHEDULE</h1>
        </div>
    </div>
    <hr class="bg-info" style="height: 2px;">

    <div class="row">
        <div class="col">
            <div class="table-responsive" id="scrollContainer">
                <table class="table table-borderless" style="table-layout: fixed;">
                    <thead>
                        <tr class='text-center bg-primary'>
                            <!-- Render machine headers -->
                            @foreach ($deliveries as $date => $value)
                                <th class="border border-white" style="width: 150px;">
                                    <span
                                        style="font-size: 14px">{{ \Carbon\Carbon::parse($date)->translatedFormat('l, d F Y') }}</span>
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="date-column">
                            <!-- Render work orders for each machine -->
                            @foreach ($deliveries as $date => $data)
                                <td class="date-column">
                                    @foreach  ($data as $item)
                                        <x-taskDelivery :task='$item' />
                                    @endforeach
                                </td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/delivery-kanban.js') }}"></script>
@endsection
