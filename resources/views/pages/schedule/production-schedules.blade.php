@extends('_layouts.base')

@section('title', 'Kanban Schedule')

@section('content')

    <div class="row text-right">
        <div class="col-6"></div>
        <div class="col-2">
            <a href='{{ route('plan.production.preview') }}' id="previewProductionPlan"
                class="btn btn-block btn-info zoom-responsive">PREVIEW</a>
        </div>
        <div class="col-2">
            <button id="saveProductionPlan" class="btn btn-block btn-success zoom-responsive">SAVE</button>
        </div>
        <div class="col-2">
            <button id="toggleFullscreen" class="btn btn-block btn-dark zoom-responsive">TOGGLE FULLSCREEN</button>
        </div>
    </div>

    <!-- Fullscreen container yang mencakup bagian heading dan konten -->
    <div class="fullscreen-container mt-3" id="fullscreenContainer" style="display: flex;flex-direction: column;">

        <div class="row text-center w-100">
            <div class="col-12 text-primary">
                <hr class="planning-divider">
                <h1 class="planning-title"><strong>PRODUCTION PLANNING</strong></h1>
                <span id="currentTime" class="current-time"></span>
                <hr class="planning-divider">
            </div>
        </div>

        <div class="floating-buttons">
            <div class="status-square-wrapper">
                <div class="status-square btn-planning"></div>
                <div class="status-text">Planning</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-prepare"></div>
                <div class="status-text">Prepared</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-process"></div>
                <div class="status-text">In Progress</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-running"></div>
                <div class="status-text">Running on Ext</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-finished"></div>
                <div class="status-text">Finished</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-hold"></div>
                <div class="status-text">Hold</div>
            </div>
            <div class="status-square-wrapper">
                <div class="status-square btn-canceled"></div>
                <div class="status-text">Canceled</div>
            </div>
        </div>


        <!-- Konten yang sebelumnya ada -->
        <div class="row">
            <div class="col">
                <div class="schedule-production" id="scrollContainer"> </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/kanban-utils.js') }}"></script>
    <script>
        document.getElementById('toggleFullscreen').addEventListener('click', function() {
            const container = document.getElementById('fullscreenContainer');

            if (!document.fullscreenElement) {
                container.requestFullscreen().catch(err => {
                    console.error("Failed to enter fullscreen mode:", err);
                });
            } else {
                document.exitFullscreen().catch(err => {
                    console.error("Failed to exit fullscreen mode:", err);
                });
            }
        });

        function checkFullscreen() {
            // Jika dalam mode fullscreen, tambahkan class 'fullscreen' ke body
            if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document
                .msFullscreenElement) {
                document.body.classList.add('fullscreen');
            } else {
                // Jika keluar dari fullscreen, hapus class 'fullscreen'
                document.body.classList.remove('fullscreen');
            }
        }

        // Event listener untuk mendeteksi perubahan fullscreen
        document.addEventListener('fullscreenchange', checkFullscreen);
        document.addEventListener('webkitfullscreenchange', checkFullscreen);
        document.addEventListener('mozfullscreenchange', checkFullscreen);
        document.addEventListener('MSFullscreenChange', checkFullscreen);

        function updateTime() {
            const now = new Date();

            // Mendapatkan jam, menit, dan detik
            const hours = String(now.getHours()).padStart(2, '0');
            const minutes = String(now.getMinutes()).padStart(2, '0');
            const seconds = String(now.getSeconds()).padStart(2, '0');
            const currentTime = hours + ':' + minutes + ':' + seconds;

            // Mendapatkan tanggal dalam format yang diinginkan
            const options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            };
            const currentDate = now.toLocaleDateString('id-ID', options);

            // Update elemen dengan waktu dan tanggal
            document.getElementById('currentTime').textContent = currentDate + ' ' + currentTime;
        }

        // Panggil updateTime setiap 1 detik
        setInterval(updateTime, 1000);
        // Panggil langsung untuk menampilkan waktu segera
        updateTime();
    </script>
@endsection
