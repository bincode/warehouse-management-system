@extends('_layouts.base')

@section('title', 'Data Receive Items')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-7">

            <x-form title='Receive Items' routePrefix='receives' :param='$receife ?? null'>

                <div class="row">
                    <div class="col-6">
                        <x-input name='date' label="Receive Date" daterangepicker :bind='$component->model' />
                    </div>
                    <div class="col-6">
                        <x-input name='for' label='No. Receive Item' :bind='$component->model' />
                    </div>
                    <div class="col-12">
                        <x-select-company name='description' class='supplier' label='Supplier Name' :bind='$component->model' />
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table-input-material :bind='$component->model' type='receive' />
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-8 align-items-center" style="font-size: 20px;">

                    </div>
                    <div class="col-4 text-right">
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>
    <script src="{{ URL::asset('js/forms/material-release-form.js') }}"></script>
@endsection
