@extends('_layouts.base')

@section('title', 'Data Production Result')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">

            {{-- Form for DMaterial Release with dynamic route and model binding --}}
            <x-form title='Production Result' routePrefix='results' :param='$result ?? null'>

                <div class="row">
                    <div class="col-6">
                        {{-- Date input with datepicker --}}
                        <x-input name='date' label="Result Date" daterangepicker :bind='$component->model' />
                    </div>
                    <div class="col-6">
                        {{-- No. Lot input --}}
                        <x-input name='for' label='No. Lot' :bind='$component->model' />
                    </div>
                </div>

                {{-- Product Name name input --}}
                <div class="row">
                    <div class="col-12">
                        <x-input name='description' label="Product" placeholder='~' class='bg-primary text-center product-selector-search' readonly :bind='$component->model' />
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <x-input name='consumption' label="Consumption" class='form-control-sm bg-info text-center'
                            :bind='$component->model' disabled />
                    </div>
                    <div class="col-4">
                        <x-input name='output' label="Output" class='form-control-sm text-center' :bind='$component->model'
                            disabled />
                    </div>
                    <div class="col-4">
                        <x-input name='loss' label="Loss" class='form-control-sm bg-danger text-center '
                            :bind='$component->model' disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table-product-list name='table-input-goods' :bind='$component->model' />
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-8 align-items-center" style="font-size: 24px;">
                    </div>
                    <div class="col-4 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>

            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>
    <script src="{{ URL::asset('js/forms/production-result-form.js') }}"></script>
@endsection
