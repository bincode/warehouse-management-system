<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FOCSystem | Log in</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>FOCS</b>ystem</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <h3 class="login-box-msg">Sign in</h3>

                <form action="{{ route('attemptLogin') }}" method="POST">
                    @csrf

                    <x-input name="username" type="text" placeholder="Username" icon="fas fa-user" />
                    <x-input name="password" type="password" placeholder="Password" icon="fas fa-lock" />

                    <div class="row">
                        <div class="offset-8 col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
