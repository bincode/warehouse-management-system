@extends('_layouts.base')  
  
@section('title', 'Data Racks')  
  
@section('content')  
    <div class="row d-flex justify-content-center">  
        <div class="col-11">  
  
            {{-- Header --}}  
            <h2> DATA RACKS on <b>'{{ strtoupper($warehouse->name) }} WAREHOUSE' </b> </h2>  
  
            {{-- Tools --}}  
            <div class="row d-flex justify-content-between">  
  
                {{-- Create button --}}  
                <div class="col-4 pl-2 mb-3">  
                    <x-button id="buttonCreate" :url="route('warehouses.racks.create', ['warehouse' => $warehouse->id])" class="btn btn-success" icon="far fa-plus-square" label="Create" />  
                </div>  
  
            </div>  
  
            <!-- Rack table -->  
            <x-card body-class='p-0'>  
                <x-table id='tableRack' />  
            </x-card>  
        </div>  
    </div>  
@endsection  
  
@section('js')  
    <script src="{{ URL::asset('js/tables/table-rack.js') }}"></script>  
@endsection  
