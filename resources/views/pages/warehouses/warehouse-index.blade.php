@extends('_layouts.base')  
  
@section('title', 'Data Warehouses')  
  
@section('content')  
    <div class="row d-flex justify-content-center">  
        <div class="col-11">  
  
            {{-- Header --}}  
            <h2> WAREHOUSE DATA MASTER </h2>  
  
            {{-- Tools --}}  
            <div class="row d-flex justify-content-between">  
  
                {{-- Create button --}}  
                <div class="col-4 pl-2 mb-3">  
                    <x-button id="buttonCreate" :url="route('warehouses.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />  
                </div>  
  
            </div>  
  
            <!-- Warehouse table -->  
            <x-card body-class='p-0'>  
                <x-table id='tableWarehouse' />  
            </x-card>  
        </div>  
    </div>  
@endsection  
  
@section('js')  
    <script src="{{ URL::asset('js/tables/table-warehouse.js') }}"></script>  
@endsection  
