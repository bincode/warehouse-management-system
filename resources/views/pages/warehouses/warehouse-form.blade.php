@extends('_layouts.base')

@section('title', 'Create Warehouses')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">

            <x-form title='Warehouse' routePrefix='warehouses' :param='$warehouse ?? null'>

                <x-input name='name' label='Name' :bind='$component->model' />
                <x-input-area name='address' label='Location Address' placeholder="Address Line" :bind='$component->model' />

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection
