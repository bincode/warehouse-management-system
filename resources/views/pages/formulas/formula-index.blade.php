@extends('_layouts.base')

@section('title', 'Formulation')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card cardTitle="FILTER">
                        <x-input name='search-ajax' label="Search Text" placeholder='Search...' class='form-control search-ajax' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>
        <div class="col-9">
            <h2> FORMULATION</h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-8 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('formulas.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                </div>
            </div>

            <!-- Formula table -->
            <x-card body-class='p-0'>
                <x-table id='tableFormula' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-formula.js') }}"></script>
@endsection
