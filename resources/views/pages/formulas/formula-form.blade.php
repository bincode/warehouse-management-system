@extends('_layouts.base')  
  
@section('title', 'Data Formulation')  
  
@section('content')  
    <div class="row justify-content-center">  
        <div class="col-md-6">  
  
            <x-form title='Formula' routePrefix='formulas' :param='$formula ?? null'>  
  
                <div class="row mb-3">  
                    <div class="col-6">  
                        {{-- Dropdown for selecting product --}}  
                        <x-select name='product_id' class='product-goods' label='Product Name' :bind='$component->model' />  
                    </div>  
                    <div class="col-6">  
                        {{-- Input field for formula version --}}  
                        <x-input name='version' label='Version' :bind='$component->model' />  
                    </div>  
                </div>  
  
                <hr class="bg-primary" style="height: 1px;">  
  
                <div class="row mb-3">  
                    <div class="col-12">  
                        {{-- Table for inputting formulation details --}}  
                        <x-table-input-formula :bind='$component->model' />  
                    </div>  
                </div>  
  
                <hr class="bg-primary" style="height: 1px;">  
  
                <div class="row align-items-center">  
                    @if (!$component->model || !$component->model->workorders()->exists())  
                        {{-- Show Save button if no work orders exist --}}  
                        <div class="col-10" style="font-size: 20px;">  
                            <label for="totalPercentage" class="m-0"></label>  
                        </div>  
                        <div class="col-2 text-end">  
                            <button id="saveButton" type="submit" class="btn btn-primary" disabled>  
                                Save  
                            </button>  
                        </div>  
                    @else  
                        {{-- Display message if formula is in use --}}  
                        <div class="col-12 text-center">  
                            <p class="text-danger fw-bold" style="font-size: 20px;">  
                                VIEW-ONLY, FORMULA HAS BEEN USED IN PRODUCTION.  
                            </p>  
                        </div>  
                    @endif  
                </div>  
            </x-form>  
        </div>  
    </div>  
@endsection  
  
@section('js')  
    {{-- Include JavaScript for form functionality using asset() --}}  
    <script src="{{ asset('js/forms/formula-form.js') }}"></script>  
@endsection  
