@extends('_layouts.base')

@section('title', 'Adjustment')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card cardTitle="FILTER">
                        <x-input name='search-ajax' label="Search Text" placeholder='Search...' class='form-control search-ajax' />
                        <x-input daterangepicker id='tableSrcDateStart' class="datepicker" name='tableSrcDateStart' label='Date From' />
                        <x-input daterangepicker id='tableSrcDateEnd' class="datepicker" name='tableSrcDateEnd' label='Date To' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>

        <div class="col-9">
            <h2>DATA ADJUSTMENT</h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-8 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('adjustments.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                    <x-button id='buttonExport' class='btn btn-primary' icon='fas fa-file-download' label='Download' />
                    <x-button id='buttonImport' class='btn btn-primary' icon='fas fa-file-upload' label='Upload' />
                </div>
            </div>

            {{-- Adjustment table --}}
            <x-card body-class='p-0'>
                <x-table id='tableAdjustment' />
            </x-card>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-adjustment.js') }}"></script>
@endsection
