@extends('_layouts.base')

@section('title', 'Create Adjustment')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">

            {{-- Form for Adjustment with dynamic route and model binding --}}
            <x-form title='Adjustment' routePrefix='adjustments' :param='$adjustment ?? null'>

                <div class="row">
                    <div class="col-6">
                        {{-- Date input with datepicker --}}
                        <x-input name='date' label="Adjustment Order Date" class="datepicker" daterangepicker :bind='$component->model' />
                    </div>
                    <div class="col-6">
                        {{-- Document reference input --}}
                        <x-input name='for' label='No. Document Reference' :bind='$component->model' />
                    </div>
                    <div class="col-12">
                        {{-- Description input --}}
                        <x-input name='description' label='Note' :bind='$component->model' />
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        {{-- Table input for materials --}}
                        <x-table-input-material :bind='$component->model' />
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-8 align-items-center" style="font-size: 20px;">
                        {{-- Space for additional content if needed --}}
                    </div>
                    <div class="col-4 text-right">
                        {{-- Save button --}}
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    {{-- JavaScript for datepicker and table input functionality --}}
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>
    <script src="{{ URL::asset('js/forms/table-input/table-input-materials.js') }}"></script>
@endsection
