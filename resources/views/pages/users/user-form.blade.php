@extends('_layouts.base')

@section('title', 'User Management')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <x-form title="User" routePrefix="users" :param='$user ?? null'>
                @csrf {{-- Menyertakan token CSRF --}}

                <div class="row">  
                    <div class="col-8">  
                        {{-- Username input field --}}  
                        <x-input name='username' label='Username' :bind='$component->model' placeholder="Input New Username..." required />  
  
                        {{-- User role select field --}}  
                        <x-select name='roles[]' label='Select Roles' class='roles' multiple="multiple" :bind='$component->model' placeholder="Select roles..." />  
                    </div>  
                    <div class="col-4">  
                        {{-- Password input field --}}  
                        <x-input type='password' name='password' label='Password' placeholder='******' required />  
  
                        {{-- Password confirmation input field --}}  
                        <x-input type='password' name='password_confirmation' label='Password Confirmation' placeholder='******' required />  
                    </div>  
                </div>  
  
                {{-- Save button --}}  
                <div class="text-right">  
                    <button type="submit" class="btn btn-primary">Save</button>  
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/select2-utils.js') }}"></script>
@endsection
