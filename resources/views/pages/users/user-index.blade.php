@extends('_layouts.base')  
  
@section('title', 'Users Management')  
  
@section('content')  
    <div class="row d-flex justify-content-center">  
        <div class="col-11">  
  
            {{-- Header --}}  
            <h2> USER MANAGEMENT </h2>  
  
            {{-- Tools --}}  
            <div class="row d-flex justify-content-between">  
  
                {{-- Create button --}}  
                <div class="col-4 pl-2 mb-3">  
                    <x-button id="btn_create" :url="route('users.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />  
                </div>  
  
            </div>  
  
            <!-- User table -->  
            <x-card body-class='p-0'>  
                <x-table id='tableUser' />  
            </x-card>  
        </div>  
    </div>  
@endsection  
  
@section('js')  
    <script src="{{ URL::asset('js/tables/table-user.js') }}"></script>  
@endsection  
