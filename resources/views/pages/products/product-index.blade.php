@extends('_layouts.base')

@section('title', 'Data Product')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-11">

            {{-- Header --}}
            <h2> DATA PRODUCT LIST</h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">

                {{-- Create button --}}
                <div class="col-4 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('products.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                </div>

                {{-- Search input --}}
                <div class="col-4">
                    <x-input name='search-ajax' placeholder='Search...' class='form-control search-ajax' />
                </div>

            </div>

            <!-- Role table -->
            <x-card body-class='p-0'>
                <x-table id='tableProduct' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-product.js') }}"></script>
@endsection
