@extends('_layouts.base')

@section('title', 'Data Product')

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <x-form title="Product" routePrefix="products" :param="$product ?? null">

                {{-- Product Type --}}
                <x-select name="inventory_id" class="inventory-type" label="Product Type" :bind="$component->model" />

                {{-- Product Name --}}
                <x-input name="name" type="text" label="Product Name" :bind="$component->model" />

                {{-- Product Description --}}
                <x-input name="description" type="text" label="Description" placeholder="(Optional)" :bind="$component->model" />

                {{-- Product Color --}}
                <x-input name="color" type="text" label="Color" :bind="$component->model" />

                {{-- Default Rack --}}
                <x-select name="rack_id" class="rack" label="Default Rack" placeholder="Select Rack" :bind="$component->model" />

                <div class="row">
                    <div class="col-4">
                        <x-input name="date" label="Date Start Balance" daterangepicker :bind="$component->model" />
                    </div>
                    <div class="col-4">
                        <x-input name="quantity" type="number" class="text-right" label="Opening Stock" :bind="$component->model" />
                    </div>
                    <div class="col-4">
                        <x-input name="unit" type="text" class="text-right" label="Unit" :bind="$component->model" />
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/forms/product-form.js') }}"></script>
@endsection
