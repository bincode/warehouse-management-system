@extends('_layouts.base')

@section('title', 'Sales Order (SO)')

@section('content')
    <div class='row d-flex justify-content-center'>
        <div class='col-8'>

            <x-form title='Sales Order' routePrefix='salesorder' key='order' :param='$order ?? null'>

                <div class="row">
                    <div class="col-8">
                        <div class="row">
                            <div class="col-6">
                                <x-input name='date' label='PO Date' daterangepicker :bind='$component->model' />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <x-input name='po_number' label='PO Number' :bind='$component->model' required />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <x-select name='company_id' class='customer' label='Company Name' :bind='$component->model' required />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <x-table-sales-order-list :bind='$component->model' />
                    </div>
                </div>
                <div class='text-right'>
                    <button type='submit' class='btn btn-primary mt-3'>Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/input-row/input-sales-order.js') }}"></script>


    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <script>
                Swal.fire({
                    icon: 'warning',
                    title: 'Validation Error',
                    html: '{!! addslashes($error) !!}'
                });
            </script>
        @endforeach
    @endif
@endsection
