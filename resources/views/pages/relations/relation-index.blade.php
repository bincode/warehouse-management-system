@extends('_layouts.base')

@section('title', 'Relation Company Data Master')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-10">

            <h2> RELATION COMPANY DATA MASTER </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new relation button -->
                <x-button id="buttonCreate" :url="route('relations.create')" class="bg-success btn-sm" icon="far fa-plus-square" label="Create" />
            </div>

            <!-- Relation table -->
            <x-card body-class='p-0'>
                <x-table id='tableRelation' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-relation.js') }}"></script>
@endsection
