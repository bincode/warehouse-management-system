@extends('_layouts.base')

@section('title', 'Create Rack')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">

            <x-form title='Relation' routePrefix='relations' :param='$relation ?? null'>

                {{-- Company Name input field --}}
                <x-input name='name' type='text' label='Company Name *' :bind='$component->model' />

                {{-- Company Address input field --}}
                <x-input-area name='address' type='textarea' label='Address' :bind='$component->model' />

                {{-- Company Type select field --}}
                <x-select-type-relation name='type' label="Company As ..." :bind='$component->model' />

                <!-- Settings Section -->
                <div class="d-flex align-items-center mb-3">
                    <i class="fas fa-cogs mr-2"></i> <!-- Font Awesome icon -->
                    <strong>Settings</strong>
                </div>

                <div class="custom-control custom-switch">
                    <input id="isDisplay" name="is_display" type="checkbox" class="custom-control-input switch-active" {{ $component->model && $component->model->is_display ? 'checked' : '' }}>
                    <label class="custom-control-label" for="isDisplay">Display PO / Part Number on Workorder</label>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>

            </x-form>
        </div>
    </div>
@endsection
