@extends('_layouts.base')

@section('title', 'Data Stocktaking')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">
            <form action='{{ route('api.stocktaking.export') }}' method='POST'>
                @csrf

                <h2 class="mb-3"> Download Data Stocktaking </h2>

                <x-card bodyClass='p-3'>
                    <div class="row">
                        <div class="col-12">
                            <x-input name='date' label="Cut off Date" daterangepicker />
                        </div>
                    </div>


                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Download</button>
                    </div>
                </x-card>
            </form>
        </div>

        <div class="col-4">
            <form action='{{ route('api.stocktaking.import') }}' method='POST' enctype="multipart/form-data">
                @csrf

                <h2 class="mb-3"> Upload Data Stocktaking </h2>

                <x-card bodyClass='p-3'>
                    <div class="row">
                        <div class="col-12">
                            <x-input name='dateImport' label="Adjustment Date" daterangepicker />
                            <x-input name='file' type='file' class='form-control-plaintext align-center'
                                label="Excel File to Upload" />
                        </div>
                    </div>


                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </x-card>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>
@endsection
