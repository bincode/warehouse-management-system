@extends('_layouts.base')

@section('title', 'Data Work Order')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-9">

            <x-form id="workform" title="Work Order" routePrefix="workorders" :param="$workorder ?? null">
                <x-input name="id" :bind="$component->model" type="hidden" />
                <div class="row">
                    <!-- Left Column: Form Inputs -->
                    <div class="col-md-5 p-3 border rounded shadow-sm">
                        <div class="row mb-4">
                            <!-- Left Column: Product Information -->
                            <div class="col-md-12">
                                <h5 class="mb-3">PRODUCT INFORMATION</h5>
                                <div class="row g-3">
                                    <div class="col-6">
                                        <x-input name="lot" label="No. Lot" :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-input name="date" label="Production Date" daterangepicker :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-select name="formula_id" class="formula-active" label="Product Name / Formulasi" key='productVersion' :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-select name="endProduct_id" class="product-goods" label="End Product Name" :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-input name="color" label="Product Color" class="product-color" />
                                    </div>
                                    <div class="col-6">
                                        <x-input name="plan_quantity" label="Production Order Qty" :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-select name="extruder_id" label="Extrusion Type" class="machine-extruder" :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-input name="mixing_quantity" label="Mixing Qty" :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-select name="mixer_id" label="Mixer Type" class='machine-mixer' :bind="$component->model" />
                                    </div>
                                    <div class="col-6">
                                        <x-select name="machineProcType_id" label="Mixer Process Type" class='machine-process-type' :bind="$component->model" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Customer Information -->
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <h5 class="mb-3">CUSTOMER INFORMATION</h5>
                                <div class="row g-3">
                                    <div class="col-12">
                                        <x-select name="customers[]" label="Customer Name" class='customer' :bind="$component->model" multiple="multiple" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Mixing and PPIC Notes -->
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <h5 class="mb-3">NOTES</h5>
                                <div class="row">
                                    <div class="col-12">
                                        <x-input-area name="mixing_note" label="Mixing Note" :bind="$component->model" />
                                    </div>
                                    <div class="col-12">
                                        <x-input-area name="note" label="PPIC Note" :bind="$component->model" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Additional Information -->
                        @if (!$component->model)
                            <div class="row mb-4">
                                <div class="col-12">
                                    <hr class="border-primary mt-2 mb-2">
                                </div>
                                <div class="col-12">
                                    <x-input name="po_date" label="PO Date" daterangepicker />
                                </div>
                                <div class="col-12">
                                    <x-input name='po_number' label='PO Number / Part Number (Optional)' :bind='$component->model' />
                                </div>
                                <div class="col-6">
                                    <x-select-date-priority name='ship_priority' label="Ship Priority" />
                                </div>
                                <div class="col-6">
                                    <x-input name="ship_date" label="Ship Date" daterangepicker />
                                </div>
                                <div class="col-12">
                                    <x-input-area name="remarks" label="Remark" />
                                </div>
                            </div>
                        @endif
                    </div>

                    <!-- Right Column: Fragment -->
                    <div class="col-md-7 p-3 d-flex flex-column justify-content-between border rounded shadow-sm">
                        <!-- Fragment Title -->
                        <div class="row">
                            <div class="col-12">
                                <h5 class="mb-3">MATERIAL INFORMATION</h5>
                            </div>
                        </div>
                        <!-- Fragment Container -->
                        <div class="row flex-grow-1">
                            <div class="col d-flex justify-content-center align-items-center">
                                <div id="fragmentContainer" class="text-center">
                                    <!-- Default Message Fragment -->
                                    <div class="pt-2">
                                        <i class="fas fa-exclamation-triangle fa-3x text-warning"></i>
                                        <p class="text-muted mt-3">Oops! Material is empty.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Footer for Total Percentage and Save button -->
                        <div class="row g-3 p-3 border rounded">
                            <!-- Label for Total Percentage -->
                            <div class="col-6 d-flex align-items-center justify-content-center">
                                <label for="totalPercentage" class="m-0" style="font-size: 20px;">Total Percentage: 0%</label>
                            </div>

                            <!-- Button to check material stock -->
                            <div class="col-3 text-end">
                                <button id="checkstock" type="button" class="btn btn-block btn-info">Check Stock</button>
                            </div>

                            <!-- Button to submit the form -->
                            <div class="col-3 text-end">
                                <button id="submitwo" type="submit" class="btn btn-block btn-primary">Save</button>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
        </x-form>
    </div>
    </div>
@endsection

@section('js')
    {{-- Include JavaScript for form functionality using asset() --}}
    <script src="{{ asset('js/forms/workorder-form.js') }}"></script>
@endsection
