@extends('_layouts.base')

@section('title', $workorder->productName)

@section('content')
    <div id="printable" class="row justify-content-center"
        style="font-family: 'Tahoma', sans-serif; box-sizing: border-box; height: 100%;overflow: hidden; font-size: fit-content;">
        <div class="col-12 p-3" style="box-sizing: border-box;">
            @if (is_null($workorder->lot))
                <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
                    <div class="text-center">
                        <h3>Please Complete The Work Order.</h3>
                        <a href="{{ route('workorders.edit', $workorder->id) }}" class="btn btn-primary mt-3">Complete Work Order</a>
                    </div>
                </div>
            @else
                <div class="row justify-content-center mb-1">
                    <div class="col-12">
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="text-center">
                                    <td class="align-middle" rowspan="2">
                                        <strong style="font-size: 1.8em;">PT FOCUS COLOR INDONESIA</strong>
                                    </td>
                                    <td></td>
                                    <td class="border border-dark p-1" style="min-width: 15%;">
                                        <strong>F-LAB-01-03</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <strong>{{ $workorder->mixing_note }}</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row justify-content-center mr-1">
                    <div class="col-10">
                        <table class="table table-sm table-borderless minimal-padding">
                            <tbody>
                                <tr>
                                    <td style="width: 20%">Product ID</td>
                                    <td style="width: auto">: {{ $workorder->productName }}</td>
                                    <td style="width: 10%"></td>
                                    <td style="width: 20%">Lot No</td>
                                    <td style="width: auto">: {{ $workorder->lot }}</td>
                                </tr>
                                <tr>
                                    <td>End Product Name</td>
                                    <td>: <strong> {{ $workorder->endProductName }} </strong></td>
                                    <td></td>
                                    <td>PrO Date</td>
                                    <td>: {{ $workorder->longDate }}</td>
                                </tr>
                                <tr>
                                    <td>Color</td>
                                    <td>: {{ $workorder->formula->product->color }}</td>
                                    <td></td>
                                    <td>PrO Start Date</td>
                                    <td>: {{ $workorder->longDate }}</td>
                                </tr>
                                <tr>
                                    <td>Std Batch Qty</td>
                                    <td>: 100 Kg</td>
                                    <td></td>
                                    <td>PrO Due Date</td>
                                    <td>: {{ $workorder->longDueDate }}</td>
                                </tr>
                                <tr>
                                    <td>Prod Order Qty</td>
                                    <td>: {{ $workorder->plan_quantity }} Kg</td>
                                    <td></td>
                                    <td>Customer Name</td>
                                    <td
                                        style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; display: inline-block;">
                                        : @foreach ($workorder->customers as $customer)
                                            {{ $customer->name }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mixing Qty</td>
                                    <td>: {{ $workorder->mixing_quantity }} Kg</td>
                                    <td></td>
                                    @if ($workorder->customers->contains(fn($customer) => $customer->is_display))
                                        <td>No . PO / Part Number</td>
                                        <td>:
                                            {{-- Ambil seluruh po_number dari orders --}}
                                            @foreach ($workorder->salesorders as $order)
                                                {{ $order->po_number }}
                                                @if (!$loop->last)
                                                    {{ ', ' }}
                                                @endif
                                            @endforeach
                                        </td>
                                    @else
                                        <td>End Product Wh</td>
                                        <td>: F0P</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>No Of Mixing</td>
                                    <td>: {{ $workorder->NoOfMixing }} Times
                                    </td>
                                    <td></td>
                                    <td>Extrusion Type</td>
                                    <td>: {{ $workorder->extruderName }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Mixer Type</td>
                                    <td>: {{ $workorder->mixerName }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-2">
                        <div class="row justify-content-center">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr class="border border-dark" style="height: 10%;">
                                        <td class="text-center"><strong>Mix Proc</strong></td>
                                    </tr>
                                    <tr class="border border-dark" style="height: 30%;">
                                        <td class="text-center" style="font-size: 2.8em;">
                                            <strong>{{ $workorder->machineProcType->name ?? '-' }}</strong>
                                        </td>
                                    </tr>
                                    <tr class="border border-dark" style="height: 10%;">
                                        <td class="text-center"><strong>{{ $workorder->extruderName }}</strong></td>
                                    </tr>
                                    <tr class="border border-dark" style="height: 10%;">
                                        <td class="text-center"><strong>{{ $workorder->mixerName }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center" style="line-height: 125%; box-sizing: border-box;">
                    <div class="col-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th class="align-middle" style="width: 3%">No.</th>
                                    <th class="align-middle" style="width: auto">RM Code</th>
                                    <th class="align-middle" style="width: 9%">WH</th>
                                    <th class="align-middle" style="width: 9%">%</th>
                                    <th class="align-middle" style="width: 9%">Quantity Recipe</th>
                                    <th class="align-middle" style="width: 9%">Weight / Mixer</th>
                                    <th class="align-middle" style="width: 9%">No of Weighting</th>
                                    <th class="align-middle" style="width: 18%">Note</th>
                                    <th class="align-middle" style="width: 12%">Qty Used Actual</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($materials as $index => $material)
                                    <tr>
                                        <td class="text-center align-middle"><span>{{ $index + 1 }}</span></td>
                                        <td class="align-middle"><strong>{{ $material['name'] }}</strong> <br>
                                            <small><i>{{ $material['date'] }}</i></small>
                                        </td>
                                        <td class="text-center align-middle">WH</td>
                                        <td class="text-center align-middle">{{ $material['percentage'] }}</td>
                                        <td class="text-center align-middle">{{ $material['recipe_qty'] }}</td>
                                        <td class="text-center align-middle">{{ $material['weight_mixer'] }}</td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="text-center align-middle">{{ $material['actual_qty'] }}</td>
                                    </tr>
                                @endforeach
                                @for ($i = count($materials); $i < 10; $i++)
                                    <tr>
                                        <td class="text-center align-middle"><span>{{ $i + 1 }}</span></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                        <td class="align-middle"></td>
                                    </tr>
                                @endfor
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center align-middle"><strong>100</strong></td>
                                    <td class="text-center align-middle"><strong>{{ $workorder->plan_quantity }}</strong>
                                    </td>
                                    <td class="text-center align-middle">
                                        <strong>{{ $workorder->mixing_quantity }}</strong>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="9"></td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="text-right"><b>Total Raw Material Usage</b></td>
                                    <td colspan="2" class="text-right"><strong>{{ $workorder->rawMaterialUsage }}
                                            Kg</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="7" class="text-right"><b>End Product Quantity Result</b></td>
                                    <td colspan="2" class="text-right"><strong>{{ $workorder->QuantityResult }}
                                            Kg</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-2 d-flex justify-content-center align-items-center" style="height: 100px;">
                        {!! SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($workorder->lot)  !!}
                    </div>
                    <div class="col-8">
                        <table class="table table-bordered  mt-0 p-0">
                            <thead class="text-center">
                                <tr>
                                    <th>PrO Created By</th>
                                    <th>QC Approved By</th>
                                    <th>PrO Completed By</th>
                                    <th>PrO Finished By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="height: 100px"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-2"></div>
                </div>
                <div class="row">
                    <div class="col-8">
                        @if ($workorder->note)
                            <div class="col-2 text-right">
                                <span style="white-space: pre-line;font-size: 1.2em;"><strong>Note : </strong></span>
                            </div>
                            <div class="col-10">
                                <span
                                    style="white-space: pre-line;font-size: 1.2em;"><strong>{{ $workorder->note ?? '-' }}</strong></span>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
