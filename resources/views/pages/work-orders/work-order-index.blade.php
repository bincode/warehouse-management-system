@extends('_layouts.base')  
  
@section('title', 'Data Work Order')  
  
@section('content')  
    <div class="row justify-content-center">  
        <div class="col-md-2">  
            <div class="row">  
                <div class="col-12">  
                    {{-- Filter --}}  
                    <x-card cardTitle="FILTER">  
                        <x-input name='search-ajax' label="Search Text" placeholder='Search...' class='form-control search-ajax' />  
                        <x-input daterangepicker id='tableSrcDateStart' class="datepicker" name='tableSrcDateStart' label='Date From' />  
                        <x-input daterangepicker id='tableSrcDateEnd' class="datepicker" name='tableSrcDateEnd' label='Date To' />  
                    </x-card>  
                    {{-- ./Filter --}}  
                </div>  
            </div>  
        </div>  
        <div class="col-md-9">  
            <h2>WORK ORDER</h2>  
  
            {{-- Tools --}}  
            <div class="row justify-content-between mb-3">  
                {{-- Create button --}}  
                <div class="col-8 pe-2">  
                    <x-button id="buttonCreate" :url="route('workorders.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />  
                </div>  
            </div>  
  
            {{-- Delivery Order table --}}  
            <x-card body-class='p-0'>  
                <x-table id='tableWorkOrder' />  
            </x-card>  
        </div>  
    </div>  
@endsection  
  
@section('js')  
    {{-- Include JavaScript for table functionality using asset() --}}  
    <script src="{{ asset('js/tables/table-workorder.js') }}"></script>  
@endsection  
