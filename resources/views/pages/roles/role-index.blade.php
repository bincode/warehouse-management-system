@extends('_layouts.base')

@section('title', 'Roles Management')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> ROLE MANAGEMENT </h2>

            <div class="row pl-2 mb-3">
                <!-- Create new role button -->
                <x-button id="buttonCreate" :url="route('roles.create')" class="bg-success btn-sm" icon="far fa-plus-square" label="Create" />
            </div>

            <!-- Role table -->
            <x-card body-class='p-0'>
                <x-table id='tableRole' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-role.js') }}"></script>
@endsection
