@extends('_layouts.base')

@section('title', 'Create Machine')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">

            <x-form title='Machine Category' routePrefix='machine-process-type' :param='$machine_process_type ?? null'>

                <!-- Input for Machine Name -->
                <x-input name="name" label="Name" :bind="$component->model" />

                <!-- Input for Machine Description -->
                <x-input name="description" type="text" label="Description" placeholder="(Optional)" :bind="$component->model" />

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection
