@extends('_layouts.base')

@section('title', 'Data Machine Process Type')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-md-9">

            <h2> DATA PROCESS TYPE MASTER </h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-4 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('machine-process-type.create')" class="btn btn-success" icon="fas fa-plus-square" label="Create" />
                </div>
            </div>

            <!-- Machine Process Type table -->
            <x-card body-class="p-0">
                <x-table id="tableMachineProcessType" />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <!-- Include JavaScript file for handling machine process type table -->
    <script src="{{ URL::asset('js/tables/table-machine-process-type.js') }}"></script>
@endsection
