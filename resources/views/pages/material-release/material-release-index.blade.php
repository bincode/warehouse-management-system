@extends('_layouts.base')

@section('title', 'Release Material')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card cardTitle="FILTER">
                        <x-input name='search-ajax' label="Search Text" placeholder='Search...' class='form-control search-ajax' />
                        <x-input daterangepicker id='tableSrcDateStart' class="datepicker" name='tableSrcDateStart' label='Date From' />
                        <x-input daterangepicker id='tableSrcDateEnd' class="datepicker" name='tableSrcDateEnd' label='Date To' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>

        <div class="col-9">
            <h2>DATA RELEASE MATERIAL</h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-8 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('releases.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                </div>
            </div>

            {{-- Release Material table --}}
            <x-card body-class='p-0'>
                <x-table id='tableRelease' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-release.js') }}"></script>
@endsection
