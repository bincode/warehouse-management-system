@extends('_layouts.base')

@section('title', 'Data Release Material')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">

            {{-- Form for DMaterial Release with dynamic route and model binding --}}
            <x-form id='release' title='Release Material' routePrefix='releases' :param='$release ?? null'>

                <div class="row">
                    <div class="col-6">
                        {{-- Date input with datepicker --}}
                        <x-input name='date' label="Release Date" daterangepicker :bind='$component->model' />
                    </div>
                    <div class="col-6">
                        {{-- No. Lot input --}}
                        <x-input name='for' label='No. Lot' :bind='$component->model' />
                    </div>
                    <div class="col-12">
                        {{-- Product Name name input --}}
                        <x-select-product name='description' class='product-goods' label='Product' :bind='$component->model' />
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="fragmentContainer" class="justify-content-center align-items-center">
                            <x-table-input-material :bind='$component->model' type='release' />
                        </div>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-9 align-items-center" style="font-size: 20px;">
                        <label for="total">Material Weight : 0 Kg</label>
                    </div>
                    <div class="col-3 text-right">
                        <button type="submit" class="btn btn-block btn-primary">Save</button>
                    </div>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>
    <script src="{{ URL::asset('js/forms/material-release-form.js') }}"></script>
@endsection
