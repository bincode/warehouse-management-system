@extends('_layouts.base')

@section('title', 'Data Release Material')

@section('content')
    <div class="d-flex justify-content-center align-items-center">
        <div class="col-7">

            <x-form id='release' title='Release Material' routePrefix='releases' :param='$release ?? null'>

                <div class="row">
                    <div class="col-12">
                        <span class="mb-3" style="font-size: 2rem; resize: none; min-height: 50px;"><b>INPUT LOT.</b></span>
                        <x-input name='for' class="text-center" :bind='$component->model'
                            style="font-size: 2rem;resize: none; min-height: 50px;" />
                    </div>
                    <div class="col-12">
                        <x-input type="hidden" name='description' :bind='$component->model' />
                        <span class="mb-3" style="font-size: 2rem; resize: none; min-height: 50px;"><b>PRODUCT NAME</b></span>
                        <x-input name='id' class="text-center" :bind='$component->model' readonly
                            style="font-size: 2rem;resize: none; min-height: 50px;" />
                    </div>
                </div>

            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/fragments/material-release-fragment.js') }}"></script>
@endsection
