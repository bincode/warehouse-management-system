@extends('_layouts.base')

@section('title', 'History')

@section('content')

    <div class="row">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card>
                        <x-input daterangepicker id='tableSrcDateStart' name='tableSrcDateStart' label='Date From' class='form-control-sm' />
                        <x-input daterangepicker id='tableSrcDateEnd' name='tableSrcDateEnd' label='Date To' class='form-control-sm' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>
        <div class="col-10">
            <h2><b>{{ $product->name }}</b> <span>( {{ $product->description }} )</span></h2>

            {{-- Table --}}
            <x-card body-class='p-0'>
                <x-table id='tbProductHistory' />
            </x-card>
            {{-- ./Table --}}
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-product-history.js') }}"></script>
@endsection
