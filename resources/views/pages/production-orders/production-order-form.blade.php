@extends('_layouts.base')

@section('title', 'Production Order')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-6">

            <x-form title='Production Order' routePrefix='productionorder' key='product' :param='$product ?? null'>

                <div class="row">
                    <div class="col-12">
                        <x-input id='product_id' name='id' type='hidden' :bind='$component->model' />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-select name="sales_order_id[]" class="sales_po_prepare" label="PO Number" :bind='$component->model' multiple="multiple" required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-input name='product' type='text' label='Product Name' :bind='$component->model' readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-input name='customer' type='text' label='Customer Name' :bind='$component->model' readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <x-input name='plan_quantity' label='Planning Quantity' :bind='$component->model' />
                    </div>
                    <div class="col-4">
                        <x-input name='order_quantity' label='Order Quantity' :bind='$component->model' readonly />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <x-select-machine name='extruder_id' label='Extrution Type' class='extruder' value='1' :bind='$component->model' required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-right">
                        <button type="submit" name="action" value="production_planning" class="btn btn-primary mt-3">Save
                            for Production Planning</button>
                        <button type="submit" name="action" value="work_order" class="btn btn-success mt-3">Save for Work
                            Order</button>
                    </div>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/alert-utils.js') }}"></script>
    <script src="{{ URL::asset('js/utils/select2-utils.js') }}"></script>
    <script src="{{ URL::asset('js/utils/daterangepicker-utils.js') }}"></script>

    @if ($errors->has('swal'))
        @foreach ($errors->get('swal') as $error)
            <script>
                Swal.fire({
                    icon: 'warning',
                    title: 'Validation Error',
                    html: '{!! $error !!}'
                });
            </script>
        @endforeach
    @endif
@endsection
