@extends('_layouts.base')

@section('title', 'Production Order')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-2">
            <div class="row">
                <div class="col-12">
                    {{-- Filter --}}
                    <x-card cardTitle="FILTER">
                        <x-input name='search-ajax' label="Search Text" placeholder='Search...' class='form-control search-ajax' />
                    </x-card>
                    {{-- ./Filter --}}
                </div>
            </div>
        </div>
        <div class="col-8">

            {{-- Header --}}
            <h2> PRODUCTION ORDER </h2>

            {{-- Table --}}
            <x-card body-class='p-0'>
                <x-table id='tableProductionOrder' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-production-order.js') }}"></script>
@endsection
