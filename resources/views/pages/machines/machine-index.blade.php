@extends('_layouts.base')

@section('title', 'Data Machines')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> MACHINE DATA MASTER </h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-4 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('machines.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                    <x-button id="buttonCategory" :url="route('machine-category.index')" class="btn btn-info" icon="far fa-plus-square" label="Category" />
                </div>
            </div>

            <!-- Machine table -->
            <x-card body-class='p-0'>
                <x-table id='tableMachine' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-machine.js') }}"></script>
@endsection
