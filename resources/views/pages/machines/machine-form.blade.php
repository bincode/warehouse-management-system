@extends('_layouts.base')

@section('title', 'Create Machine')

@section('content')
    <div class='row d-flex justify-content-center'>
        <div class='col-4'>

            <x-form title='Machine' routePrefix='machines' :param='$machine ?? null'>

                <x-input name='name' label='Name' :bind='$component->model' required/>
                <x-input-area name='description' label='Desription' placeholder='Address Line' :bind='$component->model' required/>
                <x-select name='category_id' label='Machine Category' class="machine-category" :bind='$component->model' required/>

                <div class='text-right'>
                    <button type='submit' class='btn btn-primary mt-3'>Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/utils/select2-utils.js') }}"></script>
@endsection
