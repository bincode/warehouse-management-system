@extends('_layouts.base')

@section('title', 'Data Machines Category')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-9">

            <h2> MACHINE DATA CATEGORY MASTER </h2>

            {{-- Tools --}}
            <div class="row d-flex justify-content-between">
                {{-- Create button --}}
                <div class="col-4 pl-2 mb-3">
                    <x-button id="buttonCreate" :url="route('machine-category.create')" class="btn btn-success" icon="far fa-plus-square" label="Create" />
                </div>
            </div>

            <!-- Category table -->
            <x-card body-class='p-0'>
                <x-table id='tableMachineCategory' />
            </x-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ URL::asset('js/tables/table-machine-category.js') }}"></script>
@endsection
