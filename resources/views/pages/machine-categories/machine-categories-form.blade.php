@extends('_layouts.base')

@section('title', 'Create Machine')

@section('content')
    <div class="row d-flex justify-content-center">
        <div class="col-4">

            <x-form title='Machine Category' routePrefix='machine-category' :param='$machine_category ?? null'>

                <x-input name='name' label='Name' :bind='$component->model' />

                <div class="text-right">
                    <button type="submit" class="btn btn-primary mt-3">Save</button>
                </div>
            </x-form>
        </div>
    </div>
@endsection
