<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="#" class="brand-link">
        <img src="#" alt="" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">FOCSystem</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src='https://ui-avatars.com/api/?name={{ Auth::user()->username }}&background=0D8ABC&color=fff'
                    class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->username }}</a>
                @foreach (Auth::user()->roles as $role)
                    <span class="text-muted">{{ ucwords($role->name) }}</span>
                @endforeach
            </div>
        </div>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <x-sidebar-menu label='Home' :url="route('home')" icon='fas fa-home' />

                @can('view laboratory')
                    <li class="nav-header">LABORATORY</li>
                    <x-sidebar-menu label='Formula' :url="route('formulas.index')" icon='fas fa-flask' />
                @endcan

                @can('view marketing')
                    <li class="nav-header">MARKETING</li>
                    <x-sidebar-menu label='Sales Order (SO)' :url="route('salesorder.index')" icon='fas fa-clipboard-list' />
                @endcan

                @can('view ppic')
                    <li class="nav-header">PPIC</li>
                    <x-sidebar-menu label='Production Order' :url="route('productionorder.index')" icon='fas fa-clipboard-list' />
                    <x-sidebar-menu label='Production Planning' :url="route('plan.production')" icon='fas fa-calendar-alt' />
                @endcan

                @can('view manufacture')
                    <li class="nav-header">MANUFACTURE</li>
                    <x-sidebar-menu label='Work Order (WO)' :url="route('workorders.index')" icon='fas fa-clipboard-list' />
                    <x-sidebar-menu label='Release Material' :url="route('releases.index')" icon='fas fa-box-open' />
                    <x-sidebar-menu label='Product Result' :url="route('results.index')" icon='fas fa-box' />
                    <x-sidebar-menu label='Job Cost' :url="route('jobcosts.index')" icon='fas fa-drafting-compass' />
                @endcan

                @can('view warehouse')
                    <li class="nav-header">WAREHOUSE</li>
                    <x-sidebar-menu label='Receive item' :url="route('receives.index')" icon='fas fa-truck-loading' />
                    {{-- <x-sidebar-menu label='Delivery Request' :url="route('deliveryrequest.index')" icon='fas fa-clipboard-list' />
                    <x-sidebar-menu label='Delivery Planning' :url="route('plan.delivery')" icon='fas fa-calendar-alt' /> --}}
                    <x-sidebar-menu label='Delivery Order' :url="route('deliveries.index')" icon='fas fa-truck' />
                    <x-sidebar-menu label='Adjustment' :url="route('adjustments.index')" icon='fas fa-pencil-ruler' />
                @endcan

                <li class="nav-header"></li>
                <x-sidebar-menu label='Product' :url="route('products.index')" icon='fas fa-boxes' />

                @can('view administration')
                    <li class="nav-header">ADMINISTRATION</li>
                    <x-sidebar-menu label='Users Management' :url="route('users.index')" icon='fas fa-users' />
                    <x-sidebar-menu label='Roles Management' :url="route('roles.index')" icon='fas fa-user-tag' />
                @endcan

                <li class="nav-header">DATA MASTER</li>
                <x-sidebar-menu label='Data Machine' :url="route('machines.index')" icon='far fa-circle' />
                <x-sidebar-menu label='Data Machine Process Type' :url="route('machine-process-type.index')" icon='far fa-circle' />
                <x-sidebar-menu label='Warehouse' :url="route('warehouses.index')" icon='far fa-circle' />
                <x-sidebar-menu label='Relation Company' :url="route('relations.index')" icon='far fa-circle' />

                <li class="nav-header">PROFILE</li>
                <x-sidebar-menu label='Logout' :url="route('logout')" icon='fas fa-sign-out-alt' />

            </ul>
        </nav>
    </div>
</aside>
