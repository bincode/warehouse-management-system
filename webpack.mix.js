const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
        'jquery': ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery", 'global.jQuery', "global.$"]
    })
    .js('resources/js/app.js', 'public/js')

    // Javascript (JS) - Print
    .js('resources/js/print-handler.js', 'public/js')

    // Javascript (JS) - Fragments
    .js('resources/js/fragments/work-order-fragment.js', 'public/js/fragments')
    .js('resources/js/fragments/material-release-fragment.js', 'public/js/fragments')
    .js('resources/js/fragments/production-result-fragment.js', 'public/js/fragments')

    // Javascript (JS) - Form
    .js('resources/js/forms/formula-form.js', 'public/js/forms')
    .js('resources/js/forms/material-release-form.js', 'public/js/forms')
    .js('resources/js/forms/production-result-form.js', 'public/js/forms')
    .js('resources/js/forms/workorder-form.js', 'public/js/forms')

    // Javascript (JS) - Utils
    .js('resources/js/alerts/alert-utils.js', 'public/js/utils')
    .js('resources/js/daterangepicker/daterangepicker-utils.js', 'public/js/utils')
    .js('resources/js/kanban/delivery-kanban.js', 'public/js/utils')
    .js('resources/js/kanban/kanban-utils.js', 'public/js/utils')
    //.js('resources/js/selects/select2-utils.js', 'public/js/utils')
    .js('resources/js/utils/select2-utils.js', 'public/js/utils')
    .js('resources/js/validations/ajax-validation-utils.js', 'public/js/utils')

    // Javascript (JS) - Forms
    .js('resources/js/forms/product-form.js', 'public/js/forms')

    // Javascript (JS) - Forms - Table Input
    .js('resources/js/forms/table-input/table-input-materials.js', 'public/js/forms/table-input')

    // Javascript (JS) - Table
    .js('resources/js/tables/table-adjustment.js', 'public/js/tables')
    .js('resources/js/tables/table-delivery.js', 'public/js/tables')
    .js('resources/js/tables/table-delivery-schedule.js', 'public/js/tables')
    .js('resources/js/tables/table-formula.js', 'public/js/tables')
    .js('resources/js/tables/table-jobcost.js', 'public/js/tables')
    .js('resources/js/tables/table-machine-category.js', 'public/js/tables')
    .js('resources/js/tables/table-machine-process-type.js', 'public/js/tables')
    .js('resources/js/tables/table-machine.js', 'public/js/tables')
    .js('resources/js/tables/table-product.js', 'public/js/tables')
    .js('resources/js/tables/table-product-history.js', 'public/js/tables')
    .js('resources/js/tables/table-production-order.js', 'public/js/tables')
    .js('resources/js/tables/table-rack.js', 'public/js/tables')
    .js('resources/js/tables/table-receive.js', 'public/js/tables')
    .js('resources/js/tables/table-relation.js', 'public/js/tables')
    .js('resources/js/tables/table-release.js', 'public/js/tables')
    .js('resources/js/tables/table-result.js', 'public/js/tables')
    .js('resources/js/tables/table-role.js', 'public/js/tables')
    .js('resources/js/tables/table-sales-order.js', 'public/js/tables')
    .js('resources/js/tables/table-user.js', 'public/js/tables')
    .js('resources/js/tables/table-warehouse.js', 'public/js/tables')
    .js('resources/js/tables/table-workorder.js', 'public/js/tables')

    // Javascript (JS) - Input Row Table
    .js('resources/js/table-inputs/input-formula.js', 'public/js/input-row')
    .js('resources/js/table-inputs/input-material.js', 'public/js/input-row')
    .js('resources/js/table-inputs/input-goods.js', 'public/js/input-row')
    .js('resources/js/table-inputs/input-sales-order.js', 'public/js/input-row')

    // CSS Style
    .sass('resources/css/app.scss', 'public/css')
    .sass('resources/css/print.scss', 'public/css');
