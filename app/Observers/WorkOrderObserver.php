<?php

namespace App\Observers;

use App\Http\Requests\FormulaRequest;
use App\Http\Requests\SalesOrderRequest;
use App\Models\Formula;
use App\Models\SalesOrder;
use App\Models\WorkOrder;
use App\Services\NestedFormulaService;
use App\Validators\FormulaValidator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class WorkOrderObserver
{
    public function created(WorkOrder $workOrder)
    {
        // Instansiasi manual dari SalesOrderRequest  
        $request = new SalesOrderRequest();  
  
        // Simulasikan data permintaan  
        $request->merge(request()->all()); // Menggabungkan data permintaan yang ada  
  
        // Panggil prepareForValidation secara manual  
        $request->prepareForValidation();  
  
        // Buat validator menggunakan aturan dari SalesOrderRequest  
        $validator = Validator::make($request->all(), $request->rules());

        // Get the necessary data for the new SalesOrder
        $salesOrder = SalesOrder::create($validator->validated());

        // Prepare the pivot data
        $pivotData = [
            'product_id' => request('endProduct_id'),
            'amount' => request('plan_quantity'),
            'ship_priority' => request('ship_priority'),
            'remarks' => request('remark'),
            'work_order_id' => $workOrder->id,
            'status' => $workOrder->setProductStatus()
        ];

        // Check if 'ship_date' exists in the request
        if (request()->has('ship_date')) {
            $pivotData['ship_date'] = Carbon::createFromFormat('d/m/Y', request('ship_date'))->format('Y-m-d');
        }

        // Attach the newly created SalesOrder to the WorkOrder with additional pivot data
        $workOrder->salesorders()->attach($salesOrder->id, $pivotData);
    }

    public function saving(WorkOrder $workOrder)
    {
        $workOrder->setOrder();
    }

    public function saved(WorkOrder $workOrder)
    {
        // Update orders status to "process"
        $workOrder->salesorders()->where('product_id', $workOrder->endProduct->id)->update([
            'status' => $workOrder->setProductStatus(),
        ]);

        $workOrder->endProduct()->update([
            'color' => request('color')
        ]);

        $customers = request('customers', []);
        $workOrder->customers()->sync($customers);

        $service = new NestedFormulaService();
        $service->create($workOrder);

        // Retrieve the existing formula ID from the work order
        $existingFormulaId = $workOrder->formula_id;

        // If no existing formula, return
        if (!$existingFormulaId) {
            return;
        }

        // Retrieve the existing formula
        $existingFormula = Formula::find($existingFormulaId);

        // If existing formula not found, return
        if (!$existingFormula) {
            return;
        }

        // Validate the request data against the existing formula
        $validator = new FormulaValidator();
        $result = $validator->validate(request(), $existingFormula);

        // If the existing formula is different from the input, return
        if ($result) {
            return;
        }

        // Validate the incoming request data against the defined rules
        $formulaRequest = new FormulaRequest();
        $validatedData = [
            'product_id' => $existingFormula->product_id,
            'version'    => request('lot'),
        ];
        $validator = Validator::make($validatedData, $formulaRequest->rules());

        // If validation fails, return
        if ($validator->fails()) {
            return;
        }

        Formula::observe(FormulaObserver::class);

        // Update or create the formula based on the validated data
        $formula = Formula::updateOrCreate(
            ['version' => $validatedData['version'], 'product_id' => $validatedData['product_id']],
            $validator->validated()
        );

        // Update the work order with the new formula ID
        $workOrder->update(['formula_id' => $formula->id]);
    }

    public function deleting(WorkOrder $workOrder)
    {
        // Remove all associated products
        $workOrder->products()->sync([]);
    }
}
