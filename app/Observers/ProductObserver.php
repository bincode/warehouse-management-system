<?php

namespace App\Observers;

use App\Models\InitializeStock;
use App\Models\Product;

class ProductObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        // Check if the request has 'date' and 'quantity' data.
        if (!request()->hasAny(['date', 'quantity'])) return;

        // Create an 'InitializeStock' transaction with 'date' from the request.
        $transaction = InitializeStock::create(['date' => request('date')]);

        // Attach the newly created product to the transaction with specified details.
        $transaction->products()->attach([
            $product->id => [
                'quantity'  => request('quantity'),
                'rack_id'   => request('rack_id'),
                'type'      => $transaction->type,
            ],
        ]);
    }

    /**
     * Handle the Product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        // Check if the request has 'date' and 'quantity' data.
        if (!request()->hasAny(['date', 'quantity'])) return;

        // Get the initial transaction related to the product.
        $transaction = $product->initializeStock->first();

        // Update the date of the initial transaction with the new date from the request.
        $transaction->update(['date' => request('date')]);

        // Update the product's quantity and rack ID in the pivot table of the transaction.
        $transaction->products()->sync([
            $product->id => [
                'quantity'  => request('quantity'),
                'rack_id'   => request('rack_id'),
                'type'      => $transaction->type,
            ]
        ]);
    }
}
