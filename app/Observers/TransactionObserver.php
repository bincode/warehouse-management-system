<?php

namespace App\Observers;

use App\Enums\Status;
use App\Models\MaterialRelease;
use App\Models\ProductResult;
use App\Services\WorkOrderService;

class TransactionObserver
{
    public function creating($event)
    {
        if ($event instanceof MaterialRelease) {

            if ($event->workorder) {
                // Get the planned quantity from the event or elsewhere
                $planQuantity = $event->workorder->planned_quantity; // Adjust this to your actual attribute name

                // Load the formula and its materials
                $event->workorder->formula->load('materials');

                // Calculate the needed amounts for each material
                $materials = $event->workorder->formula->materials->map(function ($product) use ($planQuantity) {
                    $need = $product->percentage * $planQuantity / 100;
                    $product->amount = $need; // Setting the amount for the product
                    return $product;
                });

                request()->merge([
                    'products' => request('products', $materials->pluck('id')->toArray()),
                    'amount' => request('amount', $materials->pluck('amount')->toArray()),
                    'racks' => request('racks', array_fill(0, $materials->count(), 1)),
                ]);
            }
        }
    }

    public function saved($event)
    {
        // Get the materials, quantity, and racks from the request
        $products = request('products');
        $quantity = request('amount');
        $locations = request('racks');

        // Sync the products with the data array
        $event->products()->sync([]);

        // Iterate over the quantity array and create an associative array with product data
        for ($index = 0, $count = count($quantity) - 1; $index < $count; $index++) {
            $event->products()->attach($products[$index], [
                'quantity' => $quantity[$index],
                'rack_id' => $locations[$index],
                'type' => $event->type,
            ]);
        }

        if ($event instanceof MaterialRelease) {
            $service = new WorkOrderService();

            $workOrder = $event->workorder;

            if ($workOrder) {
                $workOrder->has_adjustment = $service->AdjCheck($workOrder);
                $workOrder->save();
            }
        }
    }

    public function created($event)
    {
        if ($event instanceof MaterialRelease) {
            // Periksa apakah workorder ada
            if ($event->workorder) {
                $event->workorder->update([
                    "status" => Status::PROCESS()
                ]);

                // Periksa apakah salesorders ada
                if ($event->workorder->salesorders) {
                    $event->workorder->salesorders()->update([
                        "status" => Status::PROCESS()
                    ]);
                }

                $event->workorder->setOrder();
            }

            $service = new WorkOrderService();
            $workOrder = $event->workorder;
            $workOrder->has_adjustment = $service->AdjCheck($workOrder);
            $workOrder->save();
        }

        if ($event instanceof ProductResult) {
            // Periksa apakah workorder ada
            if ($event->workorder) {
                $event->workorder->update([
                    "status" => Status::FINISHED()
                ]);

                // Periksa apakah salesorders ada
                if ($event->workorder->salesorders) {
                    $event->workorder->salesorders()->update([
                        "status" => Status::FINISHED()
                    ]);
                }

                $event->workorder->setOrder();
            }
        }
    }

    public function deleting($event)
    {
        if ($event instanceof ProductResult) {
            // Periksa apakah workorder ada
            if ($event->workorder) {
                $event->workorder->update([
                    "status" => Status::PROCESS()
                ]);

                // Periksa apakah salesorders ada
                if ($event->workorder->salesorders) {
                    $event->workorder->salesorders()->update([
                        "status" => Status::PROCESS()
                    ]);
                }

                $event->workorder->setOrder();
            }
        }

        // Hapus semua produk yang terkait
        $event->products()->sync([]);
    }
}
