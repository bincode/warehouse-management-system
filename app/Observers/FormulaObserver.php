<?php

namespace App\Observers;

use App\Models\Formula;

class FormulaObserver
{
    public function saved(Formula $event)
    {
        // Get the products and percentages from the request
        $products = request('products');
        $percentage = request('percentage');

        // Sync the products with the data array
        $event->materials()->sync([]);

        // Iterate over the percentage array and create an associative array with product data
        for ($index = 0, $count = count($percentage) - 1; $index < $count; $index++) {
            $event->materials()->attach($products[$index], [
                'percentage' => $percentage[$index],
            ]);
        }
    }

    public function deleting($event)
    {
        // Remove all associated products
        $event->materials()->sync([]);
    }
}
