<?php

namespace App\Observers;

use App\Enums\Status;
use App\Models\Product;
use App\Models\SalesOrder;
use Carbon\Carbon;

class SalesOrderObserver
{
    public function saved(SalesOrder $event)
    {
        // Retrieve products, quantities, ship dates, and remarks from the request
        $products = request('products');
        $amounts = request('amount');
        $shipDates = request('ship_date');
        $shipPriorities = request('ship_priority');
        $remarks = request('remark');

        // Load products with their orders
        $products = Product::with('orders')->find($products);

        // Format the ship dates to 'Y-m-d'
        $shipDates = array_map(function ($date) {
            return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        }, $shipDates);

        $syncData = [];

        foreach ($products as $index => $product) {
            // Default Status
            $status = Status::CREATED();

            // Check if there is a work order for the current product
            $status = $event->setProductStatus($product, $amounts[$index]);

            // Prepare pivot data
            $pivotData = [
                'amount' => $amounts[$index],
                'ship_priority' => $shipPriorities[$index],
                'ship_date' => $shipDates[$index] ?? null, // Handle undefined indices
                'remarks' => $remarks[$index] ?? null,     // Handle undefined indices
                'status' => $status
            ];

            // Collect pivot data for sync
            $syncData[$product->id] = $pivotData;
        }

        // Sync products with collected pivot data
        $event->products()->sync($syncData);
    }

    public function deleting($event)
    {
        // Remove all associated products
        $event->products()->sync([]);
    }
}
