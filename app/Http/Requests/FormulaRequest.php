<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormulaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'product_id' => [
                'required',
            ],
            'version' => [
                'required',
            ],
            'is_active' => [
                'sometimes',
            ],
        ];

        // Return the validation rules for the request
        return $rules;
    }
}
