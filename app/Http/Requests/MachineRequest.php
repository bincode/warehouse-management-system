<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MachineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'description' => [
                'required',
                'string',
            ],
            'category_id' => [
                'required',
                'exists:machine_categories,id',
            ],
        ];

        // If the request is for updating an existing machine, add the machine ID to the unique validation rule for the name field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['name'][] = 'unique:machines,name,' . $this->machine->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
