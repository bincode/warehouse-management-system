<?php

namespace App\Http\Requests;

use App\Enums\RelationType;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Enum\Laravel\Rules\EnumRule;

class RelationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation()
    {
        // Convert 'on' to true and ensure it's a boolean value
        $this->merge([
            'is_display' => $this->has('is_display') ? true : false,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'name' => [
                'required',
                'unique:relations,name'
            ],
            'address' => [
                'nullable',
            ],
            'type' => [
                'required',
                new EnumRule(RelationType::class)
            ],
            'is_display' => [
                'required',
                'boolean'
            ]
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the code field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['name'][1] .= ',' . $this->relation->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
