<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductResultRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'for' => [
                'required',
                'unique:results,for'
            ],
            'date' => [
                'required',
                'date_format:d/m/Y'
            ],
            'description' => [
                'required'
            ],
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the name field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['for'][1] .= ',' . $this->result->id;
        }

        // Return the validation rules for the request
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'for'           => 'No. Lot',
            'description'    => 'End Product'
        ];
    }
}
