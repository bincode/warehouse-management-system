<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    public function prepareForValidation()
    {
        info('prepareForValidation');
        // Check if 'customers' exists and is an array
        if ($this->has('customers') && is_array($this->customers)) {
            // Take the first customer ID from the 'customers' array
            $this->merge([
                'company_id' => $this->customers[0], // Take the first element of the 'customers' array
            ]);
        }

        // Merge 'po_date' into 'date' field
        $this->merge([
            'date' => $this->po_date,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        info('rules');
        // Set the validation rules for the request
        $rules = [
            'po_number' => [
                'required',
            ],
            'date' => [
                'required',
                'date_format:d/m/Y'
            ],
            'company_id' => [
                'required',
            ],
        ];

        // Return the validation rules for the request
        return $rules;
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Get the current order from the route
            $order = $this->route('order');

            if ($order) {
                // Retrieve the products associated with the order
                $products = $order->products;

                // Iterate over each product to perform custom validation
                foreach ($products as $index => $product) {
                    // Check if the product can update the sales order with the specified amount
                    if (!$product->canUpdateSalesOrder($this->amount[$index])) {
                        // Add a custom error message if the product cannot be updated
                        $validator->errors()->add("amount.{$index}", "<span class='badge badge-warning'>{$product->name}</span> Can`t change because it has been processed");
                    }
                }
            }
        });
    }
}
