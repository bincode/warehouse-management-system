<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductionOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            "plan_quantity" => [
                "required",
                "numeric",
                "gte:order_quantity"
            ],
            "extruder_id" => [
                "required",
                "exists:machines,id"
            ],
            "sales_order_id" => [
                "required",
                "array"
            ]
        ];

        return $rules;
    }

    /**
     * Get custom error messages for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            "plan_quantity" => "Planning Quantity",
        ];
    }

    /**
     * Add additional validation after basic validation passes.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            // Check if the basic validation has failed
            if ($validator->failed()) {
                return;
            }

            // Retrieve the product instance from the route parameters
            $product = $this->route('product');

            // If no product is found, skip additional validation
            if (!$product) {
                return;
            }

            // Check if the product's color is not set and add an error message if so
            if (is_null($product->color)) {
                $validator->errors()->add("swal", "The product color has not been set.");
            }

            // Check if the product's latest formula is not set and add an error message if so
            if (is_null($product->latestFormula)) {
                $validator->errors()->add("swal", "Please create a formulation first.");
            }
        });
    }
}
