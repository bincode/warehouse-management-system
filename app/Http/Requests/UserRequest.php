<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'username' => [
                'required',
                'unique:users,username'
            ],
            'password' => [
                'required',
                'confirmed',
                'min:4'
            ]
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the username field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['username'][1] .= ',' . $this->user->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
