<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Set the validation rules for the request
        $rules = [
            'for' => [
                'required',
                'unique:receives,for'
            ],
            'date' => [
                'required',
                'date_format:d/m/Y'
            ],
            'description' => [
                'required'
            ],
        ];

        // If the request is for updating an existing user, add the user ID to the unique validation rule for the name field
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['for'][1] .= ',' . $this->delivery->id;
        }

        // Return the validation rules for the request
        return $rules;
    }
}
