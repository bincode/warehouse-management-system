<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules for the request.
     *
     * @return array
     */
    public function rules()
    {
        // Define the base validation rules
        $rules = [
            'lot' => [
                'nullable',
                'unique:work_orders,lot' // Ensure 'lot' is unique among work orders
            ],
            'date' => [
                'required',
                'date_format:d/m/Y' // Validate 'date' format
            ],
            'color' => [
                'required', // Ensure 'formula_id' is provided
            ],
            'formula_id' => [
                'required', // Ensure 'formula_id' is provided
            ],
            'extruder_id' => [
                'sometimes', // Ensure 'extruder_id' is provided
            ],
            'mixer_id' => [
                'sometimes', // Ensure 'mixer_id' is provided
            ],
            'plan_quantity' => [
                'required', // Ensure 'plan_quantity' is provided
            ],
            'mixing_quantity' => [
                'required', // Ensure 'mixing_quantity' is provided
            ],
            'machineProcType_id' => [
                'sometimes', // Ensure 'machineProcType_id' is provided
            ],
            'endProduct_id' => [
                'required', // Ensure 'endProduct_id' is provided
            ],
            'mixing_note' => [
                'nullable', // Allow 'mixing_note' to be nullable
            ],
            'note' => [
                'nullable', // Allow 'note' to be nullable
            ],
        ];

        // If the request is for updating an existing work order
        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            $rules['lot'][1] .= ',' . $this->workorder->id;
        }

        // Return the validation rules
        return $rules;
    }
}
