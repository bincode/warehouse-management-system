<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->has('select2')) {
            return [
                'id' => $this->id,
                'name' => $this->name,
            ];
        } else {
            return [
                'id'            => $this->id,
                'name'          => $this->name,
                'description'   => $this->description,
                'quantity'      => $this->stock,
                'reserve'       => $this->getReserveWorkQuantity(),
                'unit'          => ucwords(mb_strtolower($this->unit ?? 'KG'))
            ];
        }
    }
}
