<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request->has('select2')) {
            return [
                'id' => $this->id,
                'code' => $this->code,
            ];
        } else {
            return [
                'id'        => $this->id,
                'code'      => $this->code,
                'note'      => $this->note,
            ];
        }
    }
}
