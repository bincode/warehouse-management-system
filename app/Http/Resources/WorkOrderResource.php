<?php

namespace App\Http\Resources;

use App\Services\WorkOrderService;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $service = new WorkOrderService();
        $hasAdjustment = $service->AdjCheck($this->resource);

        $data = parent::toArray($request);

        return $data;
    }
}
