<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->product->description,
            'lot' => $this->product->latestResult->for,
            'amount' => $this->amount,
            'customer' => $this->order->company->name,
            'document' => $this->order->company->document_requirement,
            'ship_date' => $this->ship_date,
        ];
    }
}
