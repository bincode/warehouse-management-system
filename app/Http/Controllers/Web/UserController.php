<?php

namespace App\Http\Controllers\Web;

use \App\Models\User;
use \App\Http\Requests\UserRequest;
use \App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.users.user-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.users.user-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // Create a new user with the validated request data
        $user = User::create($request->validated());

        // Convert roles to integers if necessary
        $roleIds = array_map('intval', $request->roles);

        // Sync roles with the user
        $user->syncRoles($roleIds);

        // Redirect to the index page with success message
        return redirect()->route('users.index')->with('success', 'User created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('pages.users.user-form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        // Update user in the database
        $user->update($request->validated());

        // Convert roles to integers if necessary
        $roleIds = array_map('intval', $request->roles);

        // Sync roles with the user
        $user->syncRoles($roleIds);

        // Redirect to the users index page with success message
        return redirect()->route('users.index')->with('toast_success', 'User updated successfully!');
    }
}
