<?php

namespace App\Http\Controllers\Web;

use App\Models\MaterialRelease;
use App\Http\Controllers\Controller;
use App\Observers\TransactionObserver;
use App\Http\Requests\MaterialReleaseRequest;
use App\Services\WorkOrderService;

class MaterialReleaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.material-release.material-release-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.material-release.material-release-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MaterialReleaseRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MaterialReleaseRequest $request)
    {
        // Attach TransactionObserver to the MaterialRelease model
        MaterialRelease::observe(TransactionObserver::class);

        // Create a new release material in the database using the validated data from the request
        MaterialRelease::create($request->validated());

        // Redirect to the index page with a success message
        if (request('isRedirect', true)) {

            return redirect()->route('releases.index')->with('success', 'Material Release created successfully.');
        }
        return redirect()->back()->with('toast_success', "{$request->id}, Sucessfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialRelease  $release
     * @return \Illuminate\Contracts\View\View
     */
    public function show(MaterialRelease $release)
    {
        $workorder = $release->workorder->load(['endProduct', 'release.products', 'formula.materials', 'formula.materials.workOrders', 'formula.materials.receives', 'formula.materials.results']);

        // Get formatted materials data
        $service = new WorkOrderService();
        $materials = $service->getFormattedMaterials($workorder);

        // Return the view with the work order and formatted materials data
        return view('pages.work-orders.work-order-view', ['workorder' => $workorder, 'materials' => $materials]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MaterialRelease $release
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(MaterialRelease $release)
    {
        return view('pages.material-release.material-release-form', compact('release'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MaterialReleaseRequest $release
     * @param  \App\Models\MaterialRelease  $release
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MaterialReleaseRequest $request, MaterialRelease $release)
    {
        // Attach TransactionObserver to the MaterialRelease model
        MaterialRelease::observe(TransactionObserver::class);

        // Update the release material in the database with the validated data from the request
        $release->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('releases.index')->with('success', 'Release Material updated successfully.');
    }
}
