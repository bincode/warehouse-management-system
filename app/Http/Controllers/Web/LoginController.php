<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Show Login form
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        if (auth()->check()) {
            return redirect()->intended('/');
        }

        return view('pages.login');
    }

    /**
     * Authenticate user's login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function attempt(Request $request)
    {
        // Attempt to authenticate the user with the provided username and password  
        if (!auth()->attempt($request->only('username', 'password'))) {
            return redirect()->back()->withInput($request->only('username', 'password'))->withErrors([
                'password' => 'Wrong password'
            ]);
        }

        // If login is successful, create a token and store it in the session  

        /** @var User|null $user */
        $user = auth()->user();

        // Create a new token for the authenticated user  
        $token = $user->createToken('focs')->plainTextToken;

        // Store the token in the session  
        session(['auth_token' => $token]);

        // Redirect to the intended page after successful login  
        return redirect()->intended('/');
    }

    /**
     * Logout User's
     */
    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}
