<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorkOrderRequest;
use App\Models\WorkOrder;
use App\Observers\WorkOrderObserver;
use App\Services\WorkOrderService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WorkOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.work-orders.work-order-index');
    }

    /**
     * Display the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.work-orders.work-order-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\WorkOrderRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(WorkOrderRequest $request)
    {
        // Attach WorkOrderObserver to the WorkOrder model
        WorkOrder::observe(WorkOrderObserver::class);

        DB::beginTransaction();

        try {
            // Create a new formula in the database using the validated data from the request
            WorkOrder::create($request->validated());

            DB::commit();

            // Redirect to the index page with a success message
            return redirect()->route('workorders.index')->with('success', 'Work Order created successfully.');
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("Error creating work order: {$th->getMessage()}");
            return redirect()->back()->with('error', 'Failed to create Work Order. Please try again.')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkOrder  $workorder
     * @return \Illuminate\Contracts\View\View
     */
    public function show(WorkOrder $workorder)
    {
        $workorder->load(['endProduct', 'release.products', 'formula.materials', 'formula.materials.workOrders', 'formula.materials.receives', 'formula.materials.results']);

        // Get formatted materials data
        $service = new WorkOrderService();
        $materials = $service->getFormattedMaterials($workorder);

        // Return the view with the work order and formatted materials data
        return view('pages.work-orders.work-order-view', ['workorder' => $workorder, 'materials' => $materials]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkOrder  $workorder
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(WorkOrder $workorder)
    {
        return view('pages.work-orders.work-order-form', compact('workorder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\WorkOrderRequest  $request
     * @param  \App\Models\WorkOrder  $workorder
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(WorkOrderRequest $request, WorkOrder $workorder)
    {
        // Attach WorkOrderObserver to the Formula model
        WorkOrder::observe(WorkOrderObserver::class);

        // Create a new formula in the database using the validated data from the request
        $workorder->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('workorders.index')->with('success', 'Work Order updated successfully.');
    }
}
