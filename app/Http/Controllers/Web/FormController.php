<?php

namespace App\Http\Controllers\Web;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\Formula;
use App\Models\MaterialRelease;
use App\Models\WorkOrder;
use App\View\Components\Forms\Tables\MaterialTableInputComponent;
use App\View\Components\TableList\Inputs\WorkOrderTableListComponent;

class FormController extends Controller
{
    public function renderWorkOrderForm(Formula $formula)
    {
        // Create a new instance of WorkOrderTableListComponent
        $component = new WorkOrderTableListComponent($formula, "table-input-material");

        // Load the materials relationship for the formula and calculate the work order quantity for each material
        $materials = $formula->load('materials')->materials->map(function ($material) {
            $need = $material->percentage * request('quantity') / 100;
            $material->amount = $need;
            return $material;
        });

        // Load the work orders relationship for the formula  
        $workOrders = $formula->workOrders->filter(function ($workOrder) {
            return $workOrder->status == Status::FINISHED(); // Assuming status is a column in the work orders table  
        });

        // Check if all work orders have adjustments  
        $totalWorkOrders = $workOrders->count();
        $totalAdjustments = $workOrders->filter(function ($workOrder) {
            return $workOrder->has_adjustment; // Assuming has_adjustment is a boolean column in the work orders  
        })->count();

        // Render the component with the specified name and pass the materials as a parameter
        $content = $component->render()->with(['name' => 'table-input-work-order', 'items' => $materials]);

        // Return the rendered content
        return response()->json([
            'content' => base64_encode($content),
            'product' => $formula->product,
            'totalWorkOrders' => $totalWorkOrders,
            'totalAdjustments' => $totalAdjustments
        ]);
    }

    public function renderMaterialReleaseForm($lotNumber)
    {
        $workOrder = WorkOrder::where("lot", $lotNumber)->first();

        if (!$workOrder) {
            $release = MaterialRelease::where("for", $lotNumber)->first();
            $products = $release->products ?? [];

            $component = new MaterialTableInputComponent('table-input-materials');
            $content = $component->render()->with(['name' => 'table-input-materials', 'items' => $products]);

            return response()->json([
                'content' => base64_encode($content),
                'product' => $release->product ?? null
            ]);
        }

        $planQuantity = $workOrder->plan_quantity;
        $formula = $workOrder->formula;

        // Create a new instance of MaterialTableInputComponent
        $component = new MaterialTableInputComponent("table-input-materials", $formula);

        // Load the materials relationship for the formula and calculate the work order quantity for each product
        $materials = $formula->load('materials')->materials->map(function ($product) use ($planQuantity) {
            $need = $product->percentage * $planQuantity / 100;
            $product->amount = $need;
            return $product;
        });

        // Render the component with the specified name and pass the materials as a parameter
        $content = $component->render()->with(['name' => 'table-input-materials', 'items' => $materials]);

        return response()->json([
            'content' => base64_encode($content),
            'product' => $formula->product
        ]);
    }

    public function renderProductionResultForm($lotNumber)
    {
        $release = MaterialRelease::where("for", $lotNumber)->first();

        if (!$release) {
            return [];
        }

        return [
            'product' => $release->product,
            'consumption' => $release->consumption,
        ];
    }
}
