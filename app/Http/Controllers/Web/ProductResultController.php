<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use \App\Http\Requests\ProductResultRequest;
use App\Models\ProductResult;
use App\Observers\TransactionObserver;

class ProductResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.product-result.product-result-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.product-result.product-result-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductResultRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductResultRequest $request)
    {
        // Attach TransactionObserver to the ProductResult model
        ProductResult::observe(TransactionObserver::class);

        // Create a new release material in the database using the validated data from the request
        ProductResult::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('results.index')->with('success', 'Product Result created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductResult  $result
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(ProductResult $result)
    {
        return view('pages.product-result.product-result-form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductResultRequest  $request
     * @param  \App\Models\ProductResult  $result
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductResultRequest $request, ProductResult $result)
    {
        // Attach TransactionObserver to the ProductResult model
        ProductResult::observe(TransactionObserver::class);

        // Update the result material in the database with the validated data from the request
        $result->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('results.index')->with('success', 'Product Result updated successfully.');
    }
}
