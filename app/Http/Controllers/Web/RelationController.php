<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\RelationRequest;
use App\Models\Relation;

class RelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.relations.relation-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.relations.relation-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RelationRequest $request)
    {
        Relation::create($request->validated());

        return redirect()
            ->intended(route('relations.index'))
            ->with('toast_success', "Created <b>'{$request->name}'</b>, Successfully!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Relation $relation
     * @return \Illuminate\Http\Response
     */
    public function edit(Relation $relation)
    {
        return view('pages.relations.relation-form', compact('relation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Relation $relation
     * @return \Illuminate\Http\Response
     */
    public function update(RelationRequest $request, Relation $relation)
    {
        $relation->update($request->validated());

        return redirect()
            ->intended(route('relations.index'))
            ->with('toast_success', "Updated <b>'{$request->name}'</b>, Successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Relation $relation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Relation $relation)
    {
        try {
            // Check if the rack has any associated delivery order
            if ($relation->delivery()->exists()) {
                return response()->json(['message' => 'Cannot Delete: Active Delivery Order found.'], 400);
            }

            // Check if the rack has any associated receive item
            if ($relation->receive()->exists()) {
                return response()->json(['message' => 'Cannot Delete: Active Receive Item found.'], 400);
            }

            $relation->delete();

            // Return success message if deletion is successful
            return response()->json(['message' => 'Relation deleted successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete relation. An error occurred while processing your request. Please try again later or contact support for assistance.'], 500);
        }
    }
}
