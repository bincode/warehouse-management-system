<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdjustmentRequest;
use App\Models\Adjustment;
use App\Observers\TransactionObserver;

class AdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.adjustments.adjustment-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.adjustments.adjustment-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AdjustmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdjustmentRequest $request)
    {
        // Attach TransactionObserver to the Adjustment model
        Adjustment::observe(TransactionObserver::class);

        // Create a new adjustment in the database using the validated data from the request
        Adjustment::create($request->validated());

        // Redirect to the index page with success message
        return redirect()->route('adjustments.index')->with('success', 'Adjustment created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Adjustment $adjustment
     * @return \Illuminate\Http\Response
     */
    public function edit(Adjustment $adjustment)
    {
        return view('pages.adjustments.adjustment-form', compact('adjustment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AdjustmentRequest  $request
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\Response
     */
    public function update(AdjustmentRequest $request, Adjustment $adjustment)
    {
        // Attach TransactionObserver to the Adjustment model
        Adjustment::observe(TransactionObserver::class);

        // Update adjustment in the database using the validated data from the request
        $adjustment->update($request->validated());

        // Redirect to the index page with success message
        return redirect()->route('adjustments.index')->with('success', 'Adjustment updated successfully.');
    }
}
