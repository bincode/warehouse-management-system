<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\JobCostRequest;
use App\Models\JobCost;
use App\Observers\TransactionObserver;
use Illuminate\Http\Request;

class JobCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.job-cost.job-cost-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.job-cost.job-cost-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\JobCostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobCostRequest $request)
    {
        // Attach TransactionObserver to the JobCost model
        JobCost::observe(TransactionObserver::class);

        // Create a new job cost in the database using the validated data from the request
        JobCost::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('jobcosts.index')->with('success', 'Job Cost created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobCost  $JobCost
     * @return \Illuminate\Http\Response
     */
    public function edit(JobCost $jobcost)
    {
        return view('pages.job-cost.job-cost-form', compact('jobcost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\JobCostRequest  $request
     * @param  \App\Models\JobCost  $JobCost
     * @return \Illuminate\Http\Response
     */
    public function update(JobCostRequest $request, JobCost $jobcost)
    {
        // Attach TransactionObserver to the JobCost model
        JobCost::observe(TransactionObserver::class);

        // Update the receive item in the database with the validated data from the request
        $jobcost->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('jobcosts.index')->with('success', 'Job Cost updated successfully.');
    }
}
