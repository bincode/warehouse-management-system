<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\SalesOrderRequest;
use App\Models\SalesOrder;
use App\Observers\SalesOrderObserver;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.sales-orders.sales-order-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.sales-orders.sales-order-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SalesOrderRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SalesOrderRequest $request)
    {
        // Attach SalesOrderObserver to the SalesOrder model
        SalesOrder::observe(SalesOrderObserver::class);

        // Create a new release material in the database using the validated data from the request
        SalesOrder::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('salesorder.index')->with('success', 'Product Order created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SalesOrder  $order
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(SalesOrder $order)
    {
        return view('pages.sales-orders.sales-order-form', compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SalesOrderRequest  $request
     * @param  \App\Models\SalesOrder  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SalesOrderRequest $request, SalesOrder $order)
    {
        // Attach SalesOrderObserver to the SalesOrder model
        SalesOrder::observe(SalesOrderObserver::class);

        // Create a new release material in the database using the validated data from the request
        $order->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('salesorder.index')->with('success', 'Product Order created successfully.');
    }
}
