<?php

namespace App\Http\Controllers\Web;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductionOrderRequest;
use App\Models\Product;
use App\Models\WorkOrder;

class ProductionOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.production-orders.production-order-index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create(Product $product)
    {
        return view('pages.production-orders.production-order-form', compact('product'));
    }

    public function update(ProductionOrderRequest $request, Product $product)
    {
        // Retrieve the latest formula associated with the product
        $latestFormula = $product->latestFormula;

        // Jika latestFormula null, kembalikan ke halaman sebelumnya dengan pesan kesalahan
        if (is_null($latestFormula)) {
            return redirect()->back()->with('warning', "<small><b>Please Create a Formulation First.</b></small>");
        }

        // Determine the latest work order number for the specified extruder
        $latestIndexMachine = WorkOrder::where('extruder_id', $request->input('extruder_id'))->max('order') ?? 0;

        // Retrieve sales order IDs from the request and fetch related customer IDs
        $salesOrderIds = $request->input('sales_order_id', []);
        $customerIds = $product->orders->whereIn('id', $salesOrderIds)->pluck('company_id')->toArray();

        // Get the nearest date from the related sales orders
        $latestOrderDate = $product->orders
            ->whereIn('id', $salesOrderIds)
            ->sortByDesc('date')
            ->first()
            ->date;

        // Create a new work order with incremented order number
        $workOrder = WorkOrder::create([
            'formula_id' => $latestFormula->id,
            'endProduct_id' => $product->id,
            'plan_quantity' => $request->input('plan_quantity'),
            'extruder_id' => $request->input('extruder_id'),
            'order' => $latestIndexMachine + 1,
            'date' => $latestOrderDate
        ]);

        // Synchronize the customers associated with the work order
        $workOrder->customers()->sync($customerIds);

        // Update orders status to "process"
        $product->orders()->whereIn('sales_orders.id', $salesOrderIds)->update([
            'status' => Status::PLANNING(),
            'work_order_id' => $workOrder->id,
        ]);

        switch ($request->input('action')) {
            case 'work_order':
                return redirect()->route('workorders.edit', ['workorder' => $workOrder->id]);
            case 'production_planning':
                return redirect()->route('plan.production');
            default:
                return redirect()->route('productionorder.index');
        }
    }
}
