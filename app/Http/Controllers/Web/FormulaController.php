<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormulaRequest;
use App\Models\Formula;
use App\Observers\FormulaObserver;

class FormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view("pages.formulas.formula-index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view("pages.formulas.formula-form");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FormulaRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormulaRequest $request)
    {
        // Attach FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        // Create a new formula in the database using the validated data from the request
        Formula::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route("formulas.index")->with("success", "Formula created successfully.");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit(Formula $formula)
    {
        return view("pages.formulas.formula-form", compact("formula"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\FormulaRequest  $request
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FormulaRequest $request, Formula $formula)
    {
        // Attach FormulaObserver to the Formula model
        Formula::observe(FormulaObserver::class);

        // Check if the formula has related work orders
        if ($formula->workorders()->exists()) {
            return redirect()->back()->with("warning", "<span class='badge badge-warning'>Cannot Edit</span>: <small>Formula used in production.</small>");
        }

        // Create a new formula in the database using the validated data from the request
        $formula->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route("formulas.index")->with("success", "Formula updated successfully.");
    }
}
