<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\MachineRequest;
use App\Models\Machine;

class MachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.machines.machine-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.machines.machine-form');
    }

    /**
     * Store a newly created resource in the database.
     *
     * @param  \App\Http\Requests\MachineRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MachineRequest $request)
    {
        // Create a new resource using the validated data from the request
        Machine::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('machines.index')->with('success', 'Resource created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Machine  $machine
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Machine $machine)
    {
        return view('pages.machines.machine-form', compact('machine'));
    }

    /**
     * Update the specified resource in the database.
     *
     * @param  \App\Http\Requests\MachineRequest  $request
     * @param  \App\Models\Machine  $machine
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MachineRequest $request, Machine $machine)
    {
        // Update the specified resource using the validated data from the request
        $machine->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('machines.index')->with('success', 'Resource updated successfully.');
    }
}
