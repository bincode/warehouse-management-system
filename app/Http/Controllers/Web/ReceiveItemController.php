<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReceiveItemRequest;
use App\Models\ReceiveItem;
use App\Observers\TransactionObserver;

class ReceiveItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.receive-item.receive-item-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.receive-item.receive-item-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\ReceiveItemRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReceiveItemRequest $request)
    {
        // Attach TransactionObserver to the ReceiveItem model
        ReceiveItem::observe(TransactionObserver::class);

        // Create a new release material in the database using the validated data from the request
        ReceiveItem::create($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('receives.index')->with('success', 'Receives Items created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReceiveItem  $receife
     * @return \Illuminate\Http\Response
     */
    public function edit(ReceiveItem $receife)
    {
        return view('pages.receive-item.receive-item-form', compact('receife'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ReceiveItemRequest  $request
     * @param  \App\Models\Receive  $receive
     * @return \Illuminate\Http\Response
     */
    public function update(ReceiveItemRequest $request, ReceiveItem $receife)
    {
        // Attach TransactionObserver to the ReceiveItem model
        ReceiveItem::observe(TransactionObserver::class);

        // Update the receive item in the database with the validated data from the request
        $receife->update($request->validated());

        // Redirect to the index page with a success message
        return redirect()->route('receives.index')->with('success', 'Receive Item updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Receive  $receive
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceiveItem $receive)
    {
        $receive->delete();
    }
}
