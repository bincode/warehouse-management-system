<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\MachineCategory;
use Illuminate\Http\Request;

class MachineCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.machine-categories.machine-categories-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.machine-categories.machine-categories-form');
    }

    /**
     * Store a newly created resource in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Create a new resource using the request data
        MachineCategory::create($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('machine-category.index')->with('success', 'Resource created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MachineCategory  $machine_category
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(MachineCategory $machine_category)
    {
        return view('pages.machine-categories.machine-categories-form', compact('machine_category'));
    }

    /**
     * Update the specified resource in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MachineCategory  $machine_category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, MachineCategory $machine_category)
    {
        // Update the specified resource using the request data
        $machine_category->update($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('machine-category.index')->with('success', 'Resource updated successfully.');
    }
}
