<?php

namespace App\Http\Controllers\Web;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Models\DeliverySchedule;
use App\Models\Product;
use Illuminate\Http\Request;

class DeliveryScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.delivery-schedules.delivery-schedule-index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create(Product $product)
    {
        return view('pages.delivery-schedules.delivery-schedule-form', compact('product'));
    }

    public function store(Request $request)
    {
        $schedule = DeliverySchedule::create($request->all());

        $schedule->reference->order->company->update([
            'document_requirement' => $request->document
        ]);
        $schedule->reference->update([
            "status" => Status::PACKING()
        ]);
    }

    public function update(Request $request, DeliverySchedule $deliveryrequest)
    {
        $deliveryrequest->update($request->all());

        $deliveryrequest->reference->order->company->update([
            'document_requirement' => $request->document
        ]);

        $deliveryrequest->reference->update([
            "status" => Status::PACKING()
        ]);
    }
}
