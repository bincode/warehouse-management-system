<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\RackRequest;
use App\Models\Rack;
use App\Models\Warehouse;

class WarehouseRackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function index(Warehouse $warehouse)
    {
        return view('pages.warehouse-racks.warehouse-rack-index', compact('warehouse'));
    }

    /**
     * Display the form for creating a new resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function create(Warehouse $warehouse)
    {
        return view('pages.warehouse-racks.warehouse-rack-form', compact('warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RackRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function store(RackRequest $request, Warehouse $warehouse)
    {
        // Create a new Rack record associated with the given Warehouse.
        $warehouse->racks()->create($request->validated());

        return redirect()
            ->intended(route('warehouses.racks.index', ['warehouse' => $warehouse->id]))
            ->with('toast_success', "Created Rack <b>'{$request->code}'</b>, Successfully!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @param  \App\Models\Rack $rack
     * @return \Illuminate\Http\Response
     */
    public function edit(Warehouse $warehouse, Rack $rack)
    {
        return view('pages.warehouse-racks.warehouse-rack-form', compact('rack', 'warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RackRequest  $request
     * @param  \App\Models\Warehouse  $warehouse
     * @param  \App\Models\Rack $rack
     * @return \Illuminate\Http\Response
     */
    public function update(RackRequest $request, Warehouse $warehouse, Rack $rack)
    {
        // Update rack in the database
        $rack->update($request->validated());

        return redirect()
            ->intended(route('warehouses.racks.index', ['warehouse' => $warehouse->id]))
            ->with('toast_success', "Updated Rack <b>'{$request->code}'</b>, Successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rack $rack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse, Rack $rack)
    {
        try {
            // Check if the rack has any associated transactions
            if ($rack->history()->exists()) {
                return response()->json(['message' => 'Sorry, the rack cannot be deleted because it is associated with transactions.'], 400);
            }

            // Check if there is more than one rack in the master data
            if (Rack::all()->count() <= 1) {
                return response()->json(['message' => 'Sorry, the rack cannot be deleted as there must be at least one rack in the master data.'], 400);
            }

            $rack->delete();
            // Return success message if deletion is successful
            return response()->json(['message' => 'Rack deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Oops! Something went wrong. Failed to delete the rack. Please try again.'], 500);
        }
    }
}
