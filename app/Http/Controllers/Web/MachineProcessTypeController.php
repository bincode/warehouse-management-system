<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\MachineProcType;
use Illuminate\Http\Request;

class MachineProcessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('pages.machine-process-type.machine-process-type-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.machine-process-type.machine-process-type-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Create a new machine process type in the database using all data from the request
        MachineProcType::create($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('machine-process-type.index')->with('success', 'Machine Process Type created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MachineProcType  $machine_process_type
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(MachineProcType $machine_process_type)
    {
        return view('pages.machine-process-type.machine-process-type-form', compact('machine_process_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MachineProcType  $machine_process_type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, MachineProcType $machine_process_type)
    {
        // Update the machine process type in the database using all data from the request
        $machine_process_type->update($request->all());

        // Redirect to the index page with a success message
        return redirect()->route('machine-process-type.index')->with('success', 'Machine Process Type updated successfully.');
    }
}
