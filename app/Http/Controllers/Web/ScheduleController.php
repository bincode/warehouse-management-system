<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\DeliverySchedule;
use App\Models\Machine;
use Carbon\Carbon;

class ScheduleController extends Controller
{
    /**
     * Display production schedules.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function production()
    {
        $currentDate = Carbon::now()->format('l, d F Y');
        // Return the view with machines data
        return view('pages.schedule.production-schedules', compact('currentDate'));
    }

    /**
     * Display production schedules.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function previewproduction()
    {
        // Retrieve machines with their associated work orders and related product information
        $machines = Machine::with([
            'workOrders.mixer',
            'workOrders.salesorders.products',
            'workOrders.result',
            'workOrders.formula.product'
        ])
            ->whereHas('category', function ($query) {
                $query->where(
                    'name',
                    'extruder'
                );
            })->get();

        $machines->each->processSchedules();

        // Return the view with machines data
        return view('pages.schedule.production-schedules-preview', compact('machines'));
    }

    /**
     * Display production schedules.
     *
     */
    public function delivery()
    {
        $yesterday = Carbon::yesterday();
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();

        $deliveries = DeliverySchedule::whereBetween('date', [$yesterday, $tomorrow])
            ->with('reference')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->date)->format('Y-m-d');
            });

        // Loop untuk menambahkan tanggal-tanggal yang tidak memiliki pengiriman
        $date = $yesterday;
        while ($date <= $tomorrow) {
            $formattedDate = $date->format('Y-m-d');
            if (!isset($deliveries[$formattedDate])) {
                // Tambahkan entri kosong jika tidak ada pengiriman pada tanggal ini
                $deliveries[$formattedDate] = [];
            }
            $date->addDay(); // Lanjutkan ke tanggal berikutnya
        }

        // Set locale to Indonesian
        Carbon::setLocale('id');

        // Urutkan tanggal
        $deliveries = $deliveries->sortKeys();

        // Return the view with machines data
        return view('pages.schedule.delivery-schedules', compact('deliveries'));
    }
}
