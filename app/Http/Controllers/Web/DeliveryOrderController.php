<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryOrderRequest;
use App\Models\DeliveryOrder;
use App\Observers\TransactionObserver;

class DeliveryOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.delivery-order.delivery-order-index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.delivery-order.delivery-order-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DeliveryOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryOrderRequest $request)
    {
        // Attach TransactionObserver to the DeliveryOrder model
        DeliveryOrder::observe(TransactionObserver::class);

        // Create a new delivery order in the database using the validated data from the request
        DeliveryOrder::create($request->validated());

        // Redirect to the index page with success message
        return redirect()->route('deliveries.index')->with('success', 'Delivery Order created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DeliveryOrder $delivery
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryOrder $delivery)
    {
        return view('pages.delivery-order.delivery-order-form', compact('delivery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DeliveryOrderRequest  $request
     * @param  \App\Models\DeliveryOrder  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(DeliveryOrderRequest $request, DeliveryOrder $delivery)
    {
        // Attach TransactionObserver to the DeliveryOrder model
        DeliveryOrder::observe(TransactionObserver::class);

        // Update the delivery order in the database using the validated data from the request
        $delivery->update($request->validated());

        // Redirect to the index page with success message
        return redirect()->route('deliveries.index')->with('success', 'Delivery Order updated successfully.');
    }
}
