<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductionOrderResource;
use App\Models\Product;
use Carbon\Carbon;

class ApiProductionOrderController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Product::query();

        $query->withStatus(Status::CREATED());

        $query->with(['orders' => function ($query) {
            $query->where('status', Status::CREATED());
        }, 'orders.company']);

        // Retrieve the results of the query
        $products = $query->get();

        // Modify the ship_date based on ship_priority
        $products->map(function ($product) {
            $product->orders->map(function ($order) {

                switch ($order->pivot->ship_priority) {
                    case 'ASAP':
                        $order->pivot->ship_date = Carbon::now()->addWeek()->format('Y-m-d');
                        break;
                    case 'URGENT':
                        $order->pivot->ship_date = Carbon::tomorrow()->format('Y-m-d');
                        break;
                }
                return $order;
            });
            return $product;
        });

        // Sort the products by the modified ship_date
        $resource = $products->sortBy(function ($product) {
            return $product->orders->min('pivot.ship_date');
        });

        // Transform the query results into a collection and return
        return ProductionOrderResource::collection($resource);
    }
}
