<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdjustmentResource;
use App\Models\Adjustment;
use Carbon\Carbon;

/**
 * API Controller for managing adjustments.
 */
class ApiAdjustmentController extends Controller
{
    /**
     * Display a listing of the adjustments.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Initialize a query for the Adjustment model
        $query = Adjustment::query();

        // Filter adjustments by search text if provided
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Filter adjustments by start date if provided
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Filter adjustments by end date if provided
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        // Load related product data to reduce query overhead
        $query->with(['products']);

        // Sort adjustments by date in descending order
        $query->orderBy('date', 'desc');

        // Execute the query and retrieve the results
        $adjustments = $query->get();

        // Transform the results into a resource collection
        return AdjustmentResource::collection($adjustments);
    }

    /**
     * Remove the specified adjustment from storage.
     *
     * @param  \App\Models\Adjustment  $adjustment
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Adjustment $adjustment)
    {
        try {
            // Detach all associated products from the adjustment
            $adjustment->products()->sync([]);

            // Attempt to delete the adjustment record
            $adjustment->delete();

            // Return a success response
            return response()->json(['message' => 'Adjustment deleted successfully'], 200);
        } catch (\Exception $e) {
            // Handle and return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete adjustment'], 500);
        }
    }
}
