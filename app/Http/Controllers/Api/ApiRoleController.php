<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use Illuminate\Support\Facades\Cache;
use Spatie\Permission\Models\Role;

class ApiRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Role::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Retrieve the results of the query
        $resource = $query->get();

        // Return the resource as a collection of RoleResource instances
        return RoleResource::collection($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Spatie\Permission\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try {
            // Attempt to delete the role
            $role->delete();

            // Clear the roles cache
            Cache::forget('roles');

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete role'], 500);
        }
    }
}
