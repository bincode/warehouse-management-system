<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\MaterialReleaseResource;
use App\Models\MaterialRelease;
use App\Observers\TransactionObserver;
use Carbon\Carbon;

class ApiMaterialReleaseController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Create a base query
        $query = MaterialRelease::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Apply start date filter if present in the request
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Apply end date filter if present in the request
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        // Load Relations
        $query->with(['products', 'product', 'result', 'workorder']);

        // Order the results by the "date" column in descending order.
        $query->orderBy('date', 'desc');

        // Set Limit return
        $query->limit(1000);

        // Retrieve the result
        $result = $query->get();

        // Return a collection of MaterialReleaseResource objects
        return MaterialReleaseResource::collection($result);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MaterialRelease  $release
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaterialRelease $release)
    {
        // Attach TransactionObserver to the model
        MaterialRelease::observe(TransactionObserver::class);

        try {
            if (!$release->result) {

                // Retrieve sales orders associated with the work order
                $workorder = $release->workorder;
                // Check if the work order exists
                if ($workorder) {
                    // Retrieve sales orders associated with the work order
                    $salesorders = $workorder->salesorders;

                    if ($salesorders) {
                        foreach ($salesorders as $salesOrder) {
                            // Update products' statuses associated with this work order in each sales order
                            foreach ($salesOrder->products()->where('work_order_id', $workorder->id)->get() as $product) {
                                $product->pivot->update(['status' => Status::PREPARE()]);
                            }
                        }
                    }
                }

                // Attempt to delete the release
                $release->delete();

                // Return a success response
                return response()->json(['message' => 'Material Release deleted successfully'], 200);
            } else {
                // Return an error response if deletion fails
                return response()->json(['message' => 'Failed to delete Material Release'], 500);
            }
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Material Release'], 500);
        }
    }
}
