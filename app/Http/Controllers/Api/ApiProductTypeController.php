<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCategoryResource;
use App\Models\ProductType;

class ApiProductTypeController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = ProductType::query();

        // Retrieve the result
        $result = $query->get();

        return ProductCategoryResource::collection($result);
    }
}
