<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PermissionResource;
use Spatie\Permission\Models\Permission;

class ApiPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Permission::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Retrieve the results of the query
        $resource = $query->get();

        // Return the resource as a collection of PermissionResource instances
        return PermissionResource::collection($resource);
    }
}
