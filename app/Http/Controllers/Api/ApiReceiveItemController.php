<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReceiveItemResource;
use App\Models\ReceiveItem;
use App\Observers\TransactionObserver;
use Carbon\Carbon;

class ApiReceiveItemController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = ReceiveItem::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Apply start date filter if present in the request
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Apply end date filter if present in the request
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        // Load relationships
        $query->with(['products', 'company']);

        // Order the results by the "date" column in descending order
        $query->orderBy('date', 'desc');

        // Retrieve the result
        $result = $query->get();

        // Return a collection of RackResource objects
        return ReceiveItemResource::collection($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReceiveItem  $receife
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReceiveItem $receife)
    {
        // Attach TransactionObserver to the model
        ReceiveItem::observe(TransactionObserver::class);

        try {
            // Attempt to delete the receife
            $receife->delete();

            // Return a success response
            return response()->json(['message' => 'Receive Item deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Receive Item'], 500);
        }
    }
}
