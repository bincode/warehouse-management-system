<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MachineResource;
use App\Models\Machine;

class ApiMachineController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Machine::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Apply type filter if present
        $query->when(request()->has('type'), function ($q) {
            $q->whereHas('category', function ($q) {
                // Apply filter to search by product name
                $q->where('name', 'like', '%' . request('type') . '%');
            });
        });

        // Order the machines by machine name with numeric sorting
        $query->orderByRaw("CAST(SUBSTRING_INDEX(name, ' ', -1) AS UNSIGNED)");

        // Retrieve the results of the query
        $resource = $query->get();

        // Transform the query results into a collection
        return MachineResource::collection($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Machine $machine)
    {
        try {
            // Attempt to delete the machine
            $machine->delete();

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete machine'], 500);
        }
    }
}
