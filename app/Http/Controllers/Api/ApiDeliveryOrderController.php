<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryOrderResource;
use App\Models\DeliveryOrder;
use Carbon\Carbon;

/**
 * API Controller for managing delivery orders.
 */
class ApiDeliveryOrderController extends Controller
{
    /**
     * Display a listing of the delivery orders.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Initialize a query for the DeliveryOrder model
        $query = DeliveryOrder::query();

        // Apply search filter based on the "text" request parameter
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Filter results by start date if provided
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Filter results by end date if provided
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        // Load related models to reduce query overhead
        $query->with(['products', 'company']);

        // Sort results by the "date" column in descending order
        $query->orderBy('date', 'desc');

        // Execute the query and retrieve the results
        $deliveryOrders = $query->get();

        // Transform the results into a resource collection
        return DeliveryOrderResource::collection($deliveryOrders);
    }

    /**
     * Remove the specified delivery order from storage.
     *
     * @param  \App\Models\DeliveryOrder  $delivery
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeliveryOrder $delivery)
    {
        try {
            // Detach all associated products from the delivery order
            $delivery->products()->sync([]);

            // Attempt to delete the delivery order record
            $delivery->delete();

            // Return a success response
            return response()->json(['message' => 'Delivery Order deleted successfully'], 200);
        } catch (\Exception $e) {
            // Handle and return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Delivery Order'], 500);
        }
    }
}
