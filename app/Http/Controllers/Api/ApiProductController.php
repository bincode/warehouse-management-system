<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductStockValidator;

class ApiProductController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        $query = Product::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%')
                ->orWhere('description', 'like', '%' . request('text') . '%');
        });

        // Apply category filter if present
        $query->when(request()->has('category'), function ($q) {
            switch (request('category')) {
                case 'goods':
                    // If the category is 'goods', add a condition for 'is_goods'
                    $q->whereHas('category', fn($query) => $query->where('is_goods', 1));
                    break;
                case 'material':
                    // If the category is 'materials', add a condition for 'is_materials'
                    $q->whereHas('category', fn($query) => $query->where('is_material', 1));
                    break;
            }
        });

        // Apply history filter if present
        $query->when(request('history', 'true') == 'true', function ($q) {
            $q->with(['history', 'formulas.workOrders.salesorders']);
        });

        // Order the products by product name in alphabetical order
        $query->orderBy('name', 'asc');

        // Get the number of items per page from the request, default to 10 if not provided
        $perPage = 750;

        // Get the current page number from the request, default to 1 if not provided
        $currentPage = request()->get('start', 0) / $perPage + 1;

        // Get the total number of items for pagination
        $totalItems = $query->count();

        // Get the paginated results
        $products = $query->skip(($currentPage - 1) * $perPage)->take($perPage)->get();

        // Return the paginated results transformed into a collection
        return [
            'data' => ProductResource::collection($products),
            'recordsTotal' => $totalItems,
            'recordsFiltered' => $totalItems, // Adjust if filtering affects the count
        ];
    }

    /**
     * Validate stock of products.
     *
     * @return \Illuminate\Http\Response
     */
    public function validateStockProducts()
    {
        // Retrieve products from request
        $products = request('products');
        $workorder = request('workorder', []);

        // Check if products array is empty
        if (empty($products)) {
            return response()->json(['message' => "Bad Request: The request cannot be processed due to invalid formatting."], 400);
        }

        $validated = new ProductStockValidator($products, $workorder);
        $validated = $validated->validate();

        // Return validation results
        return response()->json($validated, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {

            // Check if the product has any history entries that are not InitializeStock
            $histories = $product->history->reject(function ($history) {
                return $history->histories_type  == "Initialisation";
            });

            if ($histories->isEmpty()) {
                // Attempt to delete the product
                $product->delete();

                // Return a success response
                return response()->json(['message' => 'Product deleted successfully'], 200);
            } else {
                // Return a failed response
                return response()->json(['message' => 'Product has transaction please delete all transaction first'], 500);
            }
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete product'], 500);
        }
    }
}
