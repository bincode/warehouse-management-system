<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MachineProcTypeResource;
use App\Models\MachineProcType;

class ApiMachineProcessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = MachineProcType::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Retrieve the results of the query
        $resource = $query->get();

        // Return the resource as a collection of MachineProcTypeResource instances
        return MachineProcTypeResource::collection($resource);
    }

    public function destroy(MachineProcType $machines_process_type)
    {
        try {

            $machines_process_type->delete();

            // Return a success response
            return response()->json(['message' => 'Machine Process Type deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Machine Process Type'], 500);
        }
    }
}
