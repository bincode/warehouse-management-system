<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Exports\SalesOrderExport;
use App\Http\Controllers\Controller;
use App\Http\Resources\SalesOrderResource;
use App\Imports\SalesOrderImport;
use App\Models\SalesOrder;
use App\Observers\SalesOrderObserver;
use Maatwebsite\Excel\Facades\Excel;

class ApiSalesOrderController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = SalesOrder::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('po_number', 'like', '%' . request('text') . '%')
                ->orWhereHas('company', function ($q) {
                    $q->where('name', 'like', '%' . request('text') . '%');
                })
                ->orWhereHas('products', function ($q) {
                    $q->where('name', 'like', '%' . request('text') . '%');
                });
        });

        // Apply product filter if present
        $query->when(request()->has('product'), function ($q) {
            $q->whereHas('products', function ($q) {
                $q->where('products.id', request('product'));
            });
        });

        $query->when(request()->has('status'), function ($q) {
            $q->whereHas('products', function ($q) {
                $q->where('status', request('status'));
            });
        });

        // Eager load the related model
        $query->with(['company', 'products' => function ($q) {
            // Apply product filter within the eager load if present
            if (request()->has('product')) {
                $q->where('products.id', request('product'));
            }
        }]);

        // Order by date from older
        $query->orderBy('date', 'desc');

        // Retrieve the results of the query
        $resource = $query->get();

        // Transform the query results into a collection
        return SalesOrderResource::collection($resource);
    }

    public function updateStatus(SalesOrder $salesorder)
    {
        // Retrieve product ID and new status from the request
        $productId = request('product');
        $status = request('status');

        // Find the product within the sales order
        $product = $salesorder->products()->where('products.id', $productId)->first();
        if (!$product) {
            return response()->json(['message' => 'Product not found in Sales Order'], 404);
        }

        // Check if the product status allows status change
        if (!$product->canUpdateSalesOrder()) {
            return response()->json([
                'message' => "Cannot change status: Product is in Process, Packed, or Delivered",
            ], 400);
        }

        // Determine the new status
        if ($status == 'continue') {
            $status = $salesorder->setProductStatus($product, $product->pivot->amount);
        }

        // Update the product status in the pivot table
        $product->pivot->update(['status' => $status]);

        // Return a successful response
        return response()->json([
            'message' => 'Status updated successfully',
            'newStatus' => $status,
        ], 200);
    }

    public function importFromExcel(){
        (new SalesOrderImport())->import(request()->file('file'));
    }

    public function exportToExcel()
    {
        return Excel::download(new SalesOrderExport(), 'Production Order.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SalesOrder  $salesorder
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesOrder $salesorder)
    {
        // Attach TransactionObserver to the model
        SalesOrder::observe(SalesOrderObserver::class);

        // Check if the SalesOrder has related WorkOrders
        if ($salesorder->workorders()->exists()) {
            return response()->json(['message' => 'Cannot delete Sales Order: Associated Work Orders exist.'], 400);
        }

        try {
            // Attempt to delete the SalesOrder
            $salesorder->delete();

            // Return a success response
            return response()->json(['message' => 'Sales Order deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Sales Order'], 500);
        }
    }
}
