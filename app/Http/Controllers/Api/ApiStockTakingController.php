<?php

namespace App\Http\Controllers\Api;

use App\Exports\DataStocktakingExport;
use App\Http\Controllers\Controller;
use App\Imports\DataStocktakingImport;
use App\Models\Adjustment;
use Carbon\Carbon;

class ApiStockTakingController extends Controller
{
    /**
     * Export stocktaking data to an Excel file.
     * 
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportToExcel()
    {
        // Determine the date to use for the export, defaulting to the current date if none is provided
        $date = request()->has('date') ? Carbon::createFromFormat('d/m/Y', request('date')) : now();

        // Export data to Excel and prompt for download
        return (new DataStocktakingExport($date))->download('Stocktaking-' . $date->format('Ymd') . '.xlsx');
    }

     /**
     * Import stocktaking data from an uploaded Excel file.
     * Creates an Adjustment record if one does not already exist for the specified or default date.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ImportFromExcel()
    {
        // Determine the date for the import, defaulting to the current date if none is provided
        $date = request()->has('date') ? Carbon::createFromFormat('d/m/Y', request('date')) : now();

        // Find or create an Adjustment record for the specified date
        $adjustment = Adjustment::firstOrCreate(['date' => request('date', $date)], [
            'for' => 'ST' . $date->format('Ym'),
            'description' => 'Stocktaking ' . $date->format('d-m-Y'), 
            'status' => 'Uploading'
        ]);

        // Import data from the uploaded Excel file
        (new DataStocktakingImport($date, $adjustment))->import(request()->file('file'));

        // Redirect to the adjustments index page after import
        return redirect()->intended(route('adjustments.index'));
    }
}
