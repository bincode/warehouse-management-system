<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\JobCostResource;
use App\Models\JobCost;
use App\Observers\TransactionObserver;
use Carbon\Carbon;

class ApiJobCostController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = JobCost::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Apply start date filter if present
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Apply end date filter if present
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        // Order the results by the "date" column in descending order.
        $query->orderBy('date', 'desc');

        // Set Limit return
        $query->limit(1000);

        //Check if return products relationship
        $query->with('products');

        // Retrieve the result
        $result = $query->get();

        // Return a collection of RackResource objects
        return JobCostResource::collection($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobCost  $jobcost
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobCost $jobcost)
    {
        // Attach TransactionObserver to the model
        JobCost::observe(TransactionObserver::class);

        try {
            // Attempt to delete the jobcost
            $jobcost->delete();

            // Return a success response
            return response()->json(['message' => 'Receive Item deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Receive Item'], 500);
        }
    }
}
