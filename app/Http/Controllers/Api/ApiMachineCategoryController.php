<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MachineCategoryResource;
use App\Models\MachineCategory;

class ApiMachineCategoryController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = MachineCategory::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Order the warehouses by machine code in alphabetical order
        $query->orderBy('name', 'asc');

        // Retrieve the results of the query
        $resource = $query->get();

        // Transform the query results into a collection
        return MachineCategoryResource::collection($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MachineCategory  $machines_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(MachineCategory $machines_category)
    {
        try {
            // Attempt to delete the machine
            $machines_category->delete();

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete machine'], 500);
        }
    }
}
