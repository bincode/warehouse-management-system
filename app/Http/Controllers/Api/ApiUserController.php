<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;

class ApiUserController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = User::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Order the users by user code in alphabetical order
        $query->orderBy('username', 'asc');

        // Load the relationships with 'roles'
        $query->with('roles');

        // Retrieve the results of the query
        $resource = $query->get();

        // Transform the query results into a collection
        return UserResource::collection($resource);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            // Attempt to delete the user
            $user->delete();

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete user'], 500);
        }
    }
}
