<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\WorkOrderResource;
use App\Models\Machine;
use App\Models\WorkOrder;
use App\Services\ProductStockValidator;
use Carbon\Carbon;

class ApiWorkOrderController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = WorkOrder::query();

        // Apply search filter if present
        $query->when(request()->has('id'), function ($q) {
            $q->where('id', request('id'));
        });

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('lot', 'like', '%' . request('text') . '%')
                ->orWhereHas('formula.product', function ($subQuery) {
                    $subQuery->where('name', 'like', '%' . request('text') . '%');
                });
        });

        // Apply start date filter if present in the request
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Apply end date filter if present in the request
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        $query->orderBy('lot');

        // Set Limit return
        $query->limit(1000);

        // Eager load relationships
        $query->with(['formula.product', 'formula.materials', 'release.products']);

        // Retrieve the results of the query
        $resource = $query->get();

        return WorkOrderResource::collection($resource);
    }

    /**
     * Save production planning by updating machine assignments and task orders.
     */
    public function saveProductionPlanning()
    {
        // Retrieve the tasks data from the request input
        $tasksData = request()->input("workorder");

        // Iterate through each machine's tasks data
        foreach ($tasksData as $machineId => $taskData) {
            // Find the machine by its ID
            $machine = Machine::find($machineId);

            // If machine exists, proceed to update work orders
            if ($machine) {
                $taskDataOriginal = $taskData;

                // Sort the tasks by the 'order' field
                usort($taskData, function ($a, $b) {
                    return $a['order'] <=> $b['order'];
                });

                // Assign new order values based on the sorted position
                foreach ($taskData as $index => $task) {
                    // Keep track of the original order values in a separate array
                    $taskDataOriginal[$index]['order'] = $task['order'];
                }

                // Update the work orders with the sorted order values
                foreach ($taskDataOriginal as $index => $task) {
                    // Find the work order by its ID
                    $wo = WorkOrder::find($task['taskId']);

                    // If work order exists, update machine ID and order
                    if ($wo) {
                        $wo->update([
                            'extruder_id' => $machineId,
                            'order' => $task['order'] // Use the original order value
                        ]);
                    }
                }
            }
        }

        // Return a JSON response indicating successful operation
        return response()->json(['message' => 'Production planning saved successfully'], 200);
    }

    public function updateStatus(WorkOrder $workorder)
    {
        // Retrieve product ID and new status from the request
        $status = request('status');

        // Check if the status is 'running'
        if ($status === 'running') {
            // Check if there is another work order with the same extruder and status 'running'
            $hasRunningWorkOrder = WorkOrder::where('extruder_id', $workorder->extruder_id)
                ->where('status', 'running')
                ->where('id', '!=', $workorder->id) // Exclude the current work order
                ->exists();

            if ($hasRunningWorkOrder) {
                return response()->json([
                    'error' => true,
                    'message' => 'There is already a work order with status "running" on the same extruder.'
                ], 400);
            }
        }

        if ($status == 'continue' || $status == 'default') {
            $status = $workorder->setProductStatus();
        }

        // Pluck IDs and convert to array
        $products = $workorder->formula->products->pluck('id')->toArray();

        $validated = new ProductStockValidator($products);
        $validated = $validated->validate();

        $workorder->update(['status' => $status]);
        $workorder->salesorders()->where('product_id', $workorder->endProduct_id)->update([
            'product_sales_order.status' => $status
        ]);

        // Return validation results
        return response()->json($validated, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkOrder  $workorder
     */
    public function destroy(WorkOrder $workorder)
    {
        try {
            // Check if the work order has no releases associated with it
            if (!$workorder->release()->exists()) {
                // Retrieve sales orders associated with the work order
                $salesorders = $workorder->salesorders;

                if ($salesorders) {
                    foreach ($salesorders as $salesOrder) {
                        // Update products' statuses associated with this work order in each sales order
                        foreach ($salesOrder->products()->where('work_order_id', $workorder->id)->get() as $product) {
                            $product->pivot->update(['status' => Status::CREATED()]);
                        }
                    }
                }

                // Attempt to delete the work order
                $workorder->delete();

                // Return a success response
                return response()->json(['message' => 'Work order deleted successfully'], 200);
            }

            // Return a failure response if there are releases associated with the work order
            return response()->json(['message' => 'Failed to delete work order'], 500);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Server Error'], 500);
        }
    }
}
