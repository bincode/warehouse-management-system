<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormulaResource;
use App\Models\Formula;
use App\Observers\FormulaObserver;
use App\Validators\FormulaValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiFormulaController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the Adjustment model
        $query = Formula::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->whereHas('product', function ($subQuery) {
                $subQuery->where('name', 'like', "%" . request('text') . "%");
            });
        });

        $query->when(request()->has('isActive'), function ($q) {
            $q->where('is_active', request('isActive', false));
        });

        $query->with('product');

        $query->withCount('workorders');

        // Retrieve the result
        $resource = $query->get();

        // Sort the results by the name attribute of each formula
        $resource = $resource->sortBy(function ($formula) {
            return $formula->product->name;
        });

        // Return a collection of RackResource objects
        return FormulaResource::collection($resource);
    }

    /**
     * Toggle the active status of a formula.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function toggleActive(Formula $formula)
    {
        // Toggle the current value of 'is_active'
        $active = !$formula->is_active;

        // Update the 'is_active' status in the database
        $formula->update(['is_active' => $active]);

        // Determine the text message based on the new status
        $text = $active ? 'Activated' : 'Not Activated';

        // Return a JSON response with a success message
        return response()->json(["message" => "Formula {$formula->product->name} is <b>{$text}</b>"], 200);
    }

    /**
     * Validate the formula.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFormulaChanges(Request $request, Formula $formula): JsonResponse
    {
        // Create a new instance of the FormulaValidator
        $validator = new FormulaValidator();

        // Validate the request data against the given formula
        $result = $validator->validate($request, $formula);

        // Check if validation succeeded
        if ($result) {
            // If validation succeeds, return a success response
            return response()->json(['success' => true]);
        } else {
            // If validation fails, return a failure response with an error message
            return response()->json(['success' => false, 'message' => 'Changes in addition or subtraction of materials']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formula  $formula
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Formula $formula): JsonResponse
    {
        // Attach FormulaObserver to the model
        Formula::observe(FormulaObserver::class);

        try {
            // Check if there are any workOrders related to this formula
            if ($formula->workOrders()->exists()) {
                return response()->json(['message' => 'Cannot delete formula because it has related work orders'], 400);
            }

            // Attempt to delete the formula
            $formula->delete();

            // Return a success response
            return response()->json(['message' => 'Formula deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Formula'], 500);
        }
    }
}
