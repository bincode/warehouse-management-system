<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Machine;
use App\View\Components\Kanban\ScheduleProduction;

class ApiScheduleController extends Controller
{
    public function loadProductionShedule()
    {
        // Retrieve machines with their associated work orders and related product information
        $machines = Machine::with([
            'workOrders.mixer',
            'workOrders.salesorders.products',
            'workOrders.salesorders.products',
            'workOrders.result',
            'workOrders.formula.product'
        ])
            ->whereHas('category', function ($query) {
                $query->where(
                    'name',
                    'extruder'
                );
            })->get();

        $machines->each->processSchedules();
        $component = new ScheduleProduction($machines);

        // Render the component with the specified name and pass the products as a parameter
        $content = $component->render()->with('machines', $machines);

        // Return the rendered content
        return $content;
    }

    public function loadProductionOngoing()
    {
        // Retrieve machines with their associated work orders and related product information
        $machines = Machine::with([
            'workOrders' => function ($query) {
                $query->orderBy('order')
                    ->with('formula')
                    ->where('status', '!=', 'finished');;
            }
        ])->whereHas('category', function ($query) {
            $query->where(
                'name',
                'extruder'
            );
        })->get();

        return view('pages.schedule.production-ongoing', compact('machines'));
    }
}
