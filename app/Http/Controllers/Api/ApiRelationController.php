<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RelationResource;
use App\Models\Relation;

class ApiRelationController extends Controller
{
    public function index()
    {
        // Create a query for the Relation model
        $query = Relation::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('name', 'like', '%' . request('text') . '%');
        });

        // Apply type filter if present
        $query->when(request()->has('type'), function ($q) {
            $q->where('type', 'like', '%' . request('type') . '%');
        });

        // Order the results by the "name" column in ascending order.
        $query->orderBy('name');

        // Get the results
        $relations = $query->get();

        // Return the collection of relations as a resource
        return RelationResource::collection($relations);
    }
}
