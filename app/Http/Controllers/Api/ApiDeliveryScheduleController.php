<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use App\Http\Controllers\Controller;
use App\Http\Resources\DeliveryScheduleResource;
use App\Models\DeliveryOrder;
use App\Models\DeliverySchedule;
use App\Models\ProductSalesOrder;
use Illuminate\Http\Request;

class ApiDeliveryScheduleController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = ProductSalesOrder::query();

        $excludedStatuses = [
            Status::PACKING(),
            Status::COMPLETED(),
            Status::HOLD(),
            Status::CANCELED()
        ];

        $query->whereNotIn('status', $excludedStatuses);

        $query->with("product", "order.company");

        $query->orderBy('ship_date');

        // Retrieve the results of the query
        $resource = $query->get();

        return DeliveryScheduleResource::collection($resource);
    }

    public function store(Request $request)
    {
        $ds = DeliverySchedule::find($request->id);
        $ds->reference->update([
            "status" => Status::DELIVERED()
        ]);

        $do = DeliveryOrder::orderByDesc('id')->first();
        $inputNumberDocument = $request->for ?? $this->incrementString($do->for);
        $request->merge([
            'description' => $do->description,
            'for' => $inputNumberDocument,
        ]);

        $do = DeliveryOrder::firstOrCreate([
            "for" => $inputNumberDocument,
        ], $request->all());

        $ds->update([
            "delivery_order_id" => $do->id,
        ]);

        $do->products()->attach($request->reference['product_id'], [
            'quantity' => $request->quantity,
            'rack_id' => 1,
            'type' => $do->type,
        ]);
    }

    function incrementString($string)
    {
        // Memeriksa apakah string berakhir dengan angka
        if (preg_match('/(.*?)(\d+)$/', $string, $matches)) {
            $prefix = $matches[1];
            $number = $matches[2];
            // Menambahkan 1 ke angka
            $newNumber = intval($number) + 1;
            // Mengembalikan string dengan angka yang ditambah
            return $prefix . $newNumber;
        }
        // Jika tidak ada angka, kembalikan string asli
        return $string;
    }

    public function destroy(DeliverySchedule $deliveryschedule)
    {
        try {
            $deliveryschedule->reference->update([
                "status" => Status::FINISHED()
            ]);

            // Attempt to delete the delivery
            $deliveryschedule->delete();

            // Return a success response
            return response()->json(['message' => 'Receive Item deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Receive Item'], 500);
        }
    }
}
