<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RackResource;
use App\Models\Rack;

class ApiRackController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Rack::query();

        // Apply warehouse id filter if present
        $query->when(request()->has('warehouse'), function ($q) {
            $q->where('warehouse_id', request('warehouse'));
        });

        // Apply product filter if provided in the request
        $query->when(request()->has('product'), function ($q) {
            $q->filterByProduct(request('product'));
        });

        // Load the relationships with 'warehouse' and 'transactionHistoryWithTotalStock'
        $query->with(['warehouse']);

        // Order the results by the "code" column in descending order.
        $query->orderBy('code', 'asc');

        // Retrieve the racks
        $racks = $query->get();

        // Set Parameter to Hide Quantity
        request()->merge([
            'show' => false
        ]);

        // Return a collection of RackResource objects created from the filtered racks
        return RackResource::collection($racks);
    }

    /**
     * Get Rack by ID
     */
    public function getBy(Rack $rack)
    {
        return $rack->load('warehouse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $rack
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rack $rack)
    {
        try {
            // Attempt to delete the rack
            $rack->delete();

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete rack'], 500);
        }
    }
}
