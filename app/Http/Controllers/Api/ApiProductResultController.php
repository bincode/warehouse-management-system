<?php

namespace App\Http\Controllers\Api;

use App\Enums\Status;
use Carbon\Carbon;
use App\Models\ProductResult;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResultResource;
use App\Observers\TransactionObserver;

class ApiProductResultController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Create a new query builder instance for the "ProductResult" model.
        $query = ProductResult::query();

        // Apply search filter if present
        $query->when(request()->has('text'), function ($q) {
            $q->where('for', 'like', '%' . request('text') . '%');
        });

        // Apply start date filter if present in the request
        $query->when(request()->has('start_date') && request('start_date'), function ($q) {
            $q->where('date', '>=', Carbon::createFromFormat('d/m/Y', request('start_date'))->format('Y-m-d'));
        });

        // Apply end date filter if present in the request
        $query->when(request()->has('end_date') && request('end_date'), function ($q) {
            $q->where('date', '<=', Carbon::createFromFormat('d/m/Y', request('end_date'))->format('Y-m-d'));
        });

        //Check if return products relationship
        $query->with(['products', 'release']);

        // Order the results by the "date" column in descending order.
        $query->orderBy('date', 'desc');

        // Set Limit return
        $query->limit(1000);

        // Retrieve the result
        $result = $query->get();

        // Return a collection of RackResource objects
        return ProductResultResource::collection($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductResult  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductResult $result)
    {
        // Attach TransactionObserver to the model
        ProductResult::observe(TransactionObserver::class);

        try {
            // Retrieve sales orders associated with the work order
            $workorder = $result->workorder;

            if ($workorder) {
                $salesorders = $workorder->salesorders;

                if ($salesorders) {
                    foreach ($salesorders as $salesOrder) {
                        // Update products' statuses associated with this work order in each sales order
                        foreach ($salesOrder->products()->where('work_order_id', $workorder->id)->get() as $product) {
                            $product->pivot->update(['status' => Status::PROCESS()]);
                        }
                    }
                }
            }
            // Attempt to delete the result
            $result->delete();

            // Return a success response
            return response()->json(['message' => 'Product Result deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete Product Result'], 500);
        }
    }
}
