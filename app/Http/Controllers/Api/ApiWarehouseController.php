<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\WarehouseResource;
use App\Models\Warehouse;

class ApiWarehouseController extends Controller
{
    /**
     * Display a listing of the resource
     */
    public function index()
    {
        // Get a query builder instance for the model
        $query = Warehouse::query();

        // Apply warehouse id filter if present
        $query->when(request()->has('warehouse'), function ($q) {
            $q->where('warehouse_id', request('warehouse_id')); //
        });

        // Order the warehouses by warehouse code in alphabetical order
        $query->orderBy('name', 'asc');

        // Retrieve the results of the query
        $resource = $query->get();

        // Transform the query results into a collection
        return WarehouseResource::collection($resource);
    }

    /**
     * Display a listing of the resource
     */
    public function getWarehouse(Warehouse $warehouse)
    {
        return $warehouse;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Warehouse  $warehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warehouse $warehouse)
    {
        try {
            // Attempt to delete the warehouse
            $warehouse->delete();

            // Return a success response
            return response()->json(['message' => 'Role deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if deletion fails
            return response()->json(['message' => 'Failed to delete warehouse'], 500);
        }
    }
}
