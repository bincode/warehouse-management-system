<?php

namespace App\Models;

use App\Enums\Status;
use App\Traits\HasOrder;
use App\Traits\HistoryTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, HasOrder, HistoryTrait;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     *  Scope to eager load product history
     */
    public function scopeWithHistory($query)
    {
        $query->with(['history.histories']);
    }

    public function initializeStock()
    {
        return $this->morphedByMany(InitializeStock::class, 'histories');
    }

    /**
     * Get the date attribute for the product.
     * If the product has a history, return the date from the first history record.
     *
     * @return mixed|null
     */
    public function getDateAttribute()
    {
        return $this->initializeStock()->first()->date;
    }

    /**
     * Get the quantity attribute for the product.
     * If the product has a history, return the quantity from the first history record.
     *
     * @return float
     */
    public function getQuantityAttribute()
    {
        return $this->initializeStock()->first()->quantity; // Return null if no history or no quantity in the first history record
    }


    /**
     * Get the quantity attribute for the product.
     * If the product has a history, return the quantity from the first history record.
     *
     * @return float
     */
    public function getAmountAttribute($value)
    {
        if ($this->pivot && isset($this->pivot->amount)) {
            return $this->pivot->amount;
        }

        if ($this->pivot && isset($this->pivot->quantity)) {
            return $this->pivot->quantity;
        }

        return $value ?? 0;
    }

    /**
     * Get the percentage attribute for the product.
     * If the product has a formula, return the percentage from the formula record.
     *
     * @return float
     */
    public function getPercentageAttribute($value)
    {
        if ($this->pivot && isset($this->pivot->percentage)) {
            return $this->pivot->percentage;
        }

        return $value ?? 0;
    }

    /**
     * Get the category for the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductType::class, 'inventory_id');
    }

    /**
     * Get the default warehouse for the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * Get the default rack for the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rack()
    {
        return $this->belongsTo(Rack::class);
    }

    /**
     * Get all racks associated with the product through the history relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function racks()
    {
        return $this->belongsToMany(Rack::class, History::class)->withPivot(['histories_id', 'histories_type']);
    }

    public function latestFormula()
    {
        return $this->hasOne(Formula::class)->latestOfMany();
    }

    public function latestResult()
    {
        return $this->hasOne(ProductResult::class, 'description', 'name')->latestOfMany();
    }

    // Accessor for total_amount
    public function getTotalAmountAttribute()
    {
        // Check if the attribute is already set (e.g., by a query with a scope)
        if (array_key_exists('total_amount', $this->attributes)) {
            return $this->attributes['total_amount'];
        }

        // Calculate total_amount if not set
        return $this->orders()
            ->wherePivot('status', 'prepare')
            ->sum('pivot.amount');
    }

    public function receives()
    {
        return $this->morphedByMany(ReceiveItem::class, 'histories');
    }

    public function getLastReceiveBeforeDate($date)
    {
        $endDate = Carbon::parse($date);

        return $this->receives
            ->where('date', '<=', $endDate)
            ->sortBy('date')
            ->first();
    }

    public function results()
    {
        return $this->morphedByMany(ProductResult::class, 'histories');
    }

    public function getLastResultBeforeDate($date)
    {
        $endDate = Carbon::parse($date);

        return $this->results
            ->where('date', '<=', $endDate)
            ->sortBy('date')
            ->first();
    }

    public function workOrders()
    {
        // Define the many-to-many relationship with the WorkOrder model
        return $this->hasMany(WorkOrder::class, 'endProduct_id');
    }

    public function latestOfWorkOrder()
    {
        return $this->hasOne(WorkOrder::class, 'endProduct_id')->latestOfMany();
    }

    public function formulas()
    {
        return $this->belongsToMany(Formula::class)->withPivot('percentage');
    }


    
    /**
     * Get the total order quantity with a specific status.
     *
     * @param array $excludeWorkorders List of workorder IDs to exclude.
     * @return float
     */
    public function getReserveWorkQuantity(array $excludeWorkorders = []): float
    {
        // Define the statuses to include in the filter
        $includeStatus = [
            Status::PREPARE(),
            Status::PROCESS(),
            Status::RUNNING(),
        ];

        $reserveQuantity = 0;

        foreach ($this->formulas as $formula) {
            foreach ($formula->workOrders as $workorder) {
                if (!in_array($workorder->id, $excludeWorkorders)) {
                    if (in_array($workorder->status, $includeStatus)) {
                        $reserveQuantity += round($formula->pivot->percentage * $workorder->plan_quantity / 100, 3);
                    }
                }
            }
        }

        return $reserveQuantity;
    }
}
