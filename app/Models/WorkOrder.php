<?php

namespace App\Models;

use App\Enums\Status;
use App\Traits\DateFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkOrder extends Model
{
    use HasFactory, DateFormat;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the formatted date attribute.
     *
     * @return string|null
     */
    public function getProductNameAttribute()
    {
        return $this->formula->product->name;
    }

    /**
     * Get the formatted date attribute.
     *
     * @return string|null
     */
    public function getEndProductNameAttribute()
    {
        return $this->endProduct->name;
    }

    /**
     * Get the formatted date attribute.
     *
     * @return string|null
     */
    public function getLongDateAttribute()
    {
        // If the date is already a Carbon instance, return it formatted, otherwise parse and format it
        return ($this->date instanceof Carbon) ? $this->date->format('d M Y') : Carbon::createFromFormat('d/m/Y', $this->date)->format('d M Y');
    }

    /**
     * Get the formatted date attribute.
     *
     * @return string|null
     */
    public function getLongDueDateAttribute()
    {
        // If the date is already a Carbon instance, return it formatted, otherwise parse and format it
        return ($this->date instanceof Carbon) ? $this->date->format('d M Y') : (Carbon::createFromFormat('d/m/Y', $this->date)->endOfMonth())->format('d M Y');
    }

    /**
     * Get the color of the product associated with the work order.
     *
     * @return string|null
     */
    public function getColorAttribute()
    {
        return $this->formula->product->color;
    }

    /**
     * Calculate and return the number of mixing.
     *
     * @return float|null
     */
    public function getNoOfMixingAttribute()
    {
        return $this->plan_quantity / $this->mixing_quantity;
    }

    /**
     * Get the name of the extruder machine associated with the work order.
     *
     * @return string|null
     */
    public function getExtruderNameAttribute()
    {
        return $this->extruder->name ?? "-";
    }

    /**
     * Get the name of the mixer machine associated with the work order.
     *
     * @return string|null
     */
    public function getMixerNameAttribute()
    {
        return $this->mixer->name ?? "-";
    }

    /**
     * Define the relationship for the extruder machine.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function extruder()
    {
        return $this->belongsTo(Machine::class, 'extruder_id');
    }

    /**
     * Define the relationship for the mixer machine.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mixer()
    {
        return $this->belongsTo(Machine::class, 'mixer_id');
    }

    /**
     * Define the relationship for the machine process type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function machineProcType()
    {
        return $this->belongsTo(MachineProcType::class, 'machineProcType_id');
    }

    /**
     * Define the relationship for the end product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function endProduct()
    {
        return $this->belongsTo(Product::class, 'endProduct_id');
    }

    /**
     * Define the relationship for the formula.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formula()
    {
        return $this->belongsTo(Formula::class);
    }

    public function customers()
    {
        return $this->belongsToMany(Relation::class, 'customer_work_order', 'work_order_id', 'relation_id');
    }

    public function salesorders()
    {
        return $this->belongsToMany(SalesOrder::class, 'product_sales_order')->withPivot(['product_id', 'amount', 'ship_priority', 'ship_date', 'remarks', 'status'])->withTimestamps();
    }

    public function release()
    {
        return $this->belongsTo(MaterialRelease::class, 'lot', 'for');
    }

    public function result()
    {
        return $this->belongsTo(ProductResult::class, 'lot', 'for');
    }

    public function setProductStatus()
    {
        // If work order exists
        if ($this->lot) {
            // If work order has a lot number
            if ($this->release) {
                if ($this->result) {
                    return Status::FINISHED();
                } else {
                    return Status::PROCESS();
                }
            } else {
                return Status::PREPARE();
            }
        }

        return Status::PLANNING();
    }

    public function getRawMaterialUsageAttribute()
    {
        // Check if release is not null before accessing its products
        if ($this->release) {
            return $this->release->products->sum('amount');
        }

        // Return "-" if release is null
        return "-";
    }

    public function getQuantityResultAttribute()
    {
        // Check if result is not null before accessing its products
        if ($this->result) {
            return $this->result->products->sum('amount');
        }

        // Return "-" if result is null
        return "-";
    }

    public function setOrder()
    {
        $query = self::where('extruder_id', $this->extruder_id)
            ->where('id', '!=', $this->id);

        // Filter by status only if the status is 'finish'
        if ($this->status === Status::FINISHED()) {
            $query->where('status', Status::FINISHED());
        }

        // Get the maximum order value, or default to 0
        $latestOrder = $query->max('order') ?? 0;

        // Increment order by 1
        $this->order = $latestOrder + 1;
    }
}
