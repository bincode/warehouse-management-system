<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Define a one-to-many relationship with the DeliveryOrder model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function delivery()
    {
        return $this->hasMany(DeliveryOrder::class, 'description');
    }

    /**
     * Define a one-to-many relationship with the ReceiveItem model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function receive()
    {
        return $this->hasMany(ReceiveItem::class, 'description');
    }
}
