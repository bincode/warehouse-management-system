<?php

namespace App\Models;

use App\Traits\HistoryTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    use HasFactory, HistoryTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'note', 'warehouse_id'];

    /**
     * Scope a query to include filtered history.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  int|null  $productID
     * @return void
     */
    public function scopeFilterByProduct($query, $productID = null)
    {
        $query->whereHas('history', function ($historyQuery) use ($productID) {
            $historyQuery->where('product_id', $productID);
        });
    }

    /**
     * Get the warehouse that owns the rack.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
