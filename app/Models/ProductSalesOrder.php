<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductSalesOrder extends Pivot
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the product inititalization date.
     *
     * @param  \Carbon\Carbon|string  $value
     * @return \Carbon\Carbon|string
     */
    public function getDateAttribute($value)
    {
        return ($value instanceof Carbon) ? $value : Carbon::createFromFormat('Y-m-d', $value)->format("d/m/Y");
    }

    /**
     * Get the product associated with the sales order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the sales order associated with the product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(SalesOrder::class, 'sales_order_id');
    }

    /**
     * Get the work order associated with the sales order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workorder()
    {
        return $this->belongsTo(WorkOrder::class, 'work_order_id');
    }
}
