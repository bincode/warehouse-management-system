<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliverySchedule extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_plannings';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function reference()
    {
        return $this->belongsTo(ProductSalesOrder::class, "product_sales_order_id");
    }

    public function delivery()
    {
        return $this->belongsTo(DeliveryOrder::class, "delivery_order_id");
    }
}
