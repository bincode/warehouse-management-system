<?php

namespace App\Models;

use App\Traits\Transaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialRelease extends Model
{
    use HasFactory, Transaction;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['type'];

    /**
     * Determine the transaction type.
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        return 'CREDIT';
    }

    /**
     * Determine the transaction type.
     *
     * @return string
     */
    public function getConsumptionAttribute()
    {
        return $this->products->sum('pivot.quantity');
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'releases';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the end of product for the release.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'description');
    }

    /**
     * Get the result for after the release.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function result()
    {
        return $this->hasOne(ProductResult::class, 'for', 'for');
    }

    public function workorder()
    {
        return $this->hasOne(WorkOrder::class, 'lot', 'for');
    }
}
