<?php

namespace App\Models;

use App\Enums\Status;
use App\Traits\DateFormat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    use HasFactory, DateFormat;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['po_number', 'date', 'company_id'];

    public $timestamps = true;

    public function company()
    {
        return $this->belongsTo(Relation::class, 'company_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(['work_order_id', 'amount', 'ship_priority', 'ship_date', 'remarks', 'status']);
    }

    public function workorders()
    {
        return $this->belongsToMany(WorkOrder::class, 'product_sales_order')->withPivot(['product_id', 'amount', 'ship_date', 'remarks', 'status'])->withTimestamps();
    }

    public function workorder($productId)
    {
        return $this->belongsToMany(WorkOrder::class, 'product_sales_order')
            ->withPivot(['product_id', 'amount', 'ship_date', 'remarks', 'status'])
            ->withTimestamps()
            ->wherePivot('product_id', $productId)
            ->first();
    }

    public function setProductStatus($product, $needQuantity)
    {
        // Check if there is a work order for the current product
        $workOrder = $this->workorder($product->id);

        // Determine status based on work order and stock availability
        if ($workOrder) {
            return $workOrder->setProductStatus();
        }

        // If no work order and stock is sufficient
        if ($product->stock > ($needQuantity + $product->reserveOrderQuantity)) {
            return Status::FINISHED();
        }

        // Default status if none of the above conditions are met
        return Status::CREATED();
    }
}
