<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self ALL()
 * @method static self CREATED()
 * @method static self PLANNING()
 * @method static self PREPARE()
 * @method static self RUNNING()
 * @method static self PROCESS()
 * @method static self FINISHED()
 * @method static self PACKING()
 * @method static self DELIVERED()
 * @method static self COMPLETED()
 * @method static self HOLD()
 * @method static self CANCELED()
 */
final class Status extends Enum
{
    /**
     * Get the lowercase version of the enum value.
     *
     * @param string $name The name of the enum value.
     * @return \Closure
     */
    protected static function values(): \Closure
    {
        return fn(string $name) => $name === 'ALL' ? '' : mb_strtolower($name);
    }

    /**
     * Map enum values to corresponding CSS classes.
     *
     * @return array
     */
    public static function cssClasses(): array
    {
        return [
            self::ALL()->value => 'btn-all',          // Warna untuk ALL
            self::CREATED()->value => 'btn-created',   // Warna untuk CREATED
            self::PLANNING()->value => 'btn-planning', // Warna untuk PLANNING
            self::PREPARE()->value => 'btn-prepare',
            self::RUNNING()->value => 'btn-running',   // Warna untuk PREPARE
            self::PROCESS()->value => 'btn-process',   // Warna untuk PROCESS
            self::FINISHED()->value => 'btn-finished', // Warna untuk FINISHED
            self::PACKING()->value => 'btn-packing',   // Warna untuk PACKING
            self::DELIVERED()->value => 'btn-delivered', // Warna untuk DELIVERED
            self::COMPLETED()->value => 'btn-completed', // Warna untuk COMPLETED
            self::HOLD()->value => 'btn-hold',         // Warna untuk HOLD
            self::CANCELED()->value => 'btn-canceled', // Warna untuk CANCELED
        ];
    }

    /**
     * Get the CSS class for a specific enum value.
     *
     * @param string $value The enum value.
     * @return string|null The corresponding CSS class, or null if not found.
     */
    public static function getCssClass(string $value): ?string
    {
        return self::cssClasses()[$value] ?? null;
    }
}
