<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self CUSTOMER()
 * @method static self SUPPLIER()
 * @method static self CUSTOMER_SUPPLIER()
 */
final class RelationType extends Enum
{
    /**
     * Get the lowercase version of the enum value.
     *
     * @param string $name The name of the enum value.
     * @return \Closure
     */
    protected static function values(): \Closure
    {
        return fn (string $name) => implode('', array_map(fn ($word) => mb_strtolower(substr($word, 0, 1)), explode('_', $name)));
    }

    /**
     * Get the label version of the enum value.
     *
     * @param string $name The name of the enum value.
     * @return \Closure
     */
    protected static function labels(): \Closure
    {
        return fn (string $name) => str_replace('_', ' & ', ucwords(str_replace(' ', '_', $name)));
    }
}
