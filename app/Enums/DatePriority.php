<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self SPECIFIED_DATE()
 * @method static self ASAP()
 * @method static self URGENT()
 */
final class DatePriority extends Enum{}
