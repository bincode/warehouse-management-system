<?php

namespace App\View\Components\Kanban;

use Illuminate\View\Component;

class ScheduleProduction extends Component
{
    public $machines;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($machines)
    {
        $this->machines = $machines;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.kanban.schedule-production');
    }
}
