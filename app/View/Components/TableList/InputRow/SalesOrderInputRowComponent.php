<?php

namespace App\View\Components\TableList\InputRow;

class SalesOrderInputRowComponent extends BaseInputRowComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        parent::__construct($data);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.row-input.sales-order-input-row-component');
    }
}
