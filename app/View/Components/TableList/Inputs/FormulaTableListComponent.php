<?php

namespace App\View\Components\TableList\Inputs;

class FormulaTableListComponent extends ItemTableComponent
{
    /**
     * The relation to be loaded.
     *
     * @var string
     */
    public $relation = 'materials';

    /**
     * Create a new component instance.
     *
     * @param string  $name
     * @param mixed  $bind
     */
    public function __construct($name = "table-input-formula", $bind = null)
    {
        parent::__construct($name, $bind);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view("components.table-list.inputs.formula-table-list-component");
    }
}
