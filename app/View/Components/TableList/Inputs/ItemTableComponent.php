<?php

namespace App\View\Components\TableList\Inputs;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

/**
 * Class ProductListTable
 *
 * @package App\View\Components\Tables
 */
class ItemTableComponent  extends Component
{
    /**
     * The name of the table.
     *
     * @var string
     */
    public $name;

    /**
     * The relation to be loaded.
     *
     * @var string
     */
    public $relation = 'products';

    /**
     * The items to be displayed in the table.
     *
     * @var Collection
     */
    public $items;

    /**
     * Create a new component instance.
     *
     * @param string  $name
     * @param string  $relation
     * @param mixed  $bind
     */
    public function __construct($name, $bind)
    {
        $this->name = $name;
        $this->items = $this->load($bind);
    }

    private function load($bind)
    {
        $relation = $this->relation;

        // Check if the binding or relation is null
        if (is_null($bind) || is_null($relation)) {
            return new Collection();
        }

        // Load the specified relation and return the result
        return $bind->load($relation)->getRelation($relation);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('components.tables.product-list-table');
    }
}
