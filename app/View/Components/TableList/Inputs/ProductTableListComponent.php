<?php

namespace App\View\Components\TableList\Inputs;

class ProductTableListComponent extends BaseTableListComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($bind, $name = 'table-input-materials')
    {
        parent::__construct($name, $bind);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.inputs.product-table-list-component');
    }
}
