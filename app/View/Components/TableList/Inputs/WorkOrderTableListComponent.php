<?php

namespace App\View\Components\TableList\Inputs;

class WorkOrderTableListComponent extends ItemTableComponent
{
    /**
     * The relation to be loaded.
     *
     * @var string
     */
    public $relation = 'materials';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($bind, $name = 'table-input-work-order')
    {
        parent::__construct($name, $bind);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.inputs.work-order-table-list-component');
    }
}
