<?php

namespace App\View\Components\TableList\Inputs;

class SalesOrderListComponent extends ItemTableComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = 'input-sales-order', $bind = null)
    {
        parent::__construct($name, $bind);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.table-list.inputs.sales-order-list-component');
    }
}
