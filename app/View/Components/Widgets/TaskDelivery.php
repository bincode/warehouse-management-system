<?php

namespace App\View\Components\Widgets;

use App\Enums\Status;
use Illuminate\View\Component;

class TaskDelivery extends Component
{
    public $task;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
    }

    public function getNote()
    {
        if ($this->task->note) {
            return $this->task->note;
        }

        if ($this->task->document == "Nothing") {
            return "READY";
        }

        return $this->task->document;
    }

    public function makeOutlineColor()
    {
        return Status::getCssClass($this->task->reference->status);
    }

    public function makeBadgeClass()
    {
        // Initialize an array with a base class for the button
        $classes = ["badge"];

        // Add a class based on the status using the enum
        if ($this->task->note) {
            $classes[] = "badge-danger";
        }

        switch ($this->task->document) {
            case 'COA':
                $classes[] = "badge-warning";
                break;
            case 'Color Book':
                $classes[] = "badge-success";
                break;
            case 'COA+CB':
                $classes[] = "badge-primary";
                break;
            case 'COA+CHIP':
                $classes[] = "badge-orange";
                break;
            default:
                $classes[] = "badge-secondary";
                break;
        }

        $classes[] = "mt-1 pl-3 pr-3 pt-2 pb-2";

        // Join the array elements into a single string separated by spaces
        return implode(" ", $classes);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.task-delivery');
    }
}
