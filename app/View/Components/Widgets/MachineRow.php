<?php

namespace App\View\Components\Widgets;

use Illuminate\View\Component;

class MachineRow extends Component
{
    public $machine;
    public $workorders;
    private $totalColumns = 6;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($machine)
    {
        $this->machine = $machine;
        $this->workorders = $machine->workOrders->pad($this->totalColumns, null)->take(6);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.machine-row');
    }
}
