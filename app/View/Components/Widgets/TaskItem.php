<?php

namespace App\View\Components\Widgets;

use App\Enums\Status;
use App\Models\WorkOrder;
use Illuminate\View\Component;

class TaskItem extends Component
{
    public $order;

    public $status;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(WorkOrder $workorder)
    {
        $this->order = $workorder;

        $so = $workorder->salesorders->first();

        //if ($so) {
        //    $product = $so->products->firstWhere('id', $workorder->endProduct_id);
        //    $this->status = optional($product->pivot)->status;
        //} else {
            $this->status = $workorder->status;
        //}
    }

    /**
     * Generate a string of CSS classes for the button based on the order's properties.
     *
     * @return string
     */
    public function makeButtonClass()
    {
        // Initialize an array with a base class for the button
        $classes = ["btn btn-block"];

        // Add a class based on the status using the enum
        if ($this->status) {
            $classes[] = Status::getCssClass($this->status);
        }

        // Add a generic class for the task
        $classes[] = "task";

        // Join the array elements into a single string separated by spaces
        return implode(" ", $classes);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.task-item');
    }
}
