<?php

namespace App\View\Components\Forms\Inputs;

use App\View\Components\Forms\BaseInputGroupComponent;
use DateTime;
use Exception;

class InputComponent extends BaseInputGroupComponent
{
    /**
     * The Daterange Picker option for the input element.
     *
     * @var mixed
     */
    public $daterangepicker;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string|null $label
     * @param string|null $icon
     * @param string|null $fgroupClass
     * @param string|null $igroupClass
     * @param mixed|null $bind
     * @param mixed|null $daterangepicker
     */
    public function __construct($name, $label = null, $icon = null, $btnText = null, $btnId = null, $fgroupClass = null, $igroupClass = null, $bind = null, $daterangepicker = null)
    {
        // Call the parent constructor
        parent::__construct($name, $label, $icon, $fgroupClass, $igroupClass, $bind, $btnText, $btnId);

        // Set the daterangepicker property
        $this->setDateRangePicker($daterangepicker);
    }

    /**
     * Set up the Daterange Picker for the input element.
     *
     * @param mixed $value
     * @return void
     */
    private function setDateRangePicker($value)
    {
        $this->daterangepicker = $value;

        // If Daterange Picker is enabled, set the icon to a calendar icon
        if (isset($value)) {
            $this->icon = 'far fa-calendar-alt';
        }
    }

    /**
     * Get the value of the input field.
     *
     * @return mixed The value of the input field
     */
    public function getValue()
    {
        $value = parent::getValue();
        // If Daterange Picker is enabled, set the icon to a calendar icon
        if ($this->daterangepicker && !empty($value)) {
            // Check if the value is in the format 'YYYY-MM-DD'
            if (preg_match('/\d{4}-\d{2}-\d{2}/', $value)) {
                try {
                    $date = new DateTime($value);
                    $value = $date->format('d/m/Y');
                } catch (Exception $e) {
                    // Handle the case where $value is not a valid date
                    // Optionally log the error or set a default formatted value
                    $value = '';
                }
            }
        }

        return $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.inputs.input-component');
    }
}
