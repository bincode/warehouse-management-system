<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;
use Illuminate\Support\Str;

class BaseInputGroupComponent extends Component
{
    /**
     * The linked data that is bound to the input element.
     *
     * @var mixed
     */
    public $bind;

    /**
     * The name attribute of the input element.
     *
     * @var string
     */
    public $name;

    /**
     * The label text for the input element.
     *
     * @var string
     */
    public $label;

    /**
     * The Font Awesome icon for the input element.
     * Reference: https://fontawesome.com/
     *
     * @var string
     */
    public $icon;
    public $btnId;
    public $btnText;

    /**
     * Extra classes for the "form-group" container element. This provides a way to
     * customize the main container style.
     *
     * @var string
     */
    public $fgroupClass;

    /**
     * Extra classes for the "input-group" container element. This provides a way to
     * customize the input group container style.
     *
     * @var string
     */
    public $igroupClass;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $label = null, $icon = null, $fgroupClass = null, $igroupClass = null, $bind = null, $btnText = null, $btnId = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->icon = $icon;
        $this->btnId = $btnId;
        $this->btnText = $btnText;
        $this->fgroupClass = $fgroupClass;
        $this->igroupClass = $igroupClass;
        $this->bind = $bind;
    }

    /**
     * Create extra classes for the "form-group" element.
     *
     * @return string
     */
    public function makeFormGroupClass(): string
    {
        // Start with the 'form-group' class
        $classes = ['form-group'];

        // Add any extra classes specified in the $fgroupClass property
        $classes[] = $this->fgroupClass;

        // Return the classes as a string
        return implode(' ', $classes);
    }

    /**
     * Create extra classes for the "input-group" element.
     *
     * @return string
     */
    public function makeInputGroupClass(): string
    {
        // Start with the 'input-group' class
        $classes = ['input-group'];

        // Add any extra classes specified in the $igroupClass property
        $classes[] = $this->igroupClass;

        // Return the classes as a string
        return implode(' ', $classes);
    }

    /**
     * This method removes the square brackets from the input name
     */
    public function removeBracketName()
    {
        return Str::before($this->name, '[');
    }

    /**
     * Get the value of the input field.
     *
     * @return mixed The value of the input field
     */
    public function getValue()
    {
        // Set default value based on input type
        $value = $this->attributes['type'] == 'number' ? 0 : '';

        // Get value from old input data
        $value = old($this->name, $value);

        // Get value from bound model
        $name = $this->removeBracketName();

        // Get value from bound model, if available
        $boundValue = $this->bind->$name ?? null;

        // Check pivot value if bound value is null
        if (is_null($boundValue) && isset($this->bind->pivot)) {
            $boundValue = $this->bind->pivot->$name ?? null;
        }

        // Use bound or pivot value if available, otherwise keep the old or default value
        $value = $boundValue ?? $value;

        // Get value from input attributes
        $value = $this->attributes->get('value', $value);

        return $value;
    }

    /**
     * Get the view that represents the component.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('components.forms.base-input-group-component');
    }
}
