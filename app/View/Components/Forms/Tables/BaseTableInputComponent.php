<?php

namespace App\View\Components\Forms\Tables;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class BaseTableInputComponent extends Component
{
    /**
     * The items to be displayed in the table.
     *
     * @var Collection
     */
    public Collection $items;

    /**
     * The name of the table.
     *
     * @var string
     */
    public string $name;

    /**
     * Create a new component instance.
     *
     * @param string $name The name of the table.
     * @param Model|null $bind The binding data.
     * @param string $relation The relation to load.
     */
    public function __construct(string $name, ?Model $bind, string $relation)
    {
        $this->name = $name;
        $this->items = $this->loadItems($bind, $relation);
    }

    /**
     * Load the items associated with the binding.
     *
     * @param Model|null $bind The binding model.
     * @param string $relation The relation to load.
     * @return Collection The collection of items.
     */
    private function loadItems(?Model $bind, string $relation): Collection
    {
        // Return empty collection if binding is null
        if (is_null($bind)) {
            return collect(); // collect() is shorthand for new Collection()
        }

        $item = $bind->load(["{$relation}.history" => function ($hQuery) use ($bind) {
            $hQuery->where('histories_id', $bind->id)->with('rack');
        }])->$relation->transform(function ($item) {
            // Move 'quantity' and 'rack' from history to item level
            if ($item->history && count($item->history) > 0) {
                $item->rack = $item->history[0]->rack;
            }
            // Remove 'history' if you don't need it in final output
            unset($item->history);

            return $item;
        });
        return $item;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return '';
    }
}
