<?php

namespace App\View\Components\Forms\Tables\Row;

class WorkOrderRowInputComponent extends BaseInputRowComponent
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.tables.row.work-order-row-input-component');
    }
}
