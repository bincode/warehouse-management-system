<?php

namespace App\View\Components\Forms\Tables;

use Illuminate\View\Component;

class WorkOrderTableInputComponent extends Component
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.tables.work-order-table-input-component');
    }
}
