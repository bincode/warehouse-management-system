<?php

namespace App\View\Components\Forms\Tables;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class MaterialTableInputComponent extends BaseTableInputComponent
{
    public ?string $type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = "table-input-materials", $bind = null, $type = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->items = $this->loadItems($bind, 'products.racks');
    }

    private function loadItems(?Model $bind, string $relation): Collection
    {
        // Return empty collection if binding is null
        if (is_null($bind)) {
            return collect(); // collect() is shorthand for new Collection()
        }

        return $bind->load([$relation => function ($query) use ($bind) {
            if ($this->type) {
                $query->wherePivot('histories_id', $bind->id)->wherePivot('histories_type', $this->type);
            }
        }])->products;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.tables.material-table-input-component');
    }
}
