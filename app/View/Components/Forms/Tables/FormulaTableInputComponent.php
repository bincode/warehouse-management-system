<?php

namespace App\View\Components\Forms\Tables;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class FormulaTableInputComponent extends BaseTableInputComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($bind)
    {
        $this->name = 'table-input-formula';
        $this->items = $this->loadItems($bind, 'materials');
    }

    private function loadItems(?Model $bind, string $relation): Collection
    {
        // Return empty collection if binding is null
        if (is_null($bind)) {
            return collect(); // collect() is shorthand for new Collection()
        }

        return $bind->load($relation)->$relation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.tables.formula-table-input-component');
    }
}
