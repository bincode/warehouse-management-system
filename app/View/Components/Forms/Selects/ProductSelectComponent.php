<?php

namespace App\View\Components\Forms\Selects;

use App\Models\Product;

class ProductSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Get the bound product
        $bind = $this->getBindProduct();

        // If a product is available, return an array with the product's ID as the key and its name as the value
        if ($bind instanceof Product) {
            return [$bind->id => $bind->name];
        }

        if ($bind) {
            $product = Product::find($bind);
            return [$product->id => $product->name];
        }


        // If no product is available, return an empty array
        return [];
    }

    /**
     * Check if a value is selected.
     *
     * @param  mixed  $value
     * @return bool
     */
    public function isSelected($value) : bool
    {
        // Get the bound product
        $bind = $this->getBindProduct();

        if ($bind instanceof Product) {
            return $value === $bind->id;
        }

        // Return true if a product is available and the given value matches the product's ID
        return $bind && $value === $bind;
    }

    /**
     * Get the bind product from the bind object.
     *
     * @return \App\Models\Product|null
     */
    private function getBindProduct()
    {
        // If the bind object is an instance of Product, return it directly
        if ($this->bind instanceof Product) {
            return $this->bind;
        }

        // Extracting the proc name from the component name
        $name = str_replace('_id', '', $this->name);

        // If the bind object has a "product" property, return that property's value
        if (isset($this->bind->$name)) {
            return $this->bind->$name;
        }

        // If no bound product is found, return null
        return null;
    }
}
