<?php

namespace App\View\Components\Forms\Selects;

class MachineCategorySelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Extracting the proc name from the component name
        $category = str_replace('_id', '', $this->name);

        // Check if the model has a pivot relationship and retrieve proc information
        if ($this->bind && $proc = $this->bind->pivot?->$category) {
            return [$proc->id => $proc->name];
        }

        // Check if the model has a proc relationship and retrieve proc information
        if ($this->bind && $proc = $this->bind->$category) {
            return [$proc->id => $proc->name];
        }

        // If neither a pivot nor a proc relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value) :bool
    {
        // Extracting the proc name from the component name
        $category = str_replace('_id', '', $this->name);

        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->$category->id;
        }

        // Check if the model is bound and has a proc relationship
        if ($this->bind && isset($this->bind->$category)) {
            return $value === $this->bind->$category->id;
        }

        // Return false if the model is not bound or does not have a pivot or proc relationship
        return false;
    }
}
