<?php

namespace App\View\Components\Forms\Selects;

class MachineSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Extracting the machine name from the component name
        $machineType = str_replace('_id', '', $this->name);

        // Check if the model has a pivot relationship and retrieve machine information
        if ($this->bind && $machine = $this->bind->pivot?->$machineType) {
            return [$machine->id => $machine->name];
        }

        // Check if the model has a machine relationship and retrieve machine information
        if ($this->bind && $machine = $this->bind->$machineType) {
            return [$machine->id => $machine->name];
        }

        // If neither a pivot nor a machine relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value): bool
    {
        // Extracting the machine name from the component name
        $machineType = str_replace('_id', '', $this->name);

        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->$machineType->id;
        }

        // Check if the model is bound and has a machine relationship
        if ($this->bind && isset($this->bind->$machineType)) {
            return $value === $this->bind->$machineType->id;
        }

        // Return false if the model is not bound or does not have a pivot or machine relationship
        return false;
    }
}
