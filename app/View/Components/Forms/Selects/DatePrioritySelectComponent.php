<?php

namespace App\View\Components\Forms\Selects;

use App\Enums\DatePriority;

class DatePrioritySelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        return DatePriority::toArray();
    }

    /**
     * Setup Date Range Picker
     *
     * @return void
     */
    protected function set_select2($value)
    {
        $this->select2 = false;
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value): bool
    {
        return isset($this->bind->ship_priority) && $value === $this->bind->ship_priority;
    }
}
