<?php

namespace App\View\Components\Forms\Selects;

use App\Enums\Status;

class StatusSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        return Status::toArray();
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->status) && $value === $this->bind->status;
    }
}
