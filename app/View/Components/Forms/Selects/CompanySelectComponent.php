<?php

namespace App\View\Components\Forms\Selects;

use Illuminate\Support\Collection;

class CompanySelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        $name = str_replace(['_id', '[]'], '', $this->name);

        // Initialize an empty options array
        $options = [];

        // Check if the model has a company relationship and retrieve company information
        if ($this->bind && $companies = $this->bind->$name) {
            if (is_array($companies) || $companies instanceof Collection) {
                foreach ($companies as $company) {
                    $options[$company->id] = $company->name;
                }
            } elseif ($companies = $this->bind->company) {
                $options[$companies->id] = $companies->name;
            } else {
                $options[$companies->id] = $companies->name;
            }
        }

        // Check if the model has a pivot relationship and retrieve company information
        if ($this->bind && $companies = $this->bind->pivot?->$name) {
            if (is_array($companies) || $companies instanceof Collection) {
                foreach ($companies as $company) {
                    $options[$company->id] = $company->name;
                }
            } elseif ($companies = $this->bind->pivot?->company) {
                $options[$companies->id] = $companies->name;
            } else {
                $options[$companies->id] = $companies->name;
            }
        }

        // Return the options array
        return $options;
    }

    /**
     * Determine if the given value is selected.
     *
     * @param mixed $value
     * @return bool
     */
    public function isSelected($value): bool
    {
        $name = str_replace(['_id', '[]'], '', $this->name);

        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->company->id;
        } elseif ($this->bind && isset($this->bind->pivot)) {
            return $this->bind->pivot->$name->pluck('id')->contains($value);
        }

        // Check if the model is bound and has a company relationship
        if ($this->bind && isset($this->bind->company)) {
            return $value === $this->bind->company->id;
        } elseif ($this->bind && isset($this->bind->$name)) {
            return $this->bind->$name->pluck('id')->contains($value);
        }

        // Return false if the model is not bound or does not have a pivot or company relationship
        return false;
    }
}
