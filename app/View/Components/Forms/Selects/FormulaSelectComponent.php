<?php

namespace App\View\Components\Forms\Selects;

class FormulaSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the model has a formula relationship and retrieve formula information
        if ($this->bind && $formula = $this->bind->formula) {
            return [$formula->id => $formula->product->name];
        }

        // Check if the model has a pivot relationship and retrieve formula information
        if ($this->bind && $formula = $this->bind->pivot?->formula) {
            return [$formula->id => $formula->product->name];
        }

        // If neither a pivot nor a formula relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value) : bool
    {
        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->formula->id;
        }

        // Check if the model is bound and has a formula relationship
        if ($this->bind && isset($this->bind->formula)) {
            return $value === $this->bind->formula->id;
        }

        // Return false if the model is not bound or does not have a pivot or formula relationship
        return false;
    }
}
