<?php

namespace App\View\Components\Forms\Selects;

use App\View\Components\Forms\BaseInputGroupComponent;

class BaseSelectComponent extends BaseInputGroupComponent
{
    /**
     * The selected option(s) for the select field.
     * 
     * @var string|array
     */
    public $selected;

    /**
     * The placeholder for the select field.
     * 
     * @var string|array|null
     */
    public $placeholder;

    /**
     * The available options for the select field.
     * 
     * @var array
     */
    public $option;

    /**
     * The key to use for option values (e.g., 'name', 'id').
     * 
     * @var string
     */
    public $key;

    /**
     * The relation name to bind data from.
     * 
     * @var string
     */
    public $relation;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $option = [], $placeholder = null, $selected = null, $label = null, $icon = null, $fgroupClass = null, $igroupClass = null, $bind = null, $relation = null, $key = 'name')
    {
        // Call the parent constructor to set common properties
        parent::__construct($name, $label, $icon, $fgroupClass, $igroupClass, $bind);

        // Modify name by removing '_id', and set relation and key
        $this->relation    = $relation ?? str_replace(['_id', '[]'], '', $name);
        $this->key         = $key;
        $this->option      = $option;
        $this->placeholder = $placeholder;

        // Set selected option(s), falling back to old input or bound value
        $this->selected    = (array)(old($name) ?? $selected ?? $bind->$name ?? null);
    }

    /**
     * Generate the option list for the select field, including nested relations.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        if (!isset($this->bind)) {
            return [];
        }

        // Support for nested relations using dot notation (e.g., parent.child)
        $relationLevels = explode('.', $this->relation);
        $currentData = $this->bind;

        foreach ($relationLevels as $level) {
            if (is_iterable($currentData)) {
                $currentData = $currentData->first();
            }

            if (!isset($currentData->$level)) {
                return [$currentData->id =>  $currentData->{$this->key}];
            }

            $currentData = $currentData->$level;
        }

        // If it's iterable, construct the options list
        if (is_iterable($currentData)) {
            $options = [];
            foreach ($currentData as $data) {
                $options[$data->id] = $data->{$this->key};
            }
            return $options;
        }
        
        // If it's a single object, return a single option
        return [$currentData->id => $currentData->{$this->key}];
    }

    /**
     * Determine if the given value is selected.
     *
     * @param mixed $value The value to check against selected options.
     * 
     * @return bool
     */
    public function isSelected($value): bool
    {
        if (!isset($this->bind->{$this->relation})) {
            return $this->bind->id == $value;
        }

        // Check if the relation data is iterable
        if (is_iterable($this->bind->{$this->relation})) {
            // Use contains method for collections/arrays
            return $this->bind->{$this->relation}->contains($value);
        }

        // For single objects, compare the id directly
        return $this->bind->{$this->relation}->id == $value;
    }

    /**
     * Method to render the component view
     */
    public function render()
    {
        return view('components.forms.selects.base-select-component');
    }
}
