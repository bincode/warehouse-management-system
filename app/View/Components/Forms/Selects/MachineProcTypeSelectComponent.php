<?php

namespace App\View\Components\Forms\Selects;

class MachineProcTypeSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Extracting the proc name from the component name
        $procType = str_replace('_id', '', $this->name);

        // Check if the model has a pivot relationship and retrieve proc information
        if ($this->bind && $proc = $this->bind->pivot?->$procType) {
            return [$proc->id => $proc->name];
        }

        // Check if the model has a proc relationship and retrieve proc information
        if ($this->bind && $proc = $this->bind->$procType) {
            return [$proc->id => $proc->name];
        }

        // If neither a pivot nor a proc relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value): bool
    {
        // Extracting the proc name from the component name
        $procType = str_replace('_id', '', $this->name);

        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->$procType->id;
        }

        // Check if the model is bound and has a proc relationship
        if ($this->bind && isset($this->bind->$procType)) {
            return $value === $this->bind->$procType->id;
        }

        // Return false if the model is not bound or does not have a pivot or proc relationship
        return false;
    }
}
