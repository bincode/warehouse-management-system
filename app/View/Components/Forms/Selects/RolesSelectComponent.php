<?php

namespace App\View\Components\Forms\Selects;

class RolesSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $fgroupClass
     * @param array $selected
     * @param string $label
     * @param mixed $bind
     */
    public function __construct(string $name = 'roles[]', $fgroupClass = '', $selected = [], $label = 'Roles', $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', $fgroupClass, '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the binding and its roles are set
        if (!isset($this->bind) || !isset($this->bind->roles)) {
            return [];
        }

        // Initialize an empty array to store the options
        $options = [];

        // Iterate over each role in the roles array
        foreach ($this->bind->roles as $role) {
            $options[$role->id] = $role->name;
        }

        return $options;
    }


    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->roles) && $this->bind->roles->contains($value);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.selects.base-select-component');
    }
}
