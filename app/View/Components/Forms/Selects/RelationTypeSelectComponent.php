<?php

namespace App\View\Components\Forms\Selects;

use App\Enums\RelationType;

class RelationTypeSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        return RelationType::toArray();
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value): bool
    {
        return isset($this->bind->type) && $value === $this->bind->type;
    }
}
