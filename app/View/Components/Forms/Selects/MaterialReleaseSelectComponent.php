<?php

namespace App\View\Components\Forms\Selects;

class MaterialReleaseSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the model has a pivot relationship and retrieve release information
        if ($this->bind && $release = $this->bind->pivot?->release) {
            return [$release->id => $release->for];
        }

        // Check if the model has a release relationship and retrieve release information
        if ($this->bind && $release = $this->bind->release) {
            return [$release->id => $release->for];
        }

        // If neither a pivot nor a release relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value) : bool
    {
        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->release->id;
        }

        // Check if the model is bound and has a release relationship
        if ($this->bind && isset($this->bind->release)) {
            return $value === $this->bind->release->id;
        }

        // Return false if the model is not bound or does not have a pivot or release relationship
        return false;
    }
}
