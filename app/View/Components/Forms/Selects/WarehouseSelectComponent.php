<?php

namespace App\View\Components\Forms\Selects;

class WarehouseSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the model has a pivot relationship and retrieve warehouse information
        if ($this->bind && $warehouse = $this->bind->pivot?->warehouse) {
            return [$warehouse->id => $warehouse->name];
        }

        // Check if the model has a warehouse relationship and retrieve warehouse information
        if ($this->bind && $warehouse = $this->bind->warehouse) {
            return [$warehouse->id => $warehouse->name];
        }

        // If neither a pivot nor a warehouse relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->warehouse->id;
        }

        // Check if the model is bound and has a warehouse relationship
        if ($this->bind && isset($this->bind->warehouse)) {
            return $value === $this->bind->warehouse->id;
        }

        // Return false if the model is not bound or does not have a pivot or warehouse relationship
        return false;
    }
}
