<?php

namespace App\View\Components\Forms\Selects;

class PermissionsSelectComponent extends BaseSelectComponent
{
    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $fgroupClass
     * @param array $selected
     * @param string $label
     * @param mixed $bind
     */
    public function __construct(string $name = 'permissions[]', $fgroupClass = '', $selected = [], $label = 'Permissions', $bind = null)
    {
        parent::__construct($name, null, 'Selected...', $selected, $label, '', $fgroupClass, '', $bind, '');
    }

    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the binding and its permissions are set
        if (!isset($this->bind) || !isset($this->bind->permissions)) {
            return [];
        }

        // Initialize an empty array to store the options
        $options = [];

        // Iterate over each permission in the permissions array
        foreach ($this->bind->permissions as $permission) {
            $options[$permission->id] = $permission->name;
        }

        return $options;
    }


    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value)
    {
        return isset($this->bind->permissions) && $this->bind->permissions->contains($value);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.selects.base-select-component');
    }
}
