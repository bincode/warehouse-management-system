<?php

namespace App\View\Components\Forms\Selects;

class InventoryTypeSelectComponent extends BaseSelectComponent
{
    /**
     * Create menu items in popups and other lists of items.
     *
     * @return array
     */
    public function makeOptionList(): array
    {
        // Check if the model has a category relationship and retrieve category information
        if ($this->bind && $category = $this->bind->category) {
            return [$category->id => $category->name];
        }

        // Check if the model has a pivot relationship and retrieve category information
        if ($this->bind && $category = $this->bind->pivot?->category) {
            return [$category->id => $category->name];
        }

        // If neither a pivot nor a category relationship exists, return an empty option list.
        return [];
    }

    /**
     * Create a new component instance.
     *
     * @return bool
     */
    public function isSelected($value) : bool
    {
        // Check if the model is bound and has a pivot relationship
        if ($this->bind && isset($this->bind->pivot)) {
            return $value === $this->bind->pivot->category->id;
        }

        // Check if the model is bound and has a category relationship
        if ($this->bind && isset($this->bind->category)) {
            return $value === $this->bind->category->id;
        }

        // Return false if the model is not bound or does not have a pivot or category relationship
        return false;
    }
}
