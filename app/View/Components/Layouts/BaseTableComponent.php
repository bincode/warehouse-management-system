<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class BaseTableComponent extends Component
{
    /**
     * The array of columns to be displayed in the table.
     *
     * @var array
     */
    public $columns;

    /**
     * Create a new component instance.
     *
     * @param array $columns The columns to be displayed in the table.
     * @return void
     */
    public function __construct($columns = [])
    {
        $this->columns = $columns;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layouts.base-table-component');
    }
}
