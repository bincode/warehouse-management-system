<?php

namespace App\Providers;

use App\Http\View\Composers\CustomerComposer;
use App\Http\View\Composers\FinishGoodComposer;
use App\Http\View\Composers\JobCostReferanceComposer;
use App\Http\View\Composers\ProductTypeComposer;
use App\Http\View\Composers\SupplierComposer;
use App\Http\View\Composers\UnfinishedWorkComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
