<?php

namespace App\Providers;

use App\View\Components\Buttons\BaseButtonComponent;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\View\Components\Layouts\SidebarMenu;
use App\View\Components\Layouts\BaseCardComponent;
use App\View\Components\Layouts\BaseFormComponent;
use App\View\Components\Layouts\BaseTableComponent;
use App\View\Components\Forms\Inputs\InputComponent;
use App\View\Components\Forms\Inputs\TextAreaComponent;
use App\View\Components\Forms\Selects\BaseSelectComponent;
use App\View\Components\Forms\Selects\CompanySelectComponent;
use App\View\Components\Forms\Selects\DatePrioritySelectComponent;
use App\View\Components\Forms\Selects\FormulaSelectComponent;
use App\View\Components\Forms\Selects\RackSelectComponent;
use App\View\Components\Forms\Selects\RolesSelectComponent;
use App\View\Components\Forms\Selects\ProductSelectComponent;
use App\View\Components\Forms\Selects\WarehouseSelectComponent;
use App\View\Components\Forms\Selects\PermissionsSelectComponent;
use App\View\Components\Forms\Selects\RelationTypeSelectComponent;
use App\View\Components\Forms\Selects\InventoryTypeSelectComponent;
use App\View\Components\Forms\Selects\MachineCategorySelectComponent;
use App\View\Components\Forms\Selects\MachineProcTypeSelectComponent;
use App\View\Components\Forms\Selects\MachineSelectComponent;
use App\View\Components\Forms\Selects\MaterialReleaseSelectComponent;
use App\View\Components\Forms\Selects\StatusSelectComponent;
use App\View\Components\Forms\Tables\FormulaTableInputComponent;
use App\View\Components\Forms\Tables\MaterialTableInputComponent;
use App\View\Components\Forms\Tables\Row\FormulaRowInputComponent;
use App\View\Components\Forms\Tables\Row\MaterialRowInputComponent;
use App\View\Components\TableList\InputRow\FormulaInputRowListComponent;
use App\View\Components\TableList\InputRow\ProductInputRowComponent;
use App\View\Components\TableList\InputRow\WorkOrderInputRowComponent;
use App\View\Components\TableList\Inputs\FormulaTableListComponent;
use App\View\Components\TableList\Inputs\SalesOrderListComponent;
use App\View\Components\TableList\Inputs\ProductTableListComponent;
use App\View\Components\TableList\Inputs\WorkOrderTableListComponent;
use App\View\Components\TableList\InputRow\SalesOrderInputRowComponent;
use App\View\Components\Widgets\MachineRow;
use App\View\Components\Widgets\TaskDelivery;
use App\View\Components\Widgets\TaskItem;

class ComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('button', BaseButtonComponent::class);
        Blade::component('sidebar-menu', SidebarMenu::class);

        /** Put Input's Component on Below **/
        Blade::component('input', InputComponent::class);
        Blade::component('input-area', TextAreaComponent::class);

        /** Put Select's Component on Below **/
        Blade::component('select', BaseSelectComponent::class);
        Blade::component('select-company', CompanySelectComponent::class);
        Blade::component('select-date-priority', DatePrioritySelectComponent::class);
        Blade::component('select-formula', FormulaSelectComponent::class);
        Blade::component('select-machine', MachineSelectComponent::class);
        Blade::component('select-machine-category', MachineCategorySelectComponent::class);
        Blade::component('select-material-release', MaterialReleaseSelectComponent::class);
        Blade::component('select-mixer-proc', MachineProcTypeSelectComponent::class);
        Blade::component('select-permissions', PermissionsSelectComponent::class);
        Blade::component('select-product', ProductSelectComponent::class);
        Blade::component('select-rack', RackSelectComponent::class);
        Blade::component('select-roles', RolesSelectComponent::class);
        Blade::component('select-status', StatusSelectComponent::class);
        Blade::component('select-type-relation', RelationTypeSelectComponent::class);
        Blade::component('select-type-inventory', InventoryTypeSelectComponent::class);
        Blade::component('select-warehouse', WarehouseSelectComponent::class);

        /** Put TableList Component on Below **/
        Blade::component('table-product-list', ProductTableListComponent::class);
        Blade::component('table-sales-order-list', SalesOrderListComponent::class);
        Blade::component('table-formula-list', FormulaTableListComponent::class);
        Blade::component('table-work-order-list', WorkOrderTableListComponent::class);

        /** Put InputRow Component on Below **/
        Blade::component('input-product-row', ProductInputRowComponent::class);
        Blade::component('input-sales-order-row', SalesOrderInputRowComponent::class);
        Blade::component('input-formula-row', FormulaInputRowListComponent::class);
        Blade::component('input-work-order-row', WorkOrderInputRowComponent::class);

        /** Put Layout's Component on Below **/
        Blade::component('form', BaseFormComponent::class);
        Blade::component('card', BaseCardComponent::class);
        Blade::component('table', BaseTableComponent::class);

        /** Put Layout's Component on Below **/
        Blade::component('task', TaskItem::class);
        Blade::component('taskDelivery', TaskDelivery::class);

        Blade::component('machine-row', MachineRow::class);

        // New Component's
        Blade::component('table-input-formula', FormulaTableInputComponent::class);
        Blade::component('row-input-formula', FormulaRowInputComponent::class);

        Blade::component('table-input-material', MaterialTableInputComponent::class);
        Blade::component('row-input-material', MaterialRowInputComponent::class);
    }
}
