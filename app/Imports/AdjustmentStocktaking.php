<?php

namespace App\Imports;

use App\Models\Adjustment;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RemembersChunkOffset;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\ImportFailed;

class AdjustmentStocktaking implements ToCollection, WithHeadingRow, WithEvents, WithChunkReading, ShouldQueue
{
    use Importable, RemembersChunkOffset;

    protected $adjustment;
    protected $date;
    protected $count;
    protected $row;

    /**
     * Constructor to initialize date and adjustment, and clear existing products.
     *
     * @param Carbon $date
     * @param Adjustment $adjustment
     */
    public function __construct(Carbon $date, Adjustment $adjustment)
    {
        $this->date = $date->format('Y-m-d');
        $this->adjustment = $adjustment;
        $this->adjustment->products()->sync([]);
    }

    /**
     * Process each chunk of data from the imported file.
     *
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        // Get products with their history up to the given date
        $products = Product::with(['history' => function ($query) {
            $query->whereHasMorph('histories', '*', function ($q) {
                $q->where('date', '<=', $this->date); // Filter history by date
            })
                ->selectRaw('rack_id, product_id, SUM(CASE WHEN type = "DEBIT" THEN quantity ELSE -quantity END) as quantity')
                ->groupBy('rack_id', 'product_id'); // Group by rack_id and product_id
        }])->get();

        // Iterate over each row in the collection
        foreach ($collection as $row) {

            // Find the product by name
            $product = $products->where('name', $row['product_name'])->first();

            // Check if product and history exist
            if ($product && $product->history->isNotEmpty()) {
                // Get the first history record
                $history = $product->history->first();
                // Calculate adjustment quantity
                $adjustmentQuantity = $row['actual_stock'] - $history->quantity;

                // If adjustment is needed
                if ($adjustmentQuantity != 0) {
                    $this->adjustment->products()->attach($product->id, [
                        'quantity' => $adjustmentQuantity,
                        'rack_id' => $history->rack_id,
                        'type' => $this->adjustment->type,
                    ]);
                }
            }
        }
    }

    /**
     * Define the chunk size for reading the file.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 250;
    }

    /**
     * Store the progress of the import.
     */
    public function storeProgress()
    {
        // Get current chunk offset
        $this->row = $this->getChunkOffset();
        // Calculate percentage completed
        $percent = $this->getChunkOffset() / $this->count * 100;
        // Update adjustment status with progress
        $this->adjustment->update(['status' => 'Processing Upload...(' . round($percent, 0) . '%) ' . $this->getChunkOffset() . '/' . $this->count]);
    }

    /**
     * Register events for the import process.
     *
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeImport::class => function (BeforeImport $event) {
                $this->count = $event->reader->getTotalRows()['Worksheet'];
                $this->adjustment->update(['status' => 'Starting Upload']);
            },

            AfterImport::class => function (AfterImport $event) {
                $this->adjustment->update(['status' => 'Upload Completed']);
            },

            ImportFailed::class => function (ImportFailed $event) {
                $this->adjustment->update(['status' => 'Upload Failed']);
            },
        ];
    }
}
