<?php

namespace App\Imports;

use App\Enums\Status;
use App\Models\Product;
use App\Models\Relation;
use App\Models\SalesOrder;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SalesOrderImport implements ToCollection, WithHeadingRow
{
    use Importable;

    protected $prevDate = null;
    protected $prevPoNumber = null;
    protected $prevCustomer = null;

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // Retrieve and convert the date from the current row
            $date = $this->convertDate($row['date']);
            $customer = $this->getCustomer($row['customer_name']);
            $poNumber = $this->getPoNumber($row['po_number']);
            $product = Product::where('description', 'like', '%' . trim($row['product']) . '%')->first();

            $ship_priority = $this->isValidDate($row['ship_date']) ? 'SPECIFIED_DATE' : trim($row['ship_date']);

            // Find or create the SalesOrder
            $order = SalesOrder::where('date', $date->format('Y-m-d'))
                ->where('company_id', $customer->id)
                ->where('po_number', $poNumber)
                ->first();

            if (!$order) {
                // Create a new SalesOrder if none exists
                $order = SalesOrder::create([
                    'date' => $date->format('d/m/Y'),
                    'company_id' => $customer->id,
                    'po_number' => $poNumber
                ]);
            }

            $order->products()->attach($product->id, [
                'amount' => $row['quantity'],
                'ship_priority' => $ship_priority,
                'ship_date' => Carbon::createFromFormat('Y-m-d', Date::excelToDateTimeObject($row['ship_date'])->format('Y-m-d')),
                'remarks' => $row['remark'],
                'status' => Status::CREATED(),
            ]);
        }
    }

    /**
     * Convert serial date to a Carbon instance.
     *
     * @param mixed $value The serial date from the row.
     * @return Carbon The converted Carbon instance.
     */
    protected function convertDate($value)
    {
        if ($value) {
            // Convert the Excel serial date to a Carbon instance
            $value = Carbon::createFromFormat('Y-m-d', Date::excelToDateTimeObject($value)->format('Y-m-d'));

            // Update prev date to the current converted date
            $this->prevDate = $value;
        } else {
            // Use the prev date if available, otherwise default to the current date
            $value = $this->prevDate ?: Carbon::now();
        }

        return $value;
    }

    /**
     * Get the PO number, using the previous one if the current is null.
     *
     * @param mixed $value The current PO number from the row.
     * @return mixed The resolved PO number.
     */
    protected function getPoNumber($value)
    {
        if ($value !== null) {
            $this->prevPoNumber = $value;
        }

        return $this->prevPoNumber;
    }

    protected function getCustomer($value)
    {
        if ($value !== null) {
            $customer = Relation::where('name', 'like', '%' . trim($value) . '%')->first();
            $this->prevCustomer = $customer;
        }

        return $this->prevCustomer;
    }

    // Function to check if a string is a valid date
    private function isValidDate($dateString)
    {
        // Attempt to create a Carbon instance and check if it's a valid date
        try {
            // Convert the Excel serial date to a Carbon instance
            $date = Carbon::createFromFormat('Y-m-d', Date::excelToDateTimeObject($dateString)->format('Y-m-d'));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
