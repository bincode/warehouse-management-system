<?php

namespace App\Console\Commands;

use App\Models\WorkOrder;
use App\Services\WorkOrderService;
use Illuminate\Console\Command;

class UpdateWorkOrderAdjustments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workorder:update-adjustments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update has_adjustment column for work orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new WorkOrderService();
        $workOrders = WorkOrder::all();

        foreach ($workOrders as $workOrder) {
            $hasAdjustment = $service->AdjCheck($workOrder);
            $workOrder->has_adjustment = $hasAdjustment;
            $workOrder->save();
        }

        $this->info('Work orders updated successfully.');
    }
}
