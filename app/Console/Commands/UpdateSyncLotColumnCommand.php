<?php

namespace App\Console\Commands;

use App\Models\MaterialRelease;
use App\Models\ProductResult;
use Illuminate\Console\Command;

class UpdateSyncLotColumnCommand extends Command
{
    protected $signature = 'sync:lot-column';

    protected $description = 'Update "for" column in results table based on id from release table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $results = ProductResult::all();

        foreach ($results as $result) {
            $release = MaterialRelease::where('id', $result->for)->first();
            if ($release) {
                $result->for = $release->for; // Sesuaikan dengan nama kolom yang sesuai di tabel Release
                $result->save();
                $this->info("Updated result id {$result->id} 'for' column to {$result->for}");
            }
        }

        $this->info('All "for" columns updated successfully.');
    }
}
