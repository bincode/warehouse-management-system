<?php

namespace App\Services;

use App\Models\WorkOrder;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class WorkOrderService
{
    /**
     * Get formatted materials data for a given work order.
     *
     * @param WorkOrder $workorder
     * @return array
     */
    public function getFormattedMaterials(WorkOrder $workorder)
    {
        // Format materials and products data
        $data = $this->formatItems($workorder->formula->materials, 'material', $workorder);

        if ($workorder->release) {
            $formattedProducts = $this->formatItems($workorder->release->products, 'product', $workorder);

            // Merge and get unique data by 'name' field
            $data = $data->merge($formattedProducts);
        }

        $uniqueData = $data->unique('name');

        return $uniqueData->values()->all();
    }

    public function AdjCheck(WorkOrder $workorder)
    {
        // Return false if the work order does not have a release  
        if (!$workorder->release) {
            return false;
        }

        // Map materials to an array with name and calculated quantity based on percentage  
        $formulaData = $workorder->formula->materials->map(function ($material) use ($workorder) {
            return [
                'id' => $material->id,
                'name' => $material->name,
                'quantity' => round(($material->pivot->percentage * $workorder->plan_quantity) / 100, 3),
            ];
        });

        // Map products to an array with name and actual quantity  
        $releaseData = $workorder->release->products->map(function ($product) {
            return [
                'id' => $product->id,
                'name' => $product->name,
                'quantity' => round($product->amount, 3),
            ];
        });

        // Cek kesesuaian antara formulaData dan releaseData  
        foreach ($formulaData as $formulaItem) {
            $releaseItem = $releaseData->firstWhere('name', $formulaItem['name']);

            // Jika item tidak ditemukan di releaseData atau kuantitasnya berbeda  
            if (!$releaseItem || $releaseItem['quantity'] != $formulaItem['quantity']) {
                return true; // Ada perbedaan, kembalikan true  
            }
        }

        // Cek apakah ada item di releaseData yang tidak ada di formulaData  
        foreach ($releaseData as $releaseItem) {
            $formulaItem = $formulaData->firstWhere('name', $releaseItem['name']);

            // Jika item tidak ditemukan di formulaData  
            if (!$formulaItem) {
                return true; // Ada item tambahan, kembalikan true  
            }
        }

        // Jika semua item cocok  
        return false; // Kembalikan false jika tidak ada perbedaan
    }


    /**
     * Get the FIFO (First In, First Out) date string for a material.
     *
     * @param $material
     * @param Carbon $formattedDate
     * @return string
     */
    private function getFIFOString($material, Carbon $formattedDate): string
    {
        // Retrieve the last receive entry before the given date
        $lastReceive = $material->getLastReceiveBeforeDate($formattedDate);

        if ($lastReceive) {
            return "Recv. Date : {$lastReceive->date}";
        }

        // If no last receive entry, retrieve the last result entry before the given date
        $lastResult = $material->getLastResultBeforeDate($formattedDate);
        if ($lastResult) {
            return "Lot# : {$lastResult->lot}";
        }

        // If no last receive entry, retrieve the last result entry before the given date
        $workorder = $material->latestOfWorkOrder;
        if ($workorder) {
            return "Lot# : {$workorder->lot}";
        }

        return '-';
    }

    /**
     * Calculate the quantity based on percentage and total quantity.
     *
     * @param float $percentage
     * @param float $totalQuantity
     * @return float
     */
    private function calculateQuantity(float $percentage, float $totalQuantity): float
    {
        return ($percentage * $totalQuantity) / 100;
    }

    /**
     * Get formatted data for materials or products.
     *
     * @param Collection $items
     * @param string $type
     * @param WorkOrder $workorder
     * @return Collection
     */
    protected function formatItems(Collection $items, string $type, WorkOrder $workorder, bool $withFIFO = true): Collection
    {
        return $items->map(function ($item) use ($type, $workorder, $withFIFO) {

            // Convert work order date from 'd/m/Y' format to a Carbon instance
            $date = Carbon::createFromFormat('d/m/Y', $workorder->date);

            // Determine the FIFO date for the material
            $fifoString = $withFIFO ? $this->getFIFOString($item, $date) : '-';

            // Retrieve the percentage of the material from the pivot table
            $percentage = $item->pivot->percentage ?? 0;
            $actualQty = "";

            if ($release = $workorder->release) {
                $actualQty = $release->products->where('id', $item->id)->first()->amount ?? 0;
            }

            // Return the formatted item data
            return [
                'type' => $type,
                'name' => $item->name,
                'date' => $fifoString,
                'percentage' => round($percentage, 3),
                'recipe_qty' => round($this->calculateQuantity($percentage, $workorder->plan_quantity), 3),
                'weight_mixer' => round($this->calculateQuantity($percentage, $workorder->mixing_quantity), 3),
                'actual_qty' => $actualQty,
            ];
        });
    }
}
