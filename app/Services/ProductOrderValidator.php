<?php

namespace App\Services;

use App\Models\Product;

class ProductOrderValidator
{
    public $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function getInvalidStatus(array $validation)
    {
        $array = [];

        foreach ($this->products as $product) {
            if (in_array($product->pivot->status, $validation)) {
                $array[] = $product;
            }
        }

        $this->products = $array;
        return $this;
    }

    public function getInvalidChange(array $amounts)
    {
        $array = [];

        foreach ($this->products as $index => $product) {
            if ($product->pivot->amount != $amounts[$index]) {
                $array[] = $product;
            }
        }
        $this->products = $array;
        return $this;
    }
}
