<?php

namespace App\Services;

use App\Models\Product;

class ProductStockValidator
{
    protected $products;
    protected $workorders;

    /**
     * Initialize the products and workorders array.
     *
     * @param array $products
     */
    public function __construct(array $products, array $workorders = [])
    {
        // Filter products to only include those with both id and quantity
        $this->workorders = $workorders;
        $this->products = array_filter($products, function ($item) {
            return isset($item['id']) && isset($item['quantity']);
        });
    }

    /**
     * Validate stock availability for each product.
     *
     * @return array
     */
    public function validate()
    {
        // Array to store validation results
        $validated = [];

        // Extract product IDs from the array of products
        $productIds = array_column($this->products, 'id');

        // Eager load products with related models (to avoid N+1 queries)
        $products = Product::with([
            'history',
            'latestFormula.materials.history',
            'latestFormula.materials.formulas.workOrders.salesorders',
            'formulas.workOrders.salesorders'
        ])
            ->whereIn('id', $productIds)
            ->get()
            ->keyBy('id');

        // Validate stock for each product
        foreach ($this->products as $item) {

            // Extract product ID and quantity
            $productId = $item['id'];
            $quantity = $item['quantity'];

            // Find product by its ID
            $product = $products->get($productId);

            // If product not found, return error response
            if (!$product) {
                $validated[] = $this->response(false, "Product not found", $productId, null, null, null);
                continue;
            }

            // If product has a formula, validate stock for each material
            if ($formula = $product->latestFormula) {
                $materials = $formula->version == "DILUTED" ? $formula->materials : [$product];

                foreach ($materials as $material) {
                    $validated[] = $this->validateStock($material, $quantity);
                }
            } else {
                // Validate stock for the product itself
                $validated[] = $this->validateStock($product, $quantity);
            }
        }

        // Return all validation results
        return $validated;
    }

    /**
     * Validate stock for a specific product.
     *
     * @param $product
     * @param $quantity
     * @return array
     */
    private function validateStock($product, $quantity)
    {
        // Get the available stock and reserved stock
        $stock = $product->stock;
        $reserveStock = $product->getReserveWorkQuantity($this->workorders);
        $availableStock = $stock - $reserveStock;

        // Check if available stock is less than required quantity
        if ($availableStock < $quantity) {
            return $this->response(false, "Insufficient Stock", $product->id, $product->name, $availableStock, $reserveStock);
        }

        return $this->response(true, "Stock is Available", $product->id, $product->name, $availableStock, $reserveStock);
    }

    /**
     * Get products with insufficient stock.
     *
     * @return array
     */
    public function getInsufficientStock(): array
    {
        // Filter results where stock is insufficient
        return array_filter($this->validate(), fn($item) => !$item['status']);
    }

    /**
     * Get products with sufficient stock.
     *
     * @return array
     */
    public function getAvailableStock(): array
    {
        // Filter results where stock is available
        return array_filter($this->validate(), fn($item) => $item['status']);
    }

    /**
     * Build a response array for stock validation.
     *
     * @param bool $status
     * @param string $message
     * @param int $productId
     * @param string|null $productName
     * @param int|null $avStock
     * @param int|null $rvStock
     * @return array
     */
    protected function response(bool $status, string $message, int $productId, ?string $productName, ?float $avStock, ?float $rvStock): array
    {
        // Return validation result in array format
        return [
            'status' => $status,
            'message' => $message,
            'productId' => $productId,
            'product' => $productName,
            'avStock' => $avStock,
            'rvStock' => $rvStock,
        ];
    }
}
