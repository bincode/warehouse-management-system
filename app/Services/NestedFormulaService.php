<?php

namespace App\Services;

use App\Enums\Status;
use App\Models\WorkOrder;
use Carbon\Carbon;

class NestedFormulaService
{
    public function create(WorkOrder $parent)
    {
        $materials = $parent->formula->materials;

        // Get the current date once
        $currentDate = Carbon::now()->format('d/m/Y');

        // Get the latest order number and lot number
        $latestOrder = WorkOrder::where('extruder_id', $parent->extruder_id)->max('order') ?? 0;

        foreach ($materials as $material) {
            if ($formula = $material->latestFormula) {
                if ($formula->version === "DILUTED") {

                    // Calculate quantities once
                    $percentage = $material->pivot->percentage;
                    $planQuantity = $percentage * $parent->plan_quantity / 100;
                    $mixingQuantity = $planQuantity;

                    // Prepare attributes for creation
                    $attributes = [
                        'formula_id' => $formula->id,
                        'endProduct_id' => $material->id,
                        'date' => $currentDate,
                        'plan_quantity' => $planQuantity,
                        'mixing_quantity' => $mixingQuantity,
                        'order' => ++$latestOrder,
                        'status' => Status::FINISHED(),
                    ];

                    // Temporarily disable observers

                    WorkOrder::withoutEvents(function () use ($attributes) {
                        WorkOrder::create($attributes);
                    });
                }
            }
        }
    }
}
