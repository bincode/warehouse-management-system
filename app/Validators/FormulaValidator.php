<?php

namespace App\Validators;

use App\Models\Formula;
use Illuminate\Http\Request;

class FormulaValidator
{
    public function validate(Request $request, Formula $originalFormula)
    {
        // Get the materials associated with the formula
        $formulaProducts = $originalFormula->materials;

        // Filter out any empty products from the request
        $inputProducts = array_filter($request->products);

        // Filter out any empty percentages from the request
        $inputPercentages = array_filter($request->percentage);


        // Check if the counts of formula materials and input materials are different
        if (count($formulaProducts) !== count($inputProducts)) {
            info('1');
            // The counts are different
            return false;
        }

        // Trim percentages if there are more percentages than products
        $inputPercentages = array_slice($inputPercentages, 0, count($inputProducts));

        // Combine input materials and input percentages into an associative array
        $combinedArray = array_combine($inputProducts, $inputPercentages);

        // Filter out null values from the combined array
        $combinedArray = array_filter($combinedArray);

        // Iterate over the formula materials
        foreach ($formulaProducts as $product) {
            $productId = $product->id;

            // Check if the product ID exists in the combined array
            if (!array_key_exists($productId, $combinedArray)) {
                info('2');
                return false;
            }

            $epsilon = 0.0001; // Toleransi yang diterima
            $diff = abs(floatval($combinedArray[$productId]) - floatval($product->percentage));

            // Compare the percentage
            if ($diff > $epsilon) {
                return false;
            }
        }

        // No differences found
        return true;
    }
}
