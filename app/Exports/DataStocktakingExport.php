<?php

namespace App\Exports;

use App\Models\Product;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

/**
 * Export class for generating stocktaking data in Excel format.
 */
class DataStocktakingExport implements FromQuery, WithHeadings, WithMapping, WithStrictNullComparison, ShouldAutoSize, WithChunkReading
{
    use Exportable;

    // Holds the formatted date used for filtering data.
    protected $date;

    /**
     * Constructor to initialize the export with a specific date.
     *
     * @param Carbon $date Date for filtering product histories.
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date->format('Y-m-d');
    }

    /**
     * Builds the query to fetch products with histories up to the specified date.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Product::with(['history' => function ($query) {
            $query->whereHasMorph('histories', '*', function ($q) {
                $q->where('date', '<=', $this->date);
            });
        }]);
    }

    /**
     * Defines the headings for the exported Excel file.
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Product Name', // Column for product name
            'Description',  // Column for product description
            'Actual Stock', // Column for actual stock
        ];
    }

    /**
     * Maps the product data to rows in the exported Excel file.
     *
     * @param Product $product The product being mapped.
     * @return array
     */
    public function map($product): array
    {
        return [
            $product->name,         // Product name
            $product->description,  // Product description
            $product->stock,        // Actual stock quantity
        ];
    }

    /**
     * Defines the size of each chunk of data to be read.
     *
     * @return int
     */
    public function chunkSize(): int
    {
        return 100; // Process 100 records per chunk to optimize memory usage
    }
}
