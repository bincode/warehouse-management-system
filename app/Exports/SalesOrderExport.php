<?php

namespace App\Exports;

use App\Models\ProductSalesOrder;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SalesOrderExport implements FromQuery, WithHeadings, WithMapping, WithStrictNullComparison, ShouldAutoSize, WithStyles
{
    use Exportable;

    public function query()
    {
        return ProductSalesOrder::query()
            ->with('order.company', 'product')
            ->join('sales_orders', 'product_sales_order.sales_order_id', '=', 'sales_orders.id')
            ->orderBy('sales_orders.date');
    }

    public function headings(): array
    {
        return [
            'Date',
            'Customer Name',
            'PO Number',
            'Product',
            'Quantity',
            'Ship Date',
            'Remark'
        ];
    }

    public function map($sales): array
    {
        // Determine the ship date based on ship_priority
        $shipDate = $sales->ship_priority === 'SPECIFIED_DATE'
            ? ($sales->ship_date ? Carbon::parse($sales->ship_date)->format('d/m/Y') : '')
            : $sales->ship_priority;

        return [
            $sales->order->date,
            $sales->order->company->name,
            $sales->order->po_number,
            $sales->product->description,
            $sales->amount,
            $shipDate,
            $sales->remarks
        ];
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function styles(Worksheet $sheet)
    {
        // Define style arrays
        $headerStyle = [
            'font' => [
                'bold' => true,
            ],
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => '87CEEB', // Optional: Change header fill color if desired
                ],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER, // Center align header text
            ],
        ];

        $tableStyle = [
            'fill' => [
                'fillType' => Fill::FILL_SOLID,
                'startColor' => [
                    'rgb' => 'CCFFFF', // Light blue fill color
                ],
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'], // Black border
                ],
            ],
        ];

        $centerAlignStyle = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ];

        // Apply header style to the first row
        $sheet->getStyle('A1:G1')->applyFromArray($headerStyle);

        // Apply fill color to the entire range from the header row to the last row
        $highestRow = $sheet->getHighestRow(); // Get the highest row number
        $sheet->getStyle('A1:G' . $highestRow)->applyFromArray($tableStyle);

        // Center align the "Quantity" (Column E) and "Ship Date" (Column F) columns
        $sheet->getStyle('E1:E' . $highestRow)->applyFromArray($centerAlignStyle);
        $sheet->getStyle('F1:F' . $highestRow)->applyFromArray($centerAlignStyle);
    }
}
