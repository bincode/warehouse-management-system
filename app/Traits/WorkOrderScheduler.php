<?php

namespace App\Traits;

use App\Enums\Status;
use App\Models\WorkOrder;
use Carbon\Carbon;

trait WorkOrderScheduler
{
    /**
     * Define a one-to-many relationship with the WorkOrder model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workOrders()
    {
        return $this->hasMany(WorkOrder::class, 'extruder_id');
    }

    /**
     * Filter and sort work orders for production scheduling.
     *
     * @return void
     */
    public function processSchedules()
    {
        // Define statuses to be excluded from scheduling
        $excludeStatus = [
            Status::FINISHED(),
            Status::CANCELED(),
        ];

        // Filter and sort work orders
        $this->workOrders = $this->workOrders->filter(function ($workOrder) use ($excludeStatus) {

            // Define a date for comparison (yesterday)
            $limitTime = Carbon::now()->subHour();

            // Check if the result date exists and convert it to a Carbon instance
            $productionDate = $workOrder->updated_at;

            // If the work order status is in the excluded statuses
            if (in_array($workOrder->status, $excludeStatus)) {

                if($productionDate && $productionDate->gt($limitTime)){
                    return true;
                }

                if (auth()->check() && auth()->user()->hasRole('operator')) {
                    return false;
                }

                // If no production date is available, include this work order
                if (!$productionDate) {
                    return true;
                }

                // Include only if the production date is greater than limitTime
                return ;
            }

            // Include work orders with statuses not in the excluded list
            return true;
        })
            ->sortBy('order')
            ->sortBy(function ($workOrder) {
                // Custom sort function based on status
                switch ($workOrder->status) {
                    case Status::RUNNING():
                        return 1;
                    case Status::FINISHED():
                        return 2;
                    case Status::PROCESS():
                        return 3;
                    case Status::PREPARE():
                        return 3;
                    case Status::PLANNING():
                        return 3;
                    case Status::HOLD():
                        return 3;
                    case Status::CANCELED():
                        return 3;
                    default:
                        return 100; // Any other status last
                }
            });
    }
}
