<?php

namespace App\Traits;

use App\Models\History;

trait HistoryTrait
{
    /**
     * Get the history of product transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(History::class);
    }

    /**
     * Get the history of product transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactionHistoryWithTotalStock()
    {
        return $this->hasMany(History::class)->selectRaw('rack_id, SUM(CASE WHEN type = "DEBIT" THEN quantity ELSE -quantity END) as quantity')
            ->groupBy('rack_id');
    }

    /**
     * Get the last stock quantity.
     *
     * @return float
     */
    public function getStockAttribute()
    {
        return $this->history->reduce(function ($stock, $transaction) {
            // Check if the transaction item is an instance of Transaction model
            if (!$transaction instanceof History) {
                return 0;
            }

            if ($transaction->type === 'DEBIT') {
                return $stock + $transaction->quantity;
            }

            return $stock - $transaction->quantity;
        }, 0);
    }
}
