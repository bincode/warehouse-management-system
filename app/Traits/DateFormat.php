<?php

namespace App\Traits;

use Carbon\Carbon;

trait DateFormat
{
    /**
     * Set the product inititalization "date".
     *
     * @param  string  $value
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('d/m/Y', $value);
    }

    /**
     * Get the product inititalization date.
     *
     * @param  \Carbon\Carbon|string  $value
     * @return \Carbon\Carbon|string
     */
    public function getDateAttribute($value)
    {
        return ($value instanceof Carbon) ? $value : Carbon::createFromFormat('Y-m-d', $value)->format("d/m/Y");
    }

    /**
     * Apply start date and end date filters to the query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string|null  $startDate
     * @param  string|null  $endDate
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterByDateRange($query, $startDate, $endDate)
    {
        if ($startDate) {
            $query->where('date', '>=', Carbon::createFromFormat('d/m/Y', $startDate)->format('Y-m-d'));
        }

        if ($endDate) {
            $query->where('date', '<=', Carbon::createFromFormat('d/m/Y', $endDate)->format('Y-m-d'));
        }

        return $query;
    }
}
