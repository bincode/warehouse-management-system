<?php

namespace App\Traits;

use App\Enums\Status;
use App\Models\SalesOrder;

trait HasOrder
{
    /**
     * Get the quantity attribute for the product.
     * If the product has a history, return the quantity from the first history record.
     *
     * @return float
     */
    public function getShipPriorityAttribute($value)
    {
        if ($this->pivot && isset($this->pivot->ship_priority)) {
            return $this->pivot->ship_priority;
        }

        return $value ?? 0;
    }

    /**
     * Define a many-to-many relationship with the SalesOrder model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        // Define the many-to-many relationship with the SalesOrder model
        return $this->belongsToMany(SalesOrder::class)
            ->withPivot(['work_order_id', 'amount', 'ship_priority', 'ship_date', 'remarks', 'status'])
            ->withTimestamps();
    }

    /**
     * Scope a query to only include models with a specific order status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithStatus($query, $status)
    {
        // Scope query to filter by the specified order status
        return $query->whereHas('orders', function ($query) use ($status) {
            $query->where('product_sales_order.status', $status);
        });
    }

    /**
     * Get the total order quantity.
     *
     * @return int
     */
    public function getOrderQuantityAttribute()
    {
        // Calculate the sum of the 'amount' column from the pivot table
        return $this->orders->sum(function ($order) {
            return $order->pivot->amount;
        });
    }

    /**
     * Get the total order quantity with a specific status.
     *
     * @param string $status
     * @return float
     */
    public function getReserveOrderQuantityAttribute(): float
    {
        $exceptionStatus = [
            Status::CREATED(),
            Status::COMPLETED(),
            Status::HOLD(),
            Status::CANCELED()
        ];

        // Retrieve orders with the specified status
        $filteredOrders = $this->orders()->whereNotIn('product_sales_order.status', $exceptionStatus)->get();

        // Calculate the sum of the 'amount' column from the filtered orders
        return $filteredOrders->sum(function ($order) {
            return $order->pivot->amount;
        });
    }

    /**
     * Get the total order quantity with a specific status.
     *
     * @param array $excludeWorkorders List of workorder IDs to exclude.
     * @return float
     */

    public function canUpdateSalesOrder($amount = null): bool
    {
        // Define the statuses to include in the filter
        $includeStatus = [
            Status::PLANNING(),
            Status::PROCESS(),
            Status::FINISHED(),
            Status::COMPLETED(),
        ];

        if ($pivot = $this->pivot) {
            if (in_array($pivot->status, $includeStatus)) {

                if (!$amount) {
                    return false;
                }

                if ($pivot->amount != $amount) {
                    return false;
                }
            }
        }

        return true;
    }
}
