<?php

namespace App\Exceptions;

use Exception;

class ObserverException extends Exception
{
    public function __construct()
    {
        parent::__construct("Something Error on Observer", 500, null);
    }
}
