<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiRackController;
use App\Http\Controllers\Api\ApiRoleController;
use App\Http\Controllers\Api\ApiUserController;
use App\Http\Controllers\Api\HistoryController;
use App\Http\Controllers\Api\ApiAdjustmentController;
use App\Http\Controllers\Api\ApiDeliveryOrderController;
use App\Http\Controllers\Api\ApiDeliveryScheduleController;
use App\Http\Controllers\Api\ApiFormulaController;
use App\Http\Controllers\Api\ApiJobCostController;
use App\Http\Controllers\Api\ApiMachineCategoryController;
use App\Http\Controllers\Api\ApiMachineController;
use App\Http\Controllers\Api\ApiProductController;
use App\Http\Controllers\Api\ApiProductTypeController;
use App\Http\Controllers\Api\ApiWarehouseController;
use App\Http\Controllers\Api\ApiPermissionController;
use App\Http\Controllers\Api\ApiProductResultController;
use App\Http\Controllers\Api\ApiMaterialReleaseController;
use App\Http\Controllers\Api\ApiSalesOrderController;
use App\Http\Controllers\Api\ApiMachineProcessTypeController;
use App\Http\Controllers\Api\ApiProductionOrderController;
use App\Http\Controllers\Api\ApiReceiveItemController;
use App\Http\Controllers\Api\ApiRelationController;
use App\Http\Controllers\Api\ApiScheduleController;
use App\Http\Controllers\Api\ApiStockTakingController;
use App\Http\Controllers\Api\ApiWorkOrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->group(function () {
    // Define API resource routes for adjustments
    Route::apiResource('adjustments', ApiAdjustmentController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for delivery orders
    Route::apiResource('deliveries', ApiDeliveryOrderController::class)->only([
        'index', 'destroy'
    ]);

    Route::apiResource('deliveryschedule', ApiDeliveryScheduleController::class)->only([
        'index', 'store', 'destroy'
    ]);

    // Define custom API routes for formulas
    Route::post('/formulas/{formula}/activated', [ApiFormulaController::class, 'toggleActive']);
    Route::post('/formulas/{formula}/validate', [ApiFormulaController::class, 'checkFormulaChanges']);
    Route::put('/formulas/{formula}/validate', [ApiFormulaController::class, 'checkFormulaChanges']);
    Route::apiResource('formulas', ApiFormulaController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for job costs
    Route::apiResource('jobcosts', ApiJobCostController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for job costs
    Route::apiResource('productionorder', ApiProductionOrderController::class)->only([
        'index', 'destroy'
    ]);

    Route::apiResource('racks', ApiRackController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for machines
    Route::apiResource('machines-category', ApiMachineCategoryController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for machines
    Route::apiResource('machines-process-type', ApiMachineProcessTypeController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for machines
    Route::apiResource('machines', ApiMachineController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for permissions
    Route::apiResource('permissions', ApiPermissionController::class)->only([
        'index'
    ]);

    // Define API resource routes for racks
    Route::apiResource('racks', ApiRackController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for roles
    Route::apiResource('roles', ApiRoleController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for users
    Route::apiResource('users', ApiUserController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for warehouses
    Route::apiResource('warehouses', ApiWarehouseController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for product categories
    Route::apiResource('/product/getInventoryType', ApiProductTypeController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for products
    Route::post('/products/validationstock', [ApiProductController::class, 'validateStockProducts']);
    Route::apiResource('products', ApiProductController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for receives
    Route::apiResource('/receives', ApiReceiveItemController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for relations
    Route::apiResource('/relations', ApiRelationController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for material releases
    Route::apiResource('/releases', ApiMaterialReleaseController::class)->only([
        'index', 'destroy'
    ]);

    // Define API resource routes for product results
    Route::apiResource('/results', ApiProductResultController::class)->only([
        'index', 'destroy'
    ]);

    // Define custom API route for update status product on salesorder
    Route::post('/salesorder/import', [ApiSalesOrderController::class, 'importFromExcel']);
    Route::get('/salesorder/export', [ApiSalesOrderController::class, 'exportToExcel']);
    Route::put('/salesorder/updatestatus/{salesorder}', [ApiSalesOrderController::class, 'updateStatus']);
    Route::apiResource('salesorder', ApiSalesOrderController::class)->only([
        'index', 'destroy'
    ]);

    Route::get('/schedule/production', [ApiScheduleController::class, 'loadProductionShedule']);

    // Define custom API route for saving production planning
    Route::put('/workorders/updatestatus/{workorder}', [ApiWorkOrderController::class, 'updateStatus']);
    Route::post('/workorders/save', [ApiWorkOrderController::class, 'saveProductionPlanning']);
    // Define API resource routes for work orders
    Route::apiResource('/workorders', ApiWorkOrderController::class)->only([
        'index', 'destroy'
    ]);

    // Define custom API route for product history
    Route::get('products/{product}/history', [HistoryController::class, 'index'])->name('history');

    // Define custom API routes for stocktaking
    Route::get('/stocktaking/export', [ApiStockTakingController::class, 'exportToExcel'])->name('stocktaking.export');
    Route::post('/stocktaking/import', [ApiStockTakingController::class, 'importFromExcel'])->name('stocktaking.import');

    Route::get('/schedule/delivery', [ApiDeliveryScheduleController::class, 'delivery'])->name('schedule.delivery');
});
