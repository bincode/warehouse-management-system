<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\AdjustmentController;
use App\Http\Controllers\Web\DeliveryOrderController;
use App\Http\Controllers\Web\DeliveryScheduleController;
use App\Http\Controllers\Web\FormController;
use App\Http\Controllers\Web\FormulaController;
use App\Http\Controllers\Web\HistoryController;
use App\Http\Controllers\Web\JobCostController;
use App\Http\Controllers\Web\LoginController;
use App\Http\Controllers\Web\MachineCategoryController;
use App\Http\Controllers\Web\MachineController;
use App\Http\Controllers\Web\MachineProcessTypeController;
use App\Http\Controllers\Web\RelationController;
use App\Http\Controllers\Web\MaterialReleaseController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Web\ProductionOrderController;
use App\Http\Controllers\Web\SalesOrderController;
use App\Http\Controllers\Web\ProductResultController;
use App\Http\Controllers\Web\ReceiveItemController;
use App\Http\Controllers\Web\RoleController;
use App\Http\Controllers\Web\ScheduleController;
use App\Http\Controllers\Web\StocktakingController;
use App\Http\Controllers\Web\UserController;
use App\Http\Controllers\Web\WarehouseController;
use App\Http\Controllers\Web\WarehouseRackController;
use App\Http\Controllers\Web\WorkOrderController;
use App\Http\Controllers\Api\ApiScheduleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route for user login
Route::get('/login', [LoginController::class, 'view'])->name('login');
Route::post('/login', [LoginController::class, 'attempt'])->name('attemptLogin');
Route::get('/logout', [LoginController::class, 'logout'])->middleware('auth')->name('logout');

// Routes inside middleware 'auth' require authentication
Route::middleware('auth')->group(function () {

    // Home route
    Route::view('/', 'pages.home')->name('home');

    Route::get('/schedule/production/ongoing', [ApiScheduleController::class, 'loadProductionOngoing'])->name('plan.production.ongoing');

    // Route for viewing product history
    Route::get('/products/{product}/history', [HistoryController::class, 'index']);

    // Resource routes for various controllers
    Route::resource('/adjustments', AdjustmentController::class);
    Route::resource('/deliveries', DeliveryOrderController::class);
    Route::resource('/formulas', FormulaController::class);
    Route::resource('/jobcosts', JobCostController::class);
    Route::resource('/machines', MachineController::class);
    Route::resource('/machine-category', MachineCategoryController::class);
    Route::resource('/machine-process-type', MachineProcessTypeController::class);
    Route::resource('/products', ProductController::class);
    Route::resource('/receives', ReceiveItemController::class);
    Route::resource('/relations', RelationController::class);
    Route::resource('/results', ProductResultController::class);
    Route::resource('/roles', RoleController::class);
    Route::resource('/users', UserController::class);
    Route::resource('/warehouses', WarehouseController::class);
    Route::resource('/warehouses.racks', WarehouseRackController::class);

    // Custom route for rendering work order form
    Route::get('/form/work-order/{formula}', [FormController::class, 'renderWorkOrderForm']);
    Route::get('/form/material-release/{lotNumber}', [FormController::class, 'renderMaterialReleaseForm']);
    Route::get('/form/production-result/{lotNumber}', [FormController::class, 'renderProductionResultForm']);

    // Route for exporting stocktaking data
    Route::get('/stocktaking/export', [StocktakingController::class, 'cutOff'])->name('stocktaking.export');
    Route::resource('/warehouses.racks', WarehouseRackController::class);

    // Route for viewing production schedule
    Route::get('/schedule/production', [ScheduleController::class, 'production'])->name("plan.production");
    Route::get('/schedule/production/preview', [ScheduleController::class, 'previewproduction'])->name("plan.production.preview");


    Route::get('/schedule/delivery', [ScheduleController::class, 'delivery'])->name("plan.delivery");

    Route::resource('salesorder', SalesOrderController::class)
        ->parameters(['salesorder' => 'order'])->except(['destroy', 'show']);

    Route::resource('productionorder', ProductionOrderController::class)
        ->parameters(['productionorder' => 'order'])->except(['create', 'update', 'destroy', 'show']);

    Route::get('productionorder/create/{product}', [ProductionOrderController::class, 'create'])
        ->name('productionorder.create');

    Route::put('productionorder/{product}', [ProductionOrderController::class, 'update'])
        ->name('productionorder.update');

    Route::resource('deliveryrequest', DeliveryScheduleController::class)->except('destroy', 'show');

    Route::resource('/workorders', WorkOrderController::class);

    Route::view('/warehouse-release', 'pages.material-release.material-release-form-warehouse');
    Route::resource('/releases', MaterialReleaseController::class);
});
