<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_user_can_login()
    {
        // Create a user
        $user = User::factory()->create([
            'password' => 'password123'
        ]);

        // Test login
        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/login')
                ->type('username', $user->username)
                ->type('password', 'password123')
                ->press('Sign In')
                ->assertPathIs('/');
        });
    }

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function test_user_can_logout()
    {
        $this->browse(function (Browser $browser) {
            // Navigate to the home page or any page where the sidebar is visible
            $browser->visit('/')
                // Click the logout button in the sidebar
                ->clickLink('Logout')
                // Optionally, you can assert that the user is redirected to the login page
                ->assertPathIs('/login');
        });
    }

    /**
     * A Dusk test example for failed login.
     *
     * @return void
     */
    public function test_user_cannot_login_with_invalid_credentials()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('username', 'nonexistentuser')
                ->type('password', 'wrongpassword')
                ->press('Sign In')
                ->assertPathIs('/login')
                ->assertSee('Wrong password');
        });
    }
}
