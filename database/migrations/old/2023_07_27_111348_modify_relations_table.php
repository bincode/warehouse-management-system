<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModifyRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->enum('type', ['s', 'c', 'cs'])->default('c');
        });

        // Add a column named 'type' according to the values in the 'is_supplier' and 'is_customer' column.
        DB::table('relations')
            ->where('is_supplier', 1)
            ->where('is_customer', 1)
            ->update(['type' => 'cs']);

        DB::table('relations')
            ->where('is_supplier', 1)
            ->where('is_customer', 0)
            ->update(['type' => 's']);

        DB::table('relations')
            ->where('is_supplier', 0)
            ->where('is_customer', 1)
            ->update(['type' => 'c']);

        Schema::table('relations', function (Blueprint $table) {
            $table->dropColumn(['is_supplier', 'is_customer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations', function (Blueprint $table) {
            $table->string('is_supplier');
            $table->string('is_customer');
        });

        DB::table('relations')
            ->where('type', 'cs')
            ->update(['is_supplier' => 1, 'is_customer' => 1]);

        DB::table('relations')
            ->where('type', 's')
            ->update(['is_supplier' => 1, 'is_customer' => 0]);

        DB::table('relations')
            ->where('type', 'c')
            ->update(['is_supplier' => 0, 'is_customer' => 1]);

        Schema::table('relations', function (Blueprint $table) {
            $table->dropColumn(['type']);
        });
    }
}
