<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddTypeColumnToHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('histories', function (Blueprint $table) {
            $table->enum('type', ['DEBIT', 'CREDIT'])->nullable()->after('histories_type');
        });

        // Add a column named 'type' according to the values in the 'histories_type' column.
        DB::table('histories')
            ->where('histories_type', 'Initialisation')
            ->update(['type' => 'DEBIT']);

        DB::table('histories')
            ->where('histories_type', 'Receive')
            ->update(['type' => 'DEBIT']);

        DB::table('histories')
            ->where('histories_type', 'Result')
            ->update(['type' => 'DEBIT']);

        DB::table('histories')
            ->where('histories_type', 'Adjustment')
            ->update(['type' => 'DEBIT']);

        // Set default value for 'type' column to 'CREDIT' for rows where 'type' is still NULL
        DB::table('histories')
            ->whereNull('type')
            ->update(['type' => 'CREDIT']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('histories', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
