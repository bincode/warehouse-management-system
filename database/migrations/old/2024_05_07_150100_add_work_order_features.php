<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWorkOrderFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create machine_categories table
        Schema::create('machine_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->default('-');
            $table->timestamps();
        });

        // Create machine_proc_types table
        Schema::create('machine_proc_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create machines table
        Schema::create('machines', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("description");
            $table->unsignedBigInteger('category_id');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')
                ->on('machine_categories')
                ->onDelete('cascade');
        });

        // Add color column to products table
        Schema::table('products', function (Blueprint $table) {
            $table->string('color')->nullable()->after('description');
        });

        // Create work_orders table
        Schema::create('work_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('formula_id')->nullable();
            $table->string('lot')->unique()->nullable();
            $table->float('plan_quantity');
            $table->unsignedBigInteger('extruder_id');
            $table->unsignedBigInteger('mixer_id')->nullable();
            $table->unsignedBigInteger('endProduct_id')->nullable();
            $table->string('mixing_note')->nullable();
            $table->text('note')->nullable();
            $table->integer('mixing_quantity')->nullable();
            $table->integer('order')->unsigned();
            $table->date('date')->nullable();
            $table->unsignedBigInteger('machineProcType_id')->nullable();
            $table->timestamps();

            $table->foreign('formula_id')
                ->references('id')
                ->on('formulas')
                ->onDelete('cascade');
            $table->foreign('extruder_id')
                ->references('id')
                ->on('machines')
                ->onDelete('cascade');
            $table->foreign('mixer_id')
                ->references('id')
                ->on('machines')
                ->onDelete('cascade');
            $table->foreign('endProduct_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
            $table->foreign('machineProcType_id')
                ->references('id')
                ->on('machine_proc_types')
                ->onDelete('cascade');
        });

        // Create customer_work_order pivot table
        Schema::create('customer_work_order', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('work_order_id');
            $table->unsignedBigInteger('customer_id');
            $table->timestamps();

            $table->foreign('work_order_id')
                ->references('id')
                ->on('work_orders')
                ->onDelete('cascade');
            $table->foreign('customer_id')
                ->references('id')
                ->on('relations') // Assuming 'relations' is the table name for customers
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_work_order');
        Schema::dropIfExists('work_orders');
        Schema::dropIfExists('machines');
        Schema::dropIfExists('machine_proc_types');
        Schema::dropIfExists('machine_categories');
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
