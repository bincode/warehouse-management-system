<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_plannings', function (Blueprint $table) {
            $table->id();
            $table->date("date");
            $table->unsignedBigInteger('product_sales_order_id');
            $table->unsignedBigInteger('delivery_order_id')->nullable();
            $table->string("quantity");
            $table->string("lot");
            $table->string("bag");
            $table->string("document")->nullable();
            $table->string("note")->nullable();
            $table->timestamps();

            $table->foreign('product_sales_order_id')->references('id')->on('product_sales_order')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_plannings');
    }
}
