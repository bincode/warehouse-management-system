<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabel sales_orders
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('company_id');
            $table->string('po_number');
            $table->date('close_date')->nullable();
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('relations')->onDelete('cascade');
        });

        // Tabel pivot product_sales_order
        Schema::create('product_sales_order', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sales_order_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('work_order_id')->nullable();
            $table->integer('amount');
            $table->string('ship_priority');
            $table->date('ship_date')->nullable();
            $table->text('remarks')->nullable();
            $table->string('status')->default('N#A');
            $table->timestamps();

            $table->foreign('sales_order_id')->references('id')->on('sales_orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('work_order_id')->references('id')->on('work_orders')->onDelete('set null');
        });

        // Tabel pivot delivery_order_sales_order
        Schema::create('delivery_order_sales_order', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sales_order_id');
            $table->unsignedBigInteger('delivery_order_id');
            $table->timestamps();

            $table->foreign('sales_order_id')->references('id')->on('sales_orders')->onDelete('cascade');
            $table->foreign('delivery_order_id')->references('id')->on('deliveries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_sales_order');
        Schema::dropIfExists('product_sales_order');
        Schema::dropIfExists('sales_orders');
    }
}
