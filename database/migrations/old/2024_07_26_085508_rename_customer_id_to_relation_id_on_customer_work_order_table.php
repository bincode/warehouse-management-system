<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameCustomerIdToRelationIdOnCustomerWorkOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_work_order', function (Blueprint $table) {
            $table->renameColumn('customer_id', 'relation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_work_order', function (Blueprint $table) {
            $table->renameColumn('relation_id', 'customer_id');
        });
    }
}
