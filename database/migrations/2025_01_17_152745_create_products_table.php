<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->string('color')->nullable();
            $table->bigInteger('inventory_id')->unsigned();
            $table->bigInteger('rack_id')->unsigned();
            $table->timestamps();

            $table->foreign('inventory_id')->references('id')->on('inventory_types')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('rack_id')->references('id')->on('racks')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
