<?php

namespace Database\Seeders;

use App\Models\ProductType;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        $roles = [
            'administrator',
            'laboratory staff',
            'ppic staff',
            'warehouse staff',
            'marketing staff'
        ];

        foreach ($roles as $role) {
            Role::firstOrCreate(['name' => $role]);
        }

        // Permissions
        $permissions = [
            'view administration',
            'create administration',
            'update administration',
            'delete administration',
            'view laboratory',
            'view marketing',
            'view ppic',
            'view manufacture',
            'view warehouse',
        ];

        foreach ($permissions as $permission) {
            Permission::firstOrCreate(['name' => $permission]);
        }
        // Assign permissions to roles
        $labStaffRole = Role::findByName('laboratory staff');
        $labStaffRole->givePermissionTo('view laboratory');

        $ppicStaffRole = Role::findByName('ppic staff');
        $ppicStaffRole->givePermissionTo(['view ppic', 'view manufacture']);

        $warehouseStaffRole = Role::findByName('warehouse staff');
        $warehouseStaffRole->givePermissionTo(['view warehouse', 'view manufacture']);

        // Assign permissions to roles
        $marketingStaffRole = Role::findByName('marketing staff');
        $marketingStaffRole->givePermissionTo('view marketing');

        // Assign permissions to 'administrator' role
        $adminRole = Role::findByName('administrator');
        $adminRole->givePermissionTo($permissions);

        // Buat pengguna dengan nama pengguna 'Admin' jika belum ada
        $adminUser = User::firstOrCreate(['username' => 'Admin', 'password' => '12345678']);

        // Assign 'administrator' role to admin user
        if (!$adminUser->hasRole('administrator')) {
            $adminUser->assignRole('administrator');
        }

        ProductType::firstOrCreate(['name' => 'Raw Material', 'description' => 'Bahan baku', 'is_material' => true, 'is_goods' => false]);
        ProductType::firstOrCreate(['name' => 'Finished Goods', 'description' => 'Barang jadi', 'is_material' => false, 'is_goods' => true]);
        ProductType::firstOrCreate(['name' => 'Semi-Finished Goods', 'description' => 'Barang setengah jadi', 'is_material' => false, 'is_goods' => false]);
    }
}
