<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\ProductType;
use App\Models\Rack;
use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class SimulationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat 2 Warehouse  
        $warehouses = Warehouse::factory()->count(2)->create();

        // Menampilkan informasi tentang Warehouse yang dibuat  
        $this->command->info('2 Warehouses created:');

        // Untuk setiap Warehouse, buat 20 Rack  
        foreach ($warehouses as $warehouse) {
            Rack::factory()->count(20)->create([
                'warehouse_id' => $warehouse->id,
            ]);

            // Menampilkan informasi tentang Rack yang dibuat untuk setiap Warehouse  
            $this->command->info("Warehouse '{$warehouse->name}' has 20 racks created.");
        }

        // Membuat 2000 Produk
        $totalProducts = 100;

        // Membuat produk  
        $this->command->info("Creating Raw Materials...");
        Product::factory()->rawMaterial()->count((int) (70 * $totalProducts / 100))->create();

        $this->command->info("Creating Finished Goods...");
        Product::factory()->finishedGood()->count((int) (25 * $totalProducts / 100))->create();

        $this->command->info("Creating Semi-Finished Goods...");
        Product::factory()->semiFinishedGood()->count((int) (5 * $totalProducts / 100))->create();

        $this->command->info("Successfully created $totalProducts products.");
    }
}
