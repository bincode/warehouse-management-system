<?php

namespace Database\Seeders;

use App\Models\Machine;
use App\Models\MachineCategory;
use Illuminate\Database\Seeder;

class MachineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catExtruder = MachineCategory::firstOrCreate(['name' => 'Extruder']);
        $catMixer = MachineCategory::firstOrCreate(['name' => 'Mixer']);

        Machine::create([
            'name' => "No Extruder",
            'description' => "Process Without Extruder",
            'category_id' => $catExtruder->id
        ]);

        Machine::create([
            'name' => "No Mixer",
            'description' => "Process Without Mixer",
            'category_id' => $catMixer->id
        ]);

        for ($i = 1; $i < 13; $i++) {
            Machine::firstOrCreate(['name' => "Ext $i"], [
                'description' => "Machine Extruder $i",
                'category_id' => $catExtruder->id
            ]);
        }

        for ($i = 1; $i < 9; $i++) {
            Machine::firstOrCreate(['name' => "Mixer $i"], [
                'description' => "Machine Mixer $i",
                'category_id' => $catMixer->id
            ]);
        }
    }
}
