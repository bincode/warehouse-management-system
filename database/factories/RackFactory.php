<?php  
  
namespace Database\Factories;  
  
use Illuminate\Database\Eloquent\Factories\Factory;  
use App\Models\Warehouse;  
  
class RackFactory extends Factory  
{  
    /**  
     * Define the model's default state.  
     *  
     * @return array  
     */  
    public function definition()  
    {  
        return [  
            'code' => $this->faker->unique()->regexify('[A-Z][0-9]{3}'),  
            'note' => $this->faker->optional()->sentence,  
            'warehouse_id' => Warehouse::factory(),  
        ];  
    }  
}  
