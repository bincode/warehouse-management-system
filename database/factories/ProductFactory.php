<?php

namespace Database\Factories;

use App\Models\Rack;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**  
     * Define the model's default state.  
     *  
     * @return array  
     */
    public function definition()
    {
        return [
            'description' => $this->faker->sentence,
            'color' => $this->faker->colorName,
            'rack_id' => Rack::inRandomOrder()->first()->id, // Asumsikan Anda sudah memiliki RackFactory  
        ];
    }

    /**  
     * Indicate that the product is a raw material.  
     *  
     * @return \Illuminate\Database\Eloquent\Factories\Factory  
     */
    public function rawMaterial()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'RW ' . $this->faker->unique()->numberBetween(1000, 9999),
                'inventory_id' => 1, // Raw Material  
            ];
        });
    }

    /**  
     * Indicate that the product is a finished good.  
     *  
     * @return \Illuminate\Database\Eloquent\Factories\Factory  
     */
    public function finishedGood()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'FG ' . $this->faker->unique()->numberBetween(1000, 9999),
                'inventory_id' => 2, // Finished Goods  
            ];
        });
    }

    /**  
     * Indicate that the product is a semi-finished good.  
     *  
     * @return \Illuminate\Database\Eloquent\Factories\Factory  
     */
    public function semiFinishedGood()
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'SFG ' . $this->faker->unique()->numberBetween(1000, 9999),
                'inventory_id' => 3, // Semi-Finished Goods  
            ];
        });
    }
}
